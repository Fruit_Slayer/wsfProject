package com.wsfPro.core;

import com.wsfPro.entities.base.AbstractFreeEntity;
import com.wsfPro.services.AbstractPersisTableServiceImpl;

public interface PersisTableService extends AbstractPersisTableService<AbstractFreeEntity> {
}
