package com.wsfPro.core;

import com.wsfPro.repository.Persistable;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface AbstractService<X extends Persistable, I> extends AbstractPersisTableService<I> {

//  X save(X entity);
//
//  X update(X entity);
//
//  X findById(I id);
//
//  List<X> findByFiled(String filed, Object o);
//
//  /**
//   * 多个字段的查询
//   *
//   * @param linkedHashMap 将你的字段传入map中
//   * @return
//   */
//  List<X> findByFileds(LinkedHashMap<String, Object> linkedHashMap);
//
//  List<X> findAll();
//
//  List<X> findByNativeQuery(String sql, Map<String, Object> conditionParams);
//
//  /**
//   * 一个字段的分页
//   *
//   * @param filed 字段名
//   * @param o 字段参数
//   * @param pageNo 第几页
//   * @param pageSize 一个页面多少条数据
//   * @return
//   */
//  List<X> findPageByNativeQueryFiled(String filed, Object o, int pageNo, int pageSize);
//
//  /**
//   * 多字段查询分页
//   *
//   * @param pageNo 第几页
//   * @param pageSize 一个页面的条数
//   * @param conditionParams
//   * @return
//   */
//  Map<String, Object> findPageByNativeQueryFileds(
//      String sql, Map<String, Object> conditionParams, Integer pageNo, Integer pageSize);
//
//  /**
//   * 根据传入的map遍历key,value拼接字符串，以id为条件更新
//   *
//   * @param map 传入参数放入map中
//   * @return
//   */
//  Integer updateFileds(LinkedHashMap<String, Object> map);
//
//  /**
//   * 只能删除持久状态的实体
//   *
//   * @param entity
//   */
//  boolean remove(X entity);
//
//  /** 根据表的id删除数据 */
//  boolean deleteById(I id);
//
//  /** 根据参数删除表数据 */
//  boolean deleteByMap(Map paramsMap);
//
//  /**
//   * 根据id查询
//   *
//   * @param t
//   * @param idClass
//   * @return
//   */
//  X findByIdClass(X t, Object idClass);
//
//  X first(String hql);
//
//  X first(String hql, Object arg);
//
//  X first(String hql, Object arg, Object arg2);
//
//  X first(String hql, Object... args);
//
//  // ---------------------------------single
//  X single(String hql);
//
//  X single(String hql, Object arg);
//
//  X single(String hql, Object arg, Object arg2);
//
//  X single(String hql, Object... args);
//
//  // ---------------------------------list
//  List<X> list(String hql);
//
//  List<X> list(String hql, Object arg);
//
//  List<X> list(String hql, Object arg, Object arg2);
//
//  List<X> list(String hql, Object... args);
//
//  // ---------------------------------top
//  List<X> top(int top, String hql);
//
//  List<X> top(int top, String hql, Object arg);
//
//  List<X> top(int top, String hql, Object arg, Object arg2);
//
//  List<X> top(int top, String hql, Object... args);
}
