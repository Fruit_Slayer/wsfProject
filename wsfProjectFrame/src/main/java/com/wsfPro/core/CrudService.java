package com.wsfPro.core;

import com.wsfPro.util.controllerUtil.JsonResult;

import java.io.Serializable;

/**
 * 定义为接口，该url对应多个实现会报错提示已经有某个控制器类实现了该add delById upd findById url路径 There is already 'sysLevel' bean
 * method
 *
 * @param <T>
 * @param <ID>
 */
public interface CrudService<T extends Serializable, ID extends Serializable> {
  //  @RequestMapping("add")
  //  @RequestBody无法使用在接口的参数上
  JsonResult add(T t);
  //  @RequestMapping("delById")
  JsonResult delById(ID id);
  //  @RequestMapping("upd")
  JsonResult upd(T t);
  //  @RequestMapping("findById")
  JsonResult findById(ID id);
}
