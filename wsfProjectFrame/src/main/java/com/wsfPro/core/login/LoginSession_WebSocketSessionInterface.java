package com.wsfPro.core.login;

import javax.servlet.http.HttpSession;
import javax.websocket.Session;

/**
 * 每个浏览器生成一个websocket连接通道，登录成功根据loginId查找是否已经存在会话,
 * 如果存在则根据loginId找到webSocketSession推送实时消息账号在其他地方登录，
 * 销毁httpSession,webSocketSession，
 * 如果不存在httpsession
 * 直接执行最后一步保存 loginId:httpSession,loginId:webSocketSession,
 */
public interface LoginSession_WebSocketSessionInterface {
  /**
   * 保存Session
   *
   * @param id
   * @param session
   */
  void saveLoginSession(long id, HttpSession session);

  /**
   * 保存WebSocket
   *
   * @param id
   * @param session
   */
  void saveWebSocketSession(long id, Session session);

  // void removeLoginSession_WebSocketSession(long id);

  /**
   * 通过账号id得到HttpSession
   *
   * @param id
   * @return
   */
  HttpSession getHttpSessionByLid(long id);

  /**
   * 通过sid得到HttpSession
   *
   * @param sid
   * @return
   */
  HttpSession getHttpSessionBySid(String sid);

  /**
   * 通过lid获得WebSocketSession
   *
   * @param id
   * @return
   */
  Session getWebSocketSessionByLid(long id);
  /**
   * 通过账号id移除WebSocket
   *
   * @param id
   */
  void delWebSocketSessionByLid(long id);

  /**
   * 通过sessionid移除HttpSession
   *
   * @param sid
   */
  void delSessionBySid(String sid);
}
