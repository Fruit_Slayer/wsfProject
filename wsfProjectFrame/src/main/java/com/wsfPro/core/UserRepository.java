package com.wsfPro.core;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<com.wsfPro.core.User, String> {

}
