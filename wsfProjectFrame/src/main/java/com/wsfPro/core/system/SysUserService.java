package com.wsfPro.core.system;

import com.wsfPro.core.base.BaseCodeService;
import com.wsfPro.entities.system.SysRoleEntity;
import com.wsfPro.entities.system.SysUserEntity;
import com.wsfPro.util.controllerUtil.JsonResult;

import javax.servlet.http.HttpServletRequest;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

/** @author eiven */
public interface SysUserService extends BaseCodeService<SysUserEntity> {

  //  /**
  //   * 根据用户代码找用户信息
  //   *
  //   * @author eiven
  //   * @param code 用户代码
  //   * @return Map<String,Object>
  //   */
  //  SysUserEntity getUserMapByCode(String code);

  /**
   * 用户信息保存或新增
   *
   * @author eiven
   * @param paramsMap
   * @return
   */
  JsonResult doSaveOrUpdate(Map paramsMap);

  /**
   * 查询用户列表
   *
   * @param paramsMap 查询条件
   * @return Map<String,Object>
   */
  Map<String, Object> findUserList(Map paramsMap);

  /**
   * 注销登录
   *
   * @param request
   * @return boolean
   */
  boolean layout(HttpServletRequest request);
  //  /**
  //   * 登录
  //   *
  //   * @param paramsMap 查询条件
  //   * @return JsonResult
  //   */
  //  JsonResult doLogin(Map paramsMap);

  /**
   * 分配用户角色
   *
   * @param paramsMap 查询条件
   * @return JsonResult
   */
  JsonResult addUserRole(Map paramsMap);

  /**
   * 用户角色列表
   *
   * @param paramsMap 查询条件
   * @return JsonResult
   */
  JsonResult userRoleList(Map paramsMap);

  /** 用户角色列表 */
  List<SysRoleEntity> userRoleList(String uId);

  /** 更新登录成功基本参数 */
  void updUserParamByLoginSuccess(HttpServletRequest httpServletRequest)
      throws UnknownHostException;

  //  /** 更新登出基本参数 */
  //  void updUserParamByLoginOut(HttpServletRequest httpServletRequest) throws
  // UnknownHostException;

  JsonResult activeCode(String code);
}
