package com.wsfPro.core.system;

import com.wsfPro.controllers.dto.system.SysLevelDto;
import com.wsfPro.core.AbstractService;
import com.wsfPro.entities.system.SysLevelEntity;
import com.wsfPro.util.controllerUtil.JsonResult;

public interface SysLevelService extends AbstractService<SysLevelEntity, String> {

  JsonResult addSysLevel(SysLevelDto sysLevelDto);
}
