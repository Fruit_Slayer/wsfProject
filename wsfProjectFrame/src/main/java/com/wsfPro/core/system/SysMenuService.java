package com.wsfPro.core.system;

import com.wsfPro.controllers.dto.system.SysMenuDto;
import com.wsfPro.core.base.BaseCodeService;
import com.wsfPro.entities.system.SysMenuEntity;
import com.wsfPro.util.controllerUtil.JsonResult;

import java.util.HashMap;

public interface SysMenuService extends BaseCodeService<SysMenuEntity> {

  //  /** 根据用户角色初始菜单 */
  //  List<SysMenuEntity> initMenuList();

  //  /**
  //   * 菜单维护列表查询
  //   *
  //   * @param paramsMap 查询参数
  //   * @return Map
  //   * @author eiven
  //   */
  //  Map<String, Object> findMenuList(Map paramsMap);

  /**
   * 菜单以及其拥有的权限信息的保存
   *
   * @param sysMenuDto
   * @return
   */
  JsonResult insertUpdMenu(SysMenuDto sysMenuDto);

  /**
   * 菜单以及其拥有的权限信息更新
   *
   * @param sysMenuDto
   * @return
   */
  JsonResult update(SysMenuDto sysMenuDto);

  void delete(String id, HashMap<String, Object> map);
  //  /**
  //   * 菜单以及其拥有的权限信息的保存或新增
  //   *
  //   * @param paramsMap
  //   * @return JsonResult
  //   * @author eiven
  //   */
  //  JsonResult doSaveOrUpdate(Map paramsMap);

  //  /**
  //   * 根据表的id删除数据
  //   *
  //   * @param clazz
  //   */
  //  boolean delete(Class clazz, String id);

  //  */    /**
  ////     * 所有菜单,在url资源菜单匹配需要对菜单从细粒度开始排序
  ////     */

  //    /**
  //     * url去除/拼接查询获取matchUrl 特殊get请求有?再做截取
  //     *
  //     * @param matchUrl
  //     * @return
  //     */

  /** 根据id查询 */
  SysMenuEntity findMenuRoleById(String menuId);
}
