package com.wsfPro.core.system;

import com.wsfPro.controllers.dto.system.SysRoleDto;
import com.wsfPro.core.base.BaseDateService;
import com.wsfPro.entities.system.SysRoleEntity;
import com.wsfPro.util.controllerUtil.JsonResult;

public interface SysRoleService extends BaseDateService<SysRoleEntity> {
  JsonResult addSysRole(SysRoleDto roleItem);

  JsonResult updRole(SysRoleDto roleItem);
}
