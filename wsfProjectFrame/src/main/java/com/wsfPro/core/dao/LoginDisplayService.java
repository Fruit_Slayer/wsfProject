package com.wsfPro.core.dao;

import com.wsfPro.controllers.item.LoginDisplay;

public interface LoginDisplayService {
  LoginDisplay getLogin(String nickName, String passWord) throws Exception;
}
