package com.wsfPro.core.dao;

import java.util.List;

public interface Dao<T> {
  public List<T> select(Class<T> t, String hql);
}
