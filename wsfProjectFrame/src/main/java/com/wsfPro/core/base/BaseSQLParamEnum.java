package com.wsfPro.core.base;

public enum BaseSQLParamEnum {
  ROWS("rows"),

  TOTAL("total");

  private final String value;

  private BaseSQLParamEnum(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
