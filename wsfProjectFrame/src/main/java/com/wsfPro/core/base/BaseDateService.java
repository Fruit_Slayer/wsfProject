package com.wsfPro.core.base;

import com.wsfPro.entities.base.BaseDataEntity;
import com.wsfPro.util.controllerUtil.JsonResult;

import java.util.Collection;
import java.util.List;

public interface BaseDateService<X extends BaseDataEntity> extends BaseService<X> {

  /**
   * 保存或更新数据对象
   *
   * @param entity
   * @return
   */
  JsonResult saveOrUpdate(X entity);

  /**
   * 批量操作对象(增删改)
   *
   * @param entityList entity 集合
   * @return
   */
  JsonResult batchObject(List<X> entityList);

  Collection<X> findData(Object o);
}
