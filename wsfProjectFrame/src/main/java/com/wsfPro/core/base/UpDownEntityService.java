package com.wsfPro.core.base;

import java.util.List;

public interface UpDownEntityService<E> {
    void sortUpDownEntity(List<E> list);
}
