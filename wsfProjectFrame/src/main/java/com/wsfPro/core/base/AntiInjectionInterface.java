package com.wsfPro.core.base;

import java.util.Set;

public interface AntiInjectionInterface {
  /**
   * 防sql注入
   *
   * @param selSqlTabColumns sql 语句的条件key 执行完此方法后sqlColums便是注入的字段 如果sqlColums集合长度size为0则说明没用注入key
   * @param entityClazzs 数据库实体集合在哪些表实体校验
   * @return true 则说明有的条件key不存在当前的 entityClass false则说明所有的条件key都是entityClass某些字段
   */
  boolean checkSqlAntiInfusion(Set<String> selSqlTabColumns, Class<?>... entityClazzs);
}
