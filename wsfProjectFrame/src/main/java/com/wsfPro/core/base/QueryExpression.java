package com.wsfPro.core.base;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 查询表达式，应该可以被定义在很多个地方；表达式详细的说明请看{@link cn.bonoon.kernel.expression.ExpressionParser
 * ExpressionParser}
 *
 * @author jackson
 */
@Target({FIELD, TYPE, METHOD})
@Retention(RUNTIME)
public @interface QueryExpression {
  /**
   *
   *
   * <pre>
   * 使用join连接进行查询；允许定义多级的join查询，如：
   * from user x left join x.roles y left join y.actions z
   * </pre>
   *
   * @return join连接
   * @see QueryJoin
   */
  QueryJoin[] joins() default {};

  /**
   *
   *
   * <pre>
   * 表达式，如：x.user.id={uid} and (x.status={s0} or x.status={s1})
   *
   * 如果使用了join连接，则表达式可以写成：
   * x.user.id={uid} and y.status={rstatus}
   * 其中：
   * {@link #joins() joins}=@{@link QueryJoin JoinField(value="roles", alias="y")}
   *
   * 以上的例子中，将会解析出所需要的查询字段为：
   * uid、s0、s1、rstatus
   * 这些字段将会在查询对象里读取相应的属性值(如果有定义)；
   * 如果查询对象没有定义，则会通过request对象来查询相应的参数(参数值与"uid、s0、s1、rstatus"中的相同)
   * 如果都没有，则可以直接使用默认值，如：数字类型为0，字段串则为""等
   *
   * 该表达式也可以是固定的值，如：x.status=1等
   *
   * 表达式详细的说明请看{@link com.wsfPro.entities.base.BaseEntity}
   * </pre>
   *
   * @return 表达式
   */
  String value();
}
