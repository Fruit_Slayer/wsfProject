// package com.wsfPro.core.base;
//
// import java.util.Map;
/// **
// *
// *
// * <pre>
// * 查询表达式：
// * 1. FIELD 表达式(解析器：{@link cn.bonoon.kernel.expression.resolver.FieldExpressionResolver
// FieldExpressionResolver})
// * 描述：用于从对象、接口、枚举等类型中取出静态字段的值。
// * 格式：FIELD class field [method]
// * 参数：
// *  class - 表示指定的类、接口或枚举等
// *  field - 需要取值的字段
// *  [method] - 【可选】一般来讲该参数对于枚举用的比较多，如："UserType.ADMIN"，如果需要取枚举名，则"UserType.ADMIN.name()"；
// *              如果需要取枚举的位置，则"UserType.ADMIN.ordinal()"；<font color="red">注意：目前该方法不允许使用参数</font>
// * 例子：
// *   x.flag={FIELD cn.bonoon.entities.plugins.FlagType ADMIN} 或 x.flag={FIELD
// cn.bonoon.entities.plugins.FlagType ADMIN ordinal}
// *
// * 2. MODEL 表达式(解析器：{@link cn.bonoon.kernel.expression.resolver.ModelExpressionResolver
// ModelExpressionResolver})<font color='blue'>重点</font>
// * 描述：可以从当前界面的velocity赋值的语法中直接取bean对象的值
// * 格式：BEAN velocityExpression [fieldName] [{type}] [{mode}]
// * 参数：
// *   velocityExpression - 为velocity的赋值表达式
// *   [fieldName] - 指定用于传值的参数名，如果没指定，则会自动生成；指定的好处是该参数可以在其它地方被使用，如果自动生成，则没办预先知道参数名
// *   [{type}] - 类型参数(该参数在"MODEL"后面可以出现在任意位置；只允许定义一次，如果多次定义，则最后一次有效)，可选的类型有：
// *     -O : 表示接收这边以对象形式来接收
// *     -STR : 表示通过request来传值，并且是字符串类型的，默认情况，一般可以不用指定
// *     -DT :  表示通过request来传值，并且自动转为字符串类型
// *     -I : int
// *     -J : long
// *     -F : float
// *     -D : double
// *     -Z : boolean
// *     -B : byte
// *     -C : char
// *     -S : short
// *   [{mode}] - 方式(该参数在"MODEL"后面可以出现在任意位置；只允许定义一次，如果多次定义，则最后一次有效)，可选的类型有：
// *     #DIRECT : 参考{@link QueryParameterType#DIRECT}
// *     #PROPERTY : 参考{@link QueryParameterType#PROPERTY}
// *     #INPUT : 参考{@link QueryParameterType#INPUT}
// *     ...  具体的类型可以参照{@link QueryParameterType}类型说明
// * 例子：
// *   x.status={MODEL form.id id} 使用参数名"id"从request请求对象取值，界面(javascript/html)端也会自动生成取值语句 {id :
// '$!{form.id}'}
// *   x.status={MODEL status} 使用参数名"status"从request请求对象取值(字符串类型)
// *   x.status={MODEL status -J} 通过request传递参数，并且把该参数转为Long类型对象
// *   x.status={MODEL form.id id -O} 表示控制器接收值时是一个对象，并且使用该对象里的id字段的值
// *   x.user.status={MODEL form.status -I #PROPERTY} "form.status"为velocity的表达式，用于传值的参数名将自动生成 {???
// : '$!{form.status}'}
// *   x.user.name like '%{MODEL name name #INPUT}%' 表示界面端将使用{name : jQuery('#name').val()}来取值
// *   x.user.name like '%{MODEL name #INPUT}%' 表示界面端将使用{??? :
// jQuery('#name').val()}来取值，用于传值的参数名将自动生成；注意与上面例子的区别
// *
// * 3. USER 表达式(解析器：{@link cn.bonoon.kernel.expression.resolver.UserExpressionResolver
// UserExpressionResolver})
// * 描述：可以使用当前登录用户的一些值来组成查询条件，当然必须有当前登录的用户
// * 格式：USER field [args]
// * 参数：其中"[]"号内为简写，等同于全称
// *   field -
// 这里将有几个可选的类型，包括：id/owner[oi]/username[un]/displayname[dn]/isSuper[is]/isAdmin[ia]/get(如果是get，则需要参数)
// *   [args] - 如果类型需要参数，则在这里填写需要的参数
// *   <font
// color="red">注意：当"field"为"get"时不需要进行类型的转换。因为user.attributes里存储的为Object类型的对象，我们认为就是需要的类型！</font>
// * 例子：
// *   x.user.id={USER id}
// *   x.user.ownerId={USER owner}
// *   x.user.name like '%{USER username}%'
// *   x.user.size>{USER get year}
// *
// * 4. SESSION 表达式(解析器：{@link cn.bonoon.kernel.expression.resolver.SesssionExpressionResolver
// SesssionExpressionResolver})
// * 描述：这个表达式可以从当前用户的有效的session里取出某些对象，前提条件是这些值有某些地方已经被设置好了
// * 格式：SESSION name
// * 参数：name - 参数名
// * 例子：
// *   x.user.flag={SESSION flag}
// *   x.user.name like '{SESSION username}%'
// *
// * </pre>
// *
// * <font color="red">如果以上的表达式无法满足查询条件的需求，则可以直接重写过滤器，直接对finder进行条件操作</font>
// *
// * @author jackson
// */
// public interface ExpressionParser {
//  //	void addExpressions(ConditionExpression[] expressions);
//  //	QueryParameter addExpression(ConditionExpression expression);
//  //	QueryParameter addExpression(String expression);
//
//  Map<String, String> joinMapped();
//
//  ExpressionValue[] values();
//
//  /**
//   * <font color="red"> 注意：取得的where表达式的格式如：" and x.user=? and x.status=?"等，形状是以" and "开头的 </font>
//   */
//  String where();
// }
