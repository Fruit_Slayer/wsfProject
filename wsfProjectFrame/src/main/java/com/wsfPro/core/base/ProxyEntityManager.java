package com.wsfPro.core.base;

import com.wsfPro.entities.base.Page;

import java.util.List;

/**
 *
 *
 * <pre>
 * 目前这个接口是给框架内部使用的，不应该被外部的对象调用
 * </pre>
 *
 * @author jackson
 */
public interface ProxyEntityManager {

  /**
   * 执行数据库的查询工作
   *
   * @param iClass
   * @param ql
   * @param parameters
   * @return
   */
  <I> List<I> resultList(Class<I> iClass, String ql, Iterable<Object> parameters);

  List<Object> resultList(String ql, Iterable<Object> parameters);

  Page resultPage(String select, String count, Pageable pageable, Iterable<Object> parameters);
}
