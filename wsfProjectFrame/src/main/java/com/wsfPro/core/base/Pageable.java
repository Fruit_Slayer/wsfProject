package com.wsfPro.core.base;
/**
 * 可分页的数据实体
 *
 * @author jackson
 */
public interface Pageable {

  /**
   * 页码
   *
   * @return 页码
   */
  int getPageNumber();

  /**
   * 每页记录数
   *
   * @return 记录数
   */
  int getPageSize();

  /**
   * 当前页的第一个记录的位置
   *
   * @return 第一个记录的位置
   */
  int getFirstItem();

  /**
   * 排序内容
   *
   * @return 排序内容
   */
  //	Sort getSort();
}
