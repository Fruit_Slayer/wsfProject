package com.wsfPro.core.base;

/**
 *
 *
 * <pre>
 * 使用join查询的定义
 * 如：from user x left join x.roles y
 * 其中：
 * {@link #value() value="roles"}
 * {@link #alias() alias="y"}
 * </pre>
 *
 * @author jackson
 */
public @interface QueryJoin {

  /**
   *
   *
   * <pre>
   * 需要join查询的字段
   * 一般是对应的集合，如:from user x left join x.roles y
   * 这里的值应该为"roles"，表示对user对象里的roles属性进行子查询
   * </pre>
   *
   * @return
   */
  String value();

  /**
   *
   *
   * <pre>
   * join查询的别名，如：from user x left join x.roles y
   * 这里的别名字义为"y"，默认的情况下是自动根据规则生成的
   * 如果是自己定义，则其它字段或表达式可以直接引用
   * </pre>
   *
   * @return
   */
  String alias() default "";
}
