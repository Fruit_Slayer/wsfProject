package com.wsfPro.core.base;

import com.wsfPro.core.AbstractService;
import com.wsfPro.entities.base.BaseEntity;

public interface BaseService<X extends BaseEntity> extends AbstractService<X, String> {
  void softDelete(X entity);
}
