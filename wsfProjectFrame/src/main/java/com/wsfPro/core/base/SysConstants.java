package com.wsfPro.core.base;

import javax.servlet.http.HttpServletResponse;

public enum SysConstants {
  ROOT_ID("0"),
  USERNAME("username"),
  PASSWORD("password"),
  /** 商户token */
  MERT_ACCESS_TOKEN("Mert-Access-Token"),

  USER_ACCESS_TOKEN("User-Access-Token"),

  RESPONSE_CODE("Response-Code"),

  ROWS("rows"),

  TOTAL("total"),

  ID_CODE("id"),

  USER_TOKEN("userToken");
  private final String value;

  public static void CorsRegistry(HttpServletResponse response) {
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader("Access-Control-Allow-Headers", "*");
    StringBuffer exposeHeaders = new StringBuffer(SysConstants.RESPONSE_CODE.value);
    exposeHeaders.append(",");
    exposeHeaders.append(SysConstants.USER_ACCESS_TOKEN.value);
    response.setHeader("Access-Control-Expose-Headers", exposeHeaders.toString());
  }

  public String getValue() {
    return value;
  }

  private SysConstants(String value) {
    this.value = value;
  }
}
