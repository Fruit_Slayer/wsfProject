package com.wsfPro.core.encryptionDecrypt;

public interface EncryptionDecrypt {
  /**
   * y=a*id+b
   *
   * @param a
   * @param b
   * @param id
   * @return 返回y
   * @throws Exception
   */
  Long encryption(int a, int b, Long id) throws Exception;

  /**
   * x=(y-b)/a a!=0
   *
   * @param a
   * @param b
   * @param needDecryptNumber
   * @return (y-b)/a
   */
  Long linearEquationOneUnknownDecrypt(int a, int b, Long needDecryptNumber);

  //	/**
  //	 * 返回sqlId
  //	 * @param needDecryptNumber
  //	 * @param cha
  //	 * @return
  //	 */
  //	Long Decrypt(Long needDecryptNumber, Long cha);
}
