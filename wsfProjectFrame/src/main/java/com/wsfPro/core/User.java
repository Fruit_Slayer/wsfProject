package com.wsfPro.core;

import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Column;
import java.io.Serializable;

@RedisHash("user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @org.springframework.data.annotation.Id
    @Column(name = "userId")
    private String id;

    private String name;

    private Integer age;

//    private Timestamp birthday;

    private String bankCard;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getBankCard() {
        return bankCard;
    }

    public void setBankCard(String bankCard) {
        this.bankCard = bankCard;
    }
}
