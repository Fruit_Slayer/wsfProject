package com.wsfPro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

// @SpringBootApplication(
//  scanBasePackages = {
//    "com.wsfPro.test",
//    "com.wsfPro.filter",
//    "com.wsfPro.listener",
//    "com.wsfPro.controllers",
//    "com.wsfPro.services",
//    "com.wsfPro.util",
//    "com.wsfPro.entities"
// }
// )
@SpringBootApplication
@EnableAsync
@ServletComponentScan
public class WsfApplication {

  public static void main(String[] args) throws Exception {

    SpringApplication.run(WsfApplication.class, args);
  }
  //    @Bean
  //    public FilterRegistrationBean testFilterRegistration() {
  //
  //        FilterRegistrationBean registration = new FilterRegistrationBean();
  //        registration.setFilter(new FileterTest());
  //        registration.addUrlPatterns("/*");
  //        registration.addInitParameter("paramName", "paramValue");
  //        registration.setName("testFilter");
  //        registration.setOrder(2);
  //        return registration;
  //    }
}
