package com.wsfPro.controllers.system;

import com.wsfPro.controllers.base.BaseController;
import com.wsfPro.controllers.dto.system.SysLevelDto;
import com.wsfPro.core.CrudService;
import com.wsfPro.core.system.SysLevelService;
import com.wsfPro.util.controllerUtil.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("sysLevel/{mId}")
public class SysLevelController extends BaseController implements CrudService<SysLevelDto, String> {
  @Autowired private SysLevelService sysLevelService;

  @Override
  protected JsonResult onLoad() {
    return null;
  }

  @GetMapping("addSysLevel")
  @Override
  public JsonResult add(SysLevelDto sysLevelItem) {

    return sysLevelService.addSysLevel(sysLevelItem);
  }

  @GetMapping("delByIdSysLevel")
  @Override
  public JsonResult delById(String s) {
    return null;
  }

  @GetMapping("updSysLevel")
  @Override
  public JsonResult upd(SysLevelDto sysLevelDto) {
    return null;
  }

  @RequestMapping("findByIdSysLevel")
  @Override
  public JsonResult findById(String s) {
    return null;
  }
}
