package com.wsfPro.controllers.system;

import com.wsfPro.controllers.dto.system.InsertSysUserDto;
import com.wsfPro.core.system.SysUserService;
import com.wsfPro.entities.system.SysUserEntity;
import com.wsfPro.util.controllerUtil.ControllerUtil;
import com.wsfPro.util.controllerUtil.JsonResult;
import com.wsfPro.util.mail.MailRecipient;
import com.wsfPro.util.mail.MailSendUtil;
import com.wsfPro.util.mail.TemplateMessage;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.concurrent.Future;

@Api(value = "用户")
@RestController
@RequestMapping("sysUser")
public class SysUserController {
  @Autowired private SysUserService sysUserService;

  @Autowired private MailSendUtil mailSendUtil;

  @PostMapping("addUpdSysUser")
  public JsonResult saveUpdUser(@RequestBody InsertSysUserDto insertSysUserDto) {
    JsonResult jsonResult = insertSysUserDto.checkName("用户名");
    if (jsonResult != null) {
      return jsonResult;
    }
    if (!ControllerUtil.isMailNo(insertSysUserDto.getEmail())) {
      return JsonResult.putFail("邮箱格式错误");
    }
    if (!ControllerUtil.isMobileNO(insertSysUserDto.getMobile())) {
      return JsonResult.putFail("手机号格式错误");
    }
    if (!ControllerUtil.isPhoneNo(insertSysUserDto.getPhone())) {
      return JsonResult.putFail("电话号码格式错误");
    }

    //    年龄，生日由出生日期设置
    SysUserEntity sysUserEntity = insertSysUserDto.createInsertSysUser();
    JsonResult jsonResult1 = sysUserService.saveOrUpdate(sysUserEntity);
    String param = "127.0.0.1:8081/sysUser/activeUser?activeCode=" + sysUserEntity.getId();
    File[] files = new File[2];
    files[0] = new File("C:\\Users\\WSF\\Desktop\\timg.jpg");
    files[1] = new File("C:\\Users\\WSF\\Desktop\\宽带.txt");
    File contentImage = new File("C:\\Users\\WSF\\Desktop\\timg.jpg");
    TemplateMessage templateMessage = new TemplateMessage();
    templateMessage.setMessageStatus("status");
    templateMessage.setMessageCode("code");
    templateMessage.setCause(param);
    String content = mailSendUtil.readTemplate(templateMessage, "mail/message.ftl");
    MailRecipient mailRecipient =
        MailRecipient.getMailInfo(
            "1426256791@qq.com", null, null, "激活邮件", content, contentImage, "a", files);
    Future<Boolean> task2 = mailSendUtil.sendMail(mailRecipient);
    return jsonResult1;
  }

  @GetMapping("activeUser")
  public JsonResult activeUser(String activeCode) {
    return sysUserService.activeCode(activeCode);
  }
}
