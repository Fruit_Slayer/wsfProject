package com.wsfPro.controllers.system;

import com.wsfPro.controllers.base.BaseController;
import com.wsfPro.controllers.dto.system.SysRoleDto;
import com.wsfPro.core.system.SysRoleService;
import com.wsfPro.util.controllerUtil.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("sysRole/{mId}")
public class SysRoleController extends BaseController {
  @Autowired private SysRoleService sysRoleService;

  @Override
  protected JsonResult onLoad() {
    return JsonResult.putSuccess(sysRoleService.findData(null));
  }

  @PostMapping("addSysRole")
  public JsonResult addSysRole(@Valid @RequestBody SysRoleDto roleItem, BindingResult result) {
    JsonResult jsonResult = super.validation(result);
    //     roleItem.checkLevel();
    if (jsonResult != null) {
      return jsonResult;
    }
    jsonResult = roleItem.checkNumber("上级权限id", roleItem.getParentId());

    if (jsonResult != null) {
      return jsonResult;
    }
    return sysRoleService.addSysRole(roleItem);
  }

  @GetMapping("delSysRoleById")
  public JsonResult delRoleById(String id) {
    return null;
  }

  @PostMapping("updSysRole")
  public JsonResult updRole(@RequestBody @Valid SysRoleDto roleItem) {
    JsonResult jsonResult = roleItem.checkNumber("上级权限id", roleItem.getParentId());
    if (jsonResult != null) {
      return jsonResult;
    }
    return sysRoleService.updRole(roleItem);
  }

  @GetMapping("findSysRoleById")
  public JsonResult findRoleById(String id) {
    return null;
  }

  //    @RequestMapping("add")
  //    public JsonResult addRole(@RequestBody SysRoleItem roleItem){
  //        return  sysRoleService.save(roleItem.getSysRoleEntity(new SysRoleEntity()));
  //    }
  //    @RequestMapping("upd")
  //    public JsonResult updRole(@RequestBody SysRoleItem roleItem){
  //        return  sysRoleService.save(roleItem.getSysRoleEntity(new SysRoleEntity()));
  //    }
  //    @RequestMapping("del")
  //    public JsonResult delRole(@RequestBody SysRoleItem roleItem){
  //        return  sysRoleService.save(roleItem.getSysRoleEntity(new SysRoleEntity()));
  //    }

}
