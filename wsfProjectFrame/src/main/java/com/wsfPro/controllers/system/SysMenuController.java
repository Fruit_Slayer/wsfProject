package com.wsfPro.controllers.system;

import com.wsfPro.controllers.base.BaseController;
import com.wsfPro.controllers.dto.RequestDto;
import com.wsfPro.controllers.dto.UserDto;
import com.wsfPro.controllers.dto.system.SysMenuDto;
import com.wsfPro.core.system.SysMenuService;
import com.wsfPro.entities.system.SysMenuEntity;
import com.wsfPro.security.config.SecurityPrincipalContext;
import com.wsfPro.util.controllerUtil.JsonConstans;
import com.wsfPro.util.controllerUtil.JsonResult;
import com.wsfPro.util.mail.MailSendUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.TreeSet;

@Api(value = "菜单")
@RestController
@RequestMapping("sysMenu/{mId}")
public class SysMenuController extends BaseController<SysMenuEntity>
//    implements CrudService<HashMap<String, Object>, String>
{
  @Autowired private MailSendUtil mailSendUtil;
  @Autowired private SysMenuService sysMenuService;

  // 登陆者的权限菜单信息
  @ApiOperation(value = "菜单详情展示", notes = "显示当前用户所有菜单")
  @Override
  public JsonResult onLoad() {
    //    return JsonResult.putSuccess(SysUserUtils.getCurrentSysUser().getMenus());
    TreeSet<SysMenuDto> sysMenuDtos =
        SysMenuDto.createSysMenuDtoSet((TreeSet<SysMenuEntity>) sysMenuService.findData(null));
    return JsonResult.putSuccess(sysMenuDtos);
  }

  @GetMapping("dtoFxTest")
  public JsonResult str(@RequestBody RequestDto<UserDto> requestDto) {
    return JsonResult.putSuccess(requestDto);
  }

  @ApiOperation(value = "添加菜单", notes = "post提交json数据插入菜单")
  @ApiImplicitParam(
      name = "sysMenuDto",
      value = "菜单item",
      required = true,
      dataType = "SysMenuDto",
      paramType = "body")
  @PostMapping("addSysMenu")
  public JsonResult addSysMenu(@RequestBody SysMenuDto sysMenuDto) {
    JsonResult jsonResult = checkParam(sysMenuDto);
    if (jsonResult != null) {
      return jsonResult;
    }
    return sysMenuService.insertUpdMenu(sysMenuDto);
  }

  @ApiOperation(value = "更新菜单", notes = "更新菜单，菜单权限")
  @ApiImplicitParam(
      name = "sysMenuDto",
      value = "菜单item",
      required = true,
      dataType = "SysMenuDto",
      paramType = "body")
  @PostMapping("updSysMenu")
  public JsonResult updSysMenu(@RequestBody @Valid SysMenuDto sysMenuDto, BindingResult result) {
    JsonResult jsonResult = validation(result);
    if (jsonResult != null) {
      return jsonResult;
    }
    jsonResult = sysMenuDto.checkNumber("菜单id", sysMenuDto.getId());
    if (jsonResult != null) {
      return jsonResult;
    }
    jsonResult = checkParam(sysMenuDto);
    if (jsonResult != null) {
      return jsonResult;
    }
    sysMenuDto.setName(sysMenuDto.getName().trim());

    return sysMenuService.insertUpdMenu(sysMenuDto);
  }

  private JsonResult checkParam(SysMenuDto sysMenuDto) {
    JsonResult jsonResult = sysMenuDto.checkName("菜单名");
    if (jsonResult != null) {
      return jsonResult;
    }
    jsonResult = sysMenuDto.checkCode("菜单编号");
    if (jsonResult != null) {
      return jsonResult;
    }
    jsonResult = sysMenuDto.checkUrl();
    if (jsonResult != null) {
      return jsonResult;
    }
    if (sysMenuDto.getSort() > 0) {
      return JsonResult.putFail("排序无法小于0");
    }
    sysMenuDto.setUrl(sysMenuDto.getUrl().trim());
    sysMenuDto.setCode(sysMenuDto.getCode().trim());
    sysMenuDto.setName(sysMenuDto.getName().trim());
    return null;
  }

  @GetMapping("delSysMenuById")
  public JsonResult delSysMenuById(String s) {
    if (SecurityPrincipalContext.getUserDetails().getAuthorities().size() == 1
        && SecurityPrincipalContext.getUserDetails()
            .getAuthorities()
            .iterator()
            .next()
            .getAuthority()
            .equals("ROLE_admin")) {
      try {
        sysMenuService.delete(s, new HashMap<String, Object>());
      } catch (Exception e) {
        JsonResult.putFail(e.toString());
      }
      return JsonResult.putSuccess();
    }
    return JsonResult.putFail(JsonConstans.ERR_NOTVALID_AUTH);
  }

  @GetMapping("findSysMenuById")
  public JsonResult findById(String s) {
    return null;
  }
}
