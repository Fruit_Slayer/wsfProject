package com.wsfPro.controllers.item;

public class Msg {
  private String msg;
  private Long key;

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public Long getKey() {
    return key;
  }

  public void setKey(Long key) {
    this.key = key;
  }
}
