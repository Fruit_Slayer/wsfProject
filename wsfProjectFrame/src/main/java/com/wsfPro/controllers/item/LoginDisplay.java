package com.wsfPro.controllers.item;

public class LoginDisplay extends SuperDisplay {
  private LoginDisplay() {
    super();
  };

  public LoginDisplay(long id) throws Exception {
    super(id);
  }

  private String nickName;
  private String passWord;
  // 应该定义枚举消息类型替代消息属性
  private String msg1;

  //	public Long set(AccountEntity ae){
  //		this.nickName=ae.getNickName();
  //		this.passWord=ae.getPassWord();
  //		return null;
  //	}
  //
  public String getNickName() {
    return nickName;
  }

  public void setNickName(String nickName) {
    this.nickName = nickName;
  }

  public String getPassWord() {
    return passWord;
  }

  public void setPassWord(String passWord) {
    this.passWord = passWord;
  }

  public String getMsg1() {
    return msg1;
  }

  public void setMsg1(String msg1) {
    this.msg1 = msg1;
  }
}
