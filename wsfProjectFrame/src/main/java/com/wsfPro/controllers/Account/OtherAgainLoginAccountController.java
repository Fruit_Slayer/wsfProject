//package com.wsfPro.controllers.Account;
//
//import com.wsfPro.controllers.item.LoginDisplay;
//import com.wsfPro.controllers.item.Msg;
//import com.wsfPro.util.GoEasyUtil;
//import com.wsfPro.util.controllerUtil.ControllerUtil;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//@Controller
//@RequestMapping("otherAgainLogin")
//public class OtherAgainLoginAccountController {
//    @Resource(name = "goEasyUtil")
//    GoEasyUtil goEasyUtil;
//
//    @ResponseBody
//    @RequestMapping(value = "{key}/check", method = RequestMethod.GET)
//    public Object checkNewLogin(
//            @PathVariable Long key, HttpServletRequest request, HttpServletResponse response)
//            throws Exception {
//        // response.setHeader("Pragma", "No-cache");
//        // response.setHeader("Cache-Control", "no-cache");
//        // response.setDateHeader("Expires", 0);
//        // for(Entry<Long, HttpSession> set: this.lId_session.entrySet()){
//        // System.out.println("key"+set.getKey()+" value:"+set.getValue());
//        //
//        // }
//
//        Msg msg = new Msg();
//        HttpSession session = request.getSession();
//        LoginDisplay ld = (LoginDisplay) session.getAttribute("login");
//        if (ld == null) {
//            // 检查是否有新登陆者
//            String orderLoginMsg = (String) session.getAttribute("orderLoginMsg");
//            // 不管有没有都remove
//            session.removeAttribute("orderLoginMsg");
//            msg.setMsg(orderLoginMsg);
//
//            // goEasyUtil.sendMsg(orderLoginMsg);
//        } else {
//            System.out.println("SessionID:" + session.getId() + " NickName:" + ld.getNickName());
//            // 没有被踢掉则继续发送key
//            ControllerUtil.setRequestKeyValue(request, msg);
//        }
//        return msg;
//    }
//}
