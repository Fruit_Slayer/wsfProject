package com.wsfPro.controllers.test;

import org.springframework.stereotype.Component;

// 在控制层声明的切点aop不起作用
@Component(value = "testAop")
public class TestAop {

  public void getTestAop() {
    System.out.println("exe TestAop");
  }
}
