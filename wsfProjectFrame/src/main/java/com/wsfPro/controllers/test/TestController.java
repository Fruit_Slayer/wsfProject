package com.wsfPro.controllers.test;

import com.wsfPro.core.test.CustomerManager;
import com.wsfPro.services.test.CheckLoginService;
import com.wsfPro.services.test.CustomerManagerImpl;
import com.wsfPro.services.test.TestAop2;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("com/wsfPro/test")
public class TestController {
  @Resource private CustomerManagerImpl customerManagerImpl;
  @Resource private TestAop2 testAop2;
  @Resource private TestAop testAop;
  @Resource private CheckLoginService checkLoginService;

  @RequestMapping("suc")
  public String test(Model model) {
    model.addAttribute("suc", "success!tassd");

    // CustomerManager customerManagerImpl=new CustomerManagerImpl();
    customerManagerImpl.getCustomerById(2015);

    return "com/wsfPro/test/test";
  }

  @RequestMapping("test2")
  public String get(Model model, HttpServletRequest request, HttpServletResponse response) {

    if (checkLoginService.checkLogin(request)) {
      // try {
      // response.sendRedirect("/login.jsp");
      //
      // } catch (IOException e1) {
      // e1.printStackTrace();
      // }
      return "com/wsfPro/test/login";
    }
    model.addAttribute("get", "success aop");

    customerManagerImpl.get22();
    testAop2.getTestAop2();
    testAop.getTestAop();

    return "com/wsfPro/test/test";
  }

  @RequestMapping("testLogin")
  public String testLogin() {
    return "com/wsfPro/test/login";
  }

  public static void main(String[] args) {
    System.out.println("Hello Spring AOP!");
    BeanFactory factory = new ClassPathXmlApplicationContext("applicationContext.xml");
    CustomerManager customerManager = (CustomerManager) factory.getBean("customerManager");
    customerManager.getCustomerById(2015);
  }
}
