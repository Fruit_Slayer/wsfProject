package com.wsfPro.controllers.session;

import com.wsfPro.controllers.item.LoginDisplay;
import com.wsfPro.core.login.LoginSession_WebSocketSessionInterface;

import javax.servlet.http.HttpSession;
import javax.websocket.Session;
import java.util.HashMap;
import java.util.Map;

// @Component(value = "loginSession_WebSocketSession")
public class LoginSession_WebSocketSession implements LoginSession_WebSocketSessionInterface {

  private static volatile LoginSession_WebSocketSession session = null;
  // 登陆者对应一个httpSession
  private Map<Long, HttpSession> loginId_loginSession = null;
  private Map<Long, Session> loginId_webSocket = new HashMap<Long, Session>();
  private Map<String, HttpSession> sessionId_session = new HashMap<String, HttpSession>();

  private LoginSession_WebSocketSession() {}

  public static LoginSession_WebSocketSession getSession() {
    if (session == null) {
      synchronized (LoginSession_WebSocketSession.class) {
        if (session == null) {
          session = new LoginSession_WebSocketSession();
        }
      }
    }
    return session;
  }

  public Map<Long, HttpSession> getLoginId_loginSession() {
    if (loginId_loginSession == null) {
      synchronized (LoginSession_WebSocketSession.class) {
        if (loginId_loginSession == null) {
          loginId_loginSession = new HashMap<Long, HttpSession>();
        }
      }
    }
    return loginId_loginSession;
  }

  public Map<String, HttpSession> getSessionId_session() {
    if (sessionId_session == null) {
      synchronized (LoginSession_WebSocketSession.class) {
        if (sessionId_session == null) {
          sessionId_session = new HashMap<String, HttpSession>();
        }
      }
    }
    return sessionId_session;
  }

  public Map<Long, Session> getLoginId_webSocket() {
    if (loginId_webSocket == null) {
      synchronized (LoginSession_WebSocketSession.class) {
        if (loginId_webSocket == null) {
          loginId_webSocket = new HashMap<Long, Session>();
        }
      }
    }
    return loginId_webSocket;
  }

  @Override
  public HttpSession getHttpSessionByLid(long id) {
    return this.getLoginId_loginSession().get(id);
  }

  @Override
  public Session getWebSocketSessionByLid(long id) {
    return this.getLoginId_webSocket().get(id);
  }

  @Override
  public void saveLoginSession(long id, HttpSession session) {
    this.getLoginId_loginSession().put(id, session);
    this.getSessionId_session().put(session.getId(), session);
  }

  @Override
  public void saveWebSocketSession(long id, Session session) {
    this.getLoginId_webSocket().put(id, session);
  }

  // @Override
  // public void removeLoginSession_WebSocketSession(long id) {
  // this.getSessionId_session().remove(
  // this.getLoginId_loginSession().remove(id).getId());
  // }

  @Override
  public void delWebSocketSessionByLid(long id) {
    this.getLoginId_webSocket().remove(id);
  }

  @Override
  public HttpSession getHttpSessionBySid(String sid) {
    return this.getSessionId_session().get(sid);
  }

  @Override
  public void delSessionBySid(String sid) {
    LoginDisplay login =
        (LoginDisplay) this.getSessionId_session().remove(sid).getAttribute("login");
    this.getLoginId_webSocket().remove(login.DecryptByDisplay());
    this.getLoginId_loginSession().remove(login.DecryptByDisplay());
  }

  // @Override
  //  // public Long getLoginIdBySession(HttpSession session) {
  //  // return this.getLoginSession_loginId().get(session);
  //  // }

}
