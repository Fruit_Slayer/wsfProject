package com.wsfPro.controllers.base;

import com.wsfPro.entities.base.BaseEntity;
import com.wsfPro.util.controllerUtil.JsonResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
// @RequestMapping("base/{mId}")
public abstract class BaseController<E extends BaseEntity> {

  @GetMapping("index")
  protected abstract JsonResult onLoad();

  protected JsonResult validation(BindingResult result) {
    if (result.hasErrors()) {
      StringBuffer sb = new StringBuffer();
      for (int i = 0; i < result.getAllErrors().size(); i++) {
        ObjectError error = result.getAllErrors().get(i);

        FieldError fieldError = (FieldError) error;
        // 属性
        String field = fieldError.getField();
        // 错误信息
        String message = fieldError.getDefaultMessage();
        if (i == result.getAllErrors().size() - 1) {
          sb.append(message);
          return JsonResult.putFail(sb.toString());
        }
        sb.append(message + ",");
      }
      //      result
      //          .getAllErrors()
      //          .forEach(
      //              (error) -> {
      //                FieldError fieldError = (FieldError) error;
      //
      //                String field = fieldError.getField();
      //                // 错误信息
      //                String message = fieldError.getDefaultMessage();
      //              });
    }
    return null;
  }
  //    protected SysRoleEntity getMaxLevelUserRole(){
  //        List<SysRoleEntity> roleList= SysUserUtils.getCurrentSysUser().getRoles();
  //        //        账户有多个权限则按最大的算
  //        if (roleList.size() > 0) {
  //            SysRoleEntity bigRole= roleList.get(0);
  //            for (int i =1; i < roleList.size() ; i++) {
  //                SysRoleEntity roleNext= roleList.get(i);
  //                if (bigRole.getLevel() > roleNext.getLevel()) {
  //                    bigRole = roleNext;
  //                }
  //            }
  //
  //            return bigRole;
  //        }
  //        return null;
  //    }
}
