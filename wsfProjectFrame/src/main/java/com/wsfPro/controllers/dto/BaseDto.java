package com.wsfPro.controllers.dto;

import com.wsfPro.core.base.ResponseCodeConstans;
import com.wsfPro.core.base.ValidationUtils;
import com.wsfPro.util.StringUtils;
import com.wsfPro.util.controllerUtil.JsonResult;
import lombok.Data;

import java.util.Objects;

@Data
public abstract class BaseDto extends AbstractDto {
  private String name;

  protected <T extends BaseDto> JsonResult validationEntity(T dto) {
    String isVali = ValidationUtils.validation(dto);
    if (!isVali.equals(ResponseCodeConstans.SUCCESS)) {
      return JsonResult.putFail(isVali);
    }
    return null;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof BaseDto)) {
      return false;
    }
    BaseDto baseDto = (BaseDto) o;
    return (this.getId().equals(((BaseDto) o).getId()));
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getId(), name);
  }

  //  protected boolean paramIDNameCheck(AbstractItem abstractItem) {
  //    if (!paramIdCheck(abstractItem.getId())) {
  //      return false;
  //    }
  //    return paramNameCheck(abstractItem.getName());
  //  }
  public JsonResult checkNumber(String errorParamName, String numberStr) {
    if (StringUtils.isBlank(numberStr)) {
      return JsonResult.putFail(errorParamName + "不能为空");
    }
    if (!StringUtils.isNumeric(numberStr)) {
      return JsonResult.putFail(errorParamName + "必须为大于1的正整数");
    }
    return null;
  }

  public JsonResult checkName(String errorParamName) {
    if (StringUtils.isBlank(name)) {
      return JsonResult.putFail(errorParamName + "不能为空");
    }
    int length = 255;
    if (name.length() > length) {
      return JsonResult.putFail(errorParamName + "长度超过限制,长度不能超过" + length + "字符");
    }
    return null;
  }
}
