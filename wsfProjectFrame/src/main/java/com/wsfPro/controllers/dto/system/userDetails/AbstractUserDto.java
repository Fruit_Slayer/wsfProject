package com.wsfPro.controllers.dto.system.userDetails;

import com.wsfPro.controllers.dto.BaseDto;
import lombok.Data;

@Data
public abstract class AbstractUserDto extends BaseDto {
  private String password;
}
