package com.wsfPro.controllers.dto;

import lombok.Data;

import java.util.Objects;

@Data
public abstract class BaseStatusDto extends BaseDto {
  private int status;

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), status);
  }
}
