package com.wsfPro.controllers.dto.system;

import com.wsfPro.controllers.dto.BaseCodeDto;
import com.wsfPro.entities.system.SysRoleEntity;

public class SysRoleDto extends BaseCodeDto {
  private String parentId;
  //  private Integer level;

  public SysRoleEntity createSysRoleEntity() {
    SysRoleEntity role = new SysRoleEntity();
    role.setName(this.getName());
    role.setCode(this.getCode());
    role.setParentId(this.getParentId());
    //    role.setLevel(this.getLevel());
    return role;
  }

  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  //  public Integer getLevel() {
  //    return level;
  //  }
  //
  //  public void setLevel(Integer level) {
  //    this.level = level;
  //  }
  //
  //  public JsonResult checkLevel() {
  //    if (this.level == null) {
  //      return JsonResult.putFail("级别不能为空");
  //    }
  //    if (this.level < 1) {
  //      return JsonResult.putFail("级别不能小于1");
  //    }
  //    return null;
  //  }
}
