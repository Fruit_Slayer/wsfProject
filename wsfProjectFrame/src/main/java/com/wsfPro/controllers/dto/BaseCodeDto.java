package com.wsfPro.controllers.dto;

import com.wsfPro.util.StringUtils;
import com.wsfPro.util.controllerUtil.JsonResult;
import lombok.Data;

import java.util.Objects;

@Data
public abstract class BaseCodeDto extends BaseStatusDto {
  private String code;

  public JsonResult checkCode(String errorParamName) {
    if (StringUtils.isBlank(code)) {
      return JsonResult.putFail(errorParamName + "不能为空");
    }
    if (!StringUtils.isNumeric(code)) {
      return JsonResult.putFail(errorParamName + "必须为正整数");
    }
    if (Long.valueOf(code) < 1) {
      return JsonResult.putFail(errorParamName + "必须为大于1的正整数");
    }
    return null;
  }

  @Override
  public int hashCode() {

    return Objects.hash(super.hashCode(), code);
  }
}
