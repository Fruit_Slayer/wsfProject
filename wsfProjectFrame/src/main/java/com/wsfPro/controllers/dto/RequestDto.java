package com.wsfPro.controllers.dto;

public class RequestDto<T> {

  private T entity;

  private String token;

  private PageDto pageDto;
}
