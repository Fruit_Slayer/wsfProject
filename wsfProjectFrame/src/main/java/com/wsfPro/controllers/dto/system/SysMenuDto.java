package com.wsfPro.controllers.dto.system;

import com.wsfPro.controllers.dto.BaseCodeDto;
import com.wsfPro.entities.system.SysMenuEntity;
import com.wsfPro.util.StringUtils;
import com.wsfPro.util.controllerUtil.JsonResult;
import lombok.Data;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

@Data
public class SysMenuDto extends BaseCodeDto {
  private String parentId;
  private TreeSet<SysMenuDto> childMenus;
  private List<String> roleIdList;
  private String url;
  private int level;
  private int sort;

  private SysMenuDto(
      String id,
      String parentId,
      TreeSet<SysMenuDto> childMenus,
      String name,
      int status,
      String code,
      String url,
      int level,
      int sort) {
    this.setId(id);
    this.parentId = parentId;
    this.childMenus = childMenus;
    this.setName(name);
    this.setStatus(status);
    this.setCode(code);
    this.url = url;
    this.level = level;
    this.sort = sort;
  }

  private SysMenuDto() {}

  public static SysMenuDto createSysMenuDto(SysMenuEntity sysMenuEntity) {
    TreeSet<SysMenuEntity> childMenusSet = sysMenuEntity.getChildMenus();
    TreeSet<SysMenuDto> childenMenuDtoSet = null;
    if (childMenusSet != null && childMenusSet.size() != 0) {
      childenMenuDtoSet =
          new TreeSet<SysMenuDto>(
              new Comparator<SysMenuDto>() {
                @Override
                public int compare(SysMenuDto o1, SysMenuDto o2) {
                  if (o1.getId().equals(o2.getId())) {
                    return 0;
                  }
                  if (o1.getSort() - o2.getSort() == 0) {
                    return 1;
                  }
                  return o1.getSort() - o2.getSort();
                }
              });
      Iterator<SysMenuEntity> childMenusSetIterator = childMenusSet.iterator();
      while (childMenusSetIterator.hasNext()) {
        SysMenuEntity childMenu = childMenusSetIterator.next();
        childenMenuDtoSet.add(createSysMenuDto(childMenu));
      }
    }
    SysMenuDto sysMenuDto =
        new SysMenuDto(
            sysMenuEntity.getId(),
            sysMenuEntity.getParentId(),
            childenMenuDtoSet,
            sysMenuEntity.getName(),
            sysMenuEntity.getStatus(),
            sysMenuEntity.getCode(),
            sysMenuEntity.getUrl(),
            sysMenuEntity.getLevel(),
            sysMenuEntity.getSort());
    return sysMenuDto;
  }

  public static TreeSet<SysMenuDto> createSysMenuDtoSet(TreeSet<SysMenuEntity> menuSet) {
    TreeSet<SysMenuDto> sysMenuDtoSet = null;
    if (menuSet != null && menuSet.size() > 0) {
      Iterator<SysMenuEntity> sysMenuEntityIterator = menuSet.iterator();
      sysMenuDtoSet =
          new TreeSet<SysMenuDto>(
              new Comparator<SysMenuDto>() {
                @Override
                public int compare(SysMenuDto o1, SysMenuDto o2) {
                  if (o1.getId().equals(o2.getId())) {
                    return 0;
                  }
                  if (o1.getSort() - o2.getSort() == 0) {
                    return 1;
                  }
                  return o1.getSort() - o2.getSort();
                }
              });
      while (sysMenuEntityIterator.hasNext()) {

        SysMenuEntity sysMenuEntity = sysMenuEntityIterator.next();
        sysMenuDtoSet.add(SysMenuDto.createSysMenuDto(sysMenuEntity));
      }
    }
    return sysMenuDtoSet;
  }

  //    private String icon;

  //    private Integer sequence;
  //
  //    private Boolean isExpand;
  //
  //    private String component;
  //
  //    private Boolean isHidden;
  //
  //    private String redirect;
  //
  //    private Integer type;

  public TreeSet<SysMenuDto> getChildMenus() {
    return childMenus;
  }

  public void setChildMenus(TreeSet<SysMenuDto> childMenus) {
    if (childMenus == null) {
      childMenus =
          new TreeSet<SysMenuDto>(
              new Comparator<SysMenuDto>() {
                @Override
                public int compare(SysMenuDto o1, SysMenuDto o2) {
                  if (o1.getId().equals(o2.getId())) {
                    return 0;
                  }
                  return o1.getSort() - o2.getSort();
                }
              });
    }
    this.childMenus = childMenus;
  }

  public JsonResult checkUrl() {
    int length = 500;
    if (StringUtils.isBlank(url)) {
      return JsonResult.putFail("菜单url不能为空");
    }
    if (url.length() > length) {
      return JsonResult.putFail("菜单url长度超过限制,长度不能超过" + length + "个字符");
    }
    return null;
  }
}
