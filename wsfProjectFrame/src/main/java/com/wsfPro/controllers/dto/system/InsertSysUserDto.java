package com.wsfPro.controllers.dto.system;

import com.wsfPro.controllers.dto.system.userDetails.AbstractUserDto;
import com.wsfPro.entities.system.SysUserEntity;
import com.wsfPro.util.StringUtils;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class InsertSysUserDto extends AbstractUserDto {
  private String nickName;
  private String identityCard;
  private Integer age;
  private boolean sex;
  private Timestamp birthTime;
  private String email;
  private String phone;
  private String mobile;
  private String photo;

  public SysUserEntity createInsertSysUser() {
    SysUserEntity sysUserEntity = new SysUserEntity();
    sysUserEntity.setEnabled(false);
    if (StringUtils.isNotBlank(this.getId())) {
      sysUserEntity.setId(this.getId());
    }
    sysUserEntity.setUsename(this.getName());
    sysUserEntity.setPassword(this.getPassword());
    sysUserEntity.setName(this.getNickName());
    sysUserEntity.setIdentityCard(this.getIdentityCard());
    sysUserEntity.setAge(this.getAge());
    sysUserEntity.setSex(this.getSex());
    sysUserEntity.setBirthTime(this.getBirthTime());
    sysUserEntity.setEmail(this.getEmail());
    sysUserEntity.setPhone(this.getPhone());
    sysUserEntity.setMobile(this.getMobile());
    sysUserEntity.setPhoto(this.getPhoto());
    return sysUserEntity;
  }

  public boolean getSex() {
    return this.sex;
  }
}
