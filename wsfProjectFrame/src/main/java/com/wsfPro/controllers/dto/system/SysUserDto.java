package com.wsfPro.controllers.dto.system;

import com.wsfPro.controllers.dto.system.userDetails.UserDetailsImpl;
import com.wsfPro.entities.system.SysMenuEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.TreeSet;

public class SysUserDto extends UserDetailsImpl implements UserDetails {

  private TreeSet<SysMenuDto> menuSet;

  public SysUserDto(
      String id,
      String username,
      String password,
      Collection<SimpleGrantedAuthority> authorities,
      TreeSet<SysMenuEntity> menuSet,
      boolean accountNonExpired,
      boolean accountNonLocked,
      boolean credentialsNonExpired,
      boolean enabled) {
    super(
        id,
        username,
        password,
        authorities,
        accountNonExpired,
        accountNonLocked,
        credentialsNonExpired,
        enabled);
    this.menuSet = SysMenuDto.createSysMenuDtoSet(menuSet);
  }

  public TreeSet<SysMenuDto> getMenuSet() {
    return menuSet;
  }
}
