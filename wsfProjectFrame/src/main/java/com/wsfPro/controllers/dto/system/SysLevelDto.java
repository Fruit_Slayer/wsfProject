package com.wsfPro.controllers.dto.system;

import com.wsfPro.controllers.dto.BaseCodeDto;
import com.wsfPro.entities.system.SysLevelEntity;

public class SysLevelDto extends BaseCodeDto {
  private int level;

  public int getLevel() {
    return level;
  }

  public void setLevel(int level) {
    this.level = level;
  }

  public SysLevelEntity getSysLevelEntity() {
    SysLevelEntity level = new SysLevelEntity();
    level.setLevel(this.getLevel());
    level.setName(this.getName());
    return level;
  }
}
