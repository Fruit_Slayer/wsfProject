package com.wsfPro.controllers.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Getter
@Setter
public abstract class AbstractDto implements Serializable {
  @NotBlank(message = "id不能为空")
  private String id;
}
