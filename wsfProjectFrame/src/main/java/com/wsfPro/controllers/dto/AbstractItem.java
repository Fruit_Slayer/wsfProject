// package com.wsfPro.dto;
//
// import com.wsfPro.core.base.ResponseCodeConstans;
// import com.wsfPro.core.base.ValidationUtils;
// import com.wsfPro.util.StringUtils;
// import com.wsfPro.util.controllerUtil.JsonResult;
//
// import javax.validation.constraints.NotBlank;
// import java.io.Serializable;
// import java.sql.Timestamp;
// import java.util.Objects;
//
// public abstract class AbstractItem implements Serializable {
//
//  @NotBlank(message = "id不能为空")
//  private String id;
//
//  private String code;
//  private String name;
//  private Timestamp createDate;
//  private Timestamp updateDate;
//
//  protected <T extends AbstractItem> JsonResult validationEntity(T item) {
//    String isVali = ValidationUtils.validation(item);
//    if (!isVali.equals(ResponseCodeConstans.SUCCESS)) {
//      return JsonResult.putFail(isVali);
//    }
//    return null;
//  }
//
//  public String getId() {
//    return id;
//  }
//
//  public void setId(String id) {
//    this.id = id;
//  }
//
//  public String getCode() {
//    return code;
//  }
//
//  public void setCode(String code) {
//    this.code = code;
//  }
//
//  public String getName() {
//    return name;
//  }
//
//  public void setName(String name) {
//    this.name = name;
//  }
//
//  public Timestamp getCreateDate() {
//    return createDate;
//  }
//
//  public void setCreateDate(Timestamp createDate) {
//    this.createDate = createDate;
//  }
//
//  public Timestamp getUpdateDate() {
//    return updateDate;
//  }
//
//  public void setUpdateDate(Timestamp updateDate) {
//    this.updateDate = updateDate;
//  }
//
//  @Override
//  public boolean equals(Object o) {
//    if (this == o) {
//      return true;
//    }
//    if (!(o instanceof AbstractItem)) {
//      return false;
//    }
//    AbstractItem that = (AbstractItem) o;
//    return Objects.equals(id, that.id)
//        && Objects.equals(name, that.name)
//        && Objects.equals(createDate, that.createDate)
//        && Objects.equals(updateDate, that.updateDate);
//  }
//
//  @Override
//  public int hashCode() {
//
//    return Objects.hash(id, name, createDate, updateDate);
//  }
//
//  //  protected boolean paramIDNameCheck(AbstractItem abstractItem) {
//  //    if (!paramIdCheck(abstractItem.getId())) {
//  //      return false;
//  //    }
//  //    return paramNameCheck(abstractItem.getName());
//  //  }
//  public JsonResult checkNumber(String errorParamName, String numberStr) {
//    if (StringUtils.isBlank(numberStr)) {
//      return JsonResult.putFail(errorParamName + "不能为空");
//    }
//    if (!StringUtils.isNumeric(numberStr)) {
//      return JsonResult.putFail(errorParamName + "必须为大于1的正整数");
//    }
//    return null;
//  }
//
//  public JsonResult checkCode(String errorParamName) {
//    if (StringUtils.isBlank(code)) {
//      return JsonResult.putFail(errorParamName + "不能为空");
//    }
//    if (!StringUtils.isNumeric(code)) {
//      return JsonResult.putFail(errorParamName + "必须为正整数");
//    }
//    if (Long.valueOf(code) < 1) {
//      return JsonResult.putFail(errorParamName + "必须为大于1的正整数");
//    }
//    return null;
//  }
//
//  public JsonResult checkName(String errorParamName) {
//    if (StringUtils.isBlank(name)) {
//      return JsonResult.putFail(errorParamName + "不能为空");
//    }
//    int length = 255;
//    if (name.length() > length) {
//      return JsonResult.putFail(errorParamName + "长度超过限制,长度不能超过" + length + "字符");
//    }
//    return null;
//  }
// }
