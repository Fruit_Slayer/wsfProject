//package com.wsfPro.filter;
//
//import com.wsfPro.util.urlUtil.UrlUtil;
//import org.springframework.core.annotation.Order;
//import org.springframework.web.filter.GenericFilterBean;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
///**
// * 过滤url
// */
//@Order(1)
//@WebFilter(filterName = "urlFilter", urlPatterns = "/*")
//public class UrlFilter extends GenericFilterBean {
//    @Override
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//
//        final HttpServletRequest request = (HttpServletRequest) servletRequest;
//        final HttpServletResponse response = (HttpServletResponse) servletResponse;
//        if (!UrlUtil.filterUrl(request.getRequestURI())) {
//            filterChain.doFilter(servletRequest, servletResponse);
//        }
//    }
//}
