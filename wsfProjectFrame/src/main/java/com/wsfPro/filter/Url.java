package com.wsfPro.filter;

public enum Url {
  // 利用构造函数传参
  URL1("/js"),
  URL2("/css"),
  URL3("/images"),
  URL4("/login/login"),
  URL5("/login/loginPage"),
  URL6("error.vm");
  // 定义私有变量
  private String url;

  // 构造函数，枚举类型只能为私有
  private Url(String url) {
    this.url = url;
  }

  @Override
  public String toString() {
    return String.valueOf(this.url);
  }
}
