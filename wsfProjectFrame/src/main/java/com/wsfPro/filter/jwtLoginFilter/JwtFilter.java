//package com.wsfPro.filter.jwtLoginFilter;
//
//import com.wsfPro.util.ConstantsEnum;
//import com.wsfPro.util.jwt.JwtUtil;
//import com.wsfPro.util.jwt.JwttokenconfigBean;
//import com.wsfPro.util.urlUtil.UrlUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.annotation.Order;
//import org.springframework.web.filter.GenericFilterBean;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.Map;
//
//@Order(1)
//// 重点
//@WebFilter(filterName = "jwtFilter", urlPatterns = "/*")
//public class JwtFilter extends GenericFilterBean {
//    @Autowired
//    private JwtUtil jwtUtil;
//    @Autowired
//    private JwttokenconfigBean jwttokenconfigBean;
//
//    /**
//     * Reserved claims（保留），它的含义就像是编程语言的保留字一样，属于JWT标准里面规定的一些claim。JWT标准里面定好的claim有：
//     *
//     * <p>iss(Issuser)：代表这个JWT的签发主体； sub(Subject)：代表这个JWT的主体，即它的所有人； aud(Audience)：代表这个JWT的接收对象；
//     * exp(Expiration time)：是一个时间戳，代表这个JWT的过期时间； nbf(Not
//     * Before)：是一个时间戳，代表这个JWT生效的开始时间，意味着在这个时间之前验证JWT是会失败的； iat(Issued at)：是一个时间戳，代表这个JWT的签发时间； jti(JWT
//     * ID)：是JWT的唯一标识。
//     *
//     * @param servletRequest
//     * @param servletResponse
//     * @param filterChain
//     * @throws IOException
//     * @throws ServletException
//     */
//    @Override
//    public void doFilter(
//            ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
//            throws IOException, ServletException {
//
//        final HttpServletRequest request = (HttpServletRequest) servletRequest;
//        final HttpServletResponse response = (HttpServletResponse) servletResponse;
//        // 获取请求头信息authorization信息
//        final String authHeader = request.getHeader(jwttokenconfigBean.getHeader());
//        System.out.println("request.getMethod():" + request.getMethod());
//        // 如果是跨域
//        if ("OPTIONS".equals(request.getMethod())) {
//            response.setStatus(HttpServletResponse.SC_OK);
//            filterChain.doFilter(servletRequest, servletResponse);
//        } else {
//            //
//            //            if (authHeader == null || !authHeader.startsWith("bearer;")) {
//            //                try {
//            //                    throw new LoginException("error no bearer");
//            //                } catch (LoginException e) {
//            //                    e.printStackTrace();
//            //                }
//            //            }
//            if (authHeader == null) {
//                if (!UrlUtil.filterUrl(request.getRequestURI())) {
//                    filterChain.doFilter(servletRequest, servletResponse);
//                    return;
//                } else {
//                    response.setHeader("refresh", "3;/login/loginPage.html");
//                    return;
//                }
//            }
//            final String token = authHeader.substring(jwttokenconfigBean.getTokenHead().length());
//
//            //                //如果无法拿到jwt配置实体使用WebApplicationContextUtils生成ing(7);
//            //                if (jwtUtil.getAudience() == null) {
//            //                    BeanFactory factory =
//            // WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
//            //                    jwtUtil.setAudience((Audience) factory.getBean("audience"));
//            //                }
//            // 校验jwtToken
//
//            final Map map = jwtUtil.validateToken(authHeader);
//            if (map == null) {
//                response.setHeader("refresh", "3;/login/loginPage.html");
//                return;
//            }
//
//            response.setHeader("Authorization", authHeader);
//            request.setAttribute(ConstantsEnum.CLAIMS.toString(), map);
//            filterChain.doFilter(servletRequest, servletResponse);
//        }
//    }
//}
