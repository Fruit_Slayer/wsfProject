package com.wsfPro.services.daoImpl; // package services.daoImpl;
//
// import LoginController;
// import Dao;
// import org.apache.log4j.Logger;
// import org.hibernate.Session;
// import org.hibernate.Transaction;
// import org.springframework.stereotype.Repository;
// import util.HibernateUtil;
//
// import javax.annotation.Resource;
// import javax.persistence.EntityManager;
// import javax.persistence.PersistenceContext;
// import java.util.List;
//
// @Repository(value = "dao")
// public class DaoImpl2<T> implements Dao<T> {
//	@PersistenceContext
//	protected EntityManager entityManager;
//		@Resource(name = "hibernateUtil")
//    HibernateUtil hibernateUtil;
//	private static final Logger logger = Logger
//			.getLogger(LoginController.class);
//	@Override
//	public List<T> select(Class<T> t, String hql) {
//		// 每一次操作sql都有一个数据库会话session执行每一次crud都有其各自的事务顾session，transaction不能作为单例注入
//		Session session = hibernateUtil.openSession();
//
////		MDC.put("n", "hibernateUtil");
////		logger.info("");
//		Transaction tr = session.beginTransaction();
//		List<T> res = null;
//		if (hql != null) {
//			res = session.createQuery(hql).list();
//		} else {
//			res = session.createQuery("from " + t.getClass().getName()).list();
//		}
//		tr.commit();
//		session.close();
//		tr = null;
//		return res;
//	}
//
// }
