package com.wsfPro.services.daoImpl;

import com.wsfPro.core.dao.Dao;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository(value = "dao")
public class DaoImpl<T> implements Dao<T> {
  @PersistenceContext protected EntityManager entityManager;
  //		@Resource(name = "hibernateUtil")
  //    HibernateUtil hibernateUtil;
  private static final Logger logger = Logger.getLogger(DaoImpl.class);

  @Override
  public List<T> select(Class<T> t, String hql) {
    //		// 每一次操作sql都有一个数据库会话session执行每一次crud都有其各自的事务顾session，transaction不能作为单例注入
    //		Session session = hibernateUtil.openSession();

    //		MDC.put("n", "hibernateUtil");
    //		logger.info("");
    //		Transaction tr = session.beginTransaction();
    List<T> res = null;
    if (hql != null) {
      //			res = session.createQuery(hql).list();
      res = entityManager.createQuery(hql).getResultList();
      System.out.println(res + "---------------------------------" + res.size());
    } else {
      //			res = session.createQuery("from " + t.getClass().getName()).list();
      res = entityManager.createQuery("from " + t.getClass().getName()).getResultList();
    }
    entityManager.close();
    //		tr.commit();
    //		session.close();
    //		tr = null;
    return res;
  }
}
