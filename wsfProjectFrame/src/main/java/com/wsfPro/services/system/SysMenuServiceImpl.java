package com.wsfPro.services.system;

import com.wsfPro.controllers.dto.system.SysMenuDto;
import com.wsfPro.core.system.SysMenuService;
import com.wsfPro.entities.middle.SysRoleMenuEntity;
import com.wsfPro.entities.system.SysMenuEntity;
import com.wsfPro.entities.system.SysRoleEntity;
import com.wsfPro.repository.system.SysMenuRepository;
import com.wsfPro.repository.system.SysRoleRepository;
import com.wsfPro.repository.system.SysUserRepository;
import com.wsfPro.security.config.SecurityPrincipalContext;
import com.wsfPro.services.base.BaseCodeServiceImpl;
import com.wsfPro.util.EntityUtil;
import com.wsfPro.util.StringUtils;
import com.wsfPro.util.controllerUtil.JsonConstans;
import com.wsfPro.util.controllerUtil.JsonResult;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.*;

/** @author eiven */

/** 非顶级权限下的权限菜单只能在当前拥有该菜单的情况下进行给下级权限做菜单增删改操作 */
@Service
public class SysMenuServiceImpl extends BaseCodeServiceImpl<SysMenuEntity>
    implements SysMenuService {
  @Autowired private SysMenuRepository sysMenuRepository;

  @Autowired private SysRoleRepository sysRoleRepository;

  @Autowired private SysUserRepository sysUserRepository;

  //  private final String STR_ZERO = "0";
  //  @Autowired private BaseRepository<SysMenuEntity> baseRepository;
  //  @Autowired private SysRoleRepository sysRoleRepository;
  //  @Autowired private MiddleTableService middleTableService;
  //  @Autowired private SysMenuRepository sysMenuRepository;
  //  @Autowired private SysRoleMenuRepository sysRoleMenuRepository;

  //  @Override
  //  public List<SysMenuEntity> initMenuList() {
  //    String sql =
  //        "SELECT DISTINCT menu.id,menu.cnName,menu.`code`,menu.enName,menu.icon,\n"
  //            + "
  //	menu.isExpand,menu.type,menu.isUse,menu.parentId,menu.component,menu.isHidden,\n"
  //            + "  menu.remarks,menu.sequence,menu.url,menu.version\n"
  //            + " FROM sys_menu menu \n"
  //            + " INNER JOIN sys_role_menu rm ON rm.menuId = menu.id\n"
  //            + " INNER JOIN sys_role_user ru ON ru.roleId = rm.roleId\n"
  //            + "where ru.userId =:userId ";
  //    Map<String, Object> conditionParams = new HashMap<>(1);
  //    conditionParams.put("userId", SecurityPrincipalContext.getUserDetailsImpl().getId());
  //    Map<String, Object> dataMap =
  //            sysMenuRepository.findPageByNativeQueryFileds(
  //            sql, conditionParams, Transformers.aliasToBean(SysMenuEntity.class), null, null);
  //    if (dataMap != null) {
  //      List<SysMenuEntity> menuList = (List<SysMenuEntity>) dataMap.get(SysConstants.ROWS);
  //      // treeSetMenuList(menuList);
  //      return menuList;
  //    }
  //    return null;
  //  }
  //
  //  @Override
  //  public JsonResult doSaveOrUpdate(Map paramsMap) {
  //
  //    try {
  //      String id = StringUtils.getMapKeyValue(paramsMap, "id");
  //      String code = StringUtils.getMapKeyValue(paramsMap, "code");
  //      boolean isExistCode = this.isExistCode(code, id);
  //      if (isExistCode) {
  //        return JsonResult.putFail(JsonConstans.ERR_CODE_EXISTS);
  //      }
  //      SysMenuEntity sysMenuEntity = null;
  //      if (StringUtils.isNotBlank(id)) {
  //        sysMenuEntity = sysMenuRepository.findById(SysMenuEntity.class, id);
  //        if (sysMenuEntity == null) {
  //          return JsonResult.putFail(JsonConstans.ERR_NOT_EXISTED);
  //        }
  //        EntityUtil.putMapDataIntoEntity(paramsMap, sysMenuEntity);
  //        sysMenuRepository.update(sysMenuEntity);
  //      } else {
  //        sysMenuEntity = new SysMenuEntity();
  //        EntityUtil.putMapDataIntoEntity(paramsMap, sysMenuEntity);
  //        sysMenuRepository.save(sysMenuEntity);
  //      }
  //      return JsonResult.putSuccess();
  //    } catch (Exception e) {
  //      e.printStackTrace();
  //      return JsonResult.putFail(JsonConstans.OPERATION_FAILURE);
  //    }
  //  }
  @Override
  public void delete(String id, HashMap<String, Object> map) {
    // 删除子菜单
    List<SysMenuEntity> sysMenuEntityList = sysMenuRepository.findByFiled("parentId", id);
    for (SysMenuEntity sysMenuEntity : sysMenuEntityList) {
      delete(sysMenuEntity.getId(), map);
    }
    // 删除角色菜单
    //    List<SysRoleMenuEntity> sysRoleMenuEntityList =
    //        sysMenuRepository.findByFiled(SysRoleMenuEntity.class, "menuId", id);
    //    for (SysRoleMenuEntity sysRoleMenuEntity : sysRoleMenuEntityList) {
    //      sysMenuRepository.remove(sysRoleMenuEntity);
    //    }
    String hql =
        "delete from "
            + EntityUtil.getHqlTableName(SysRoleMenuEntity.class)
            + " as rm where rm.menuId =:menuId";
    map.clear();
    map.put("menuId", id);
    sysMenuRepository.deleteByMap(SysRoleMenuEntity.class, hql, map);
    sysMenuRepository.deleteById(id);
  }

  @Override
  public SysMenuEntity findMenuRoleById(String menuId) {
    Map<String, Object> paramMap = new HashMap<>();
    paramMap.put("id", menuId);
    String sql = "select m.* from sys_menu m where m.id=:id";

    SysMenuEntity menu = sysMenuRepository.findById(SysMenuEntity.class, menuId);
    if (menu == null) {
      return null;
    }
    sql = "SELECT * FROM sys_role_menu rm WHERE rm.menuId=:menuId";

    paramMap.clear();
    paramMap.put("menuId", menuId);
    // 菜单角色权限中间表
    List<SysRoleMenuEntity> roleMenuList =
        sysMenuRepository.findByNativeQuery(
            sql, paramMap, Transformers.aliasToBean(SysRoleMenuEntity.class));

    if (roleMenuList == null || roleMenuList.size() == 0) {
      return menu;
    }
    sql = "SELECT * FROM sys_role r WHERE r.id in (:roleIdList)";
    List<String> roleIdList = new ArrayList<>();
    for (SysRoleMenuEntity roleMenu : roleMenuList) {
      roleIdList.add(roleMenu.getRoleId());
    }
    paramMap.clear();
    paramMap.put("roleIdList", roleIdList);
    // 所有菜单的拥有的权限
    List<SysRoleEntity> roleList =
        sysMenuRepository.findByNativeQuery(
            sql, paramMap, Transformers.aliasToBean(SysRoleEntity.class));
    if (roleList == null || roleList.size() == 0) {
      return menu;
    }
    menu.setSysRoleList(roleList);
    //        for (SysMenuEntity menu : menuList) {
    //        if (menu.getSysRoleList() == null) {
    //            menu.setSysRoleList(new ArrayList<SysRoleEntity>());
    //        }
    //        roleMenu:
    //        for (SysRoleMenuEntity roleMenu : roleMenuList) {
    //            if (menu.getId().equals(roleMenu.getMenuId())) {
    //                for (SysRoleEntity role : roleList) {
    //                    if (roleMenu.getRoleId().equals(role.getId())) {
    //                        menu.getSysRoleList().add(role);
    //                        continue roleMenu;
    //                    }
    //                }
    //            }
    //        }
    //        }
    return menu;
  }

  //  @Override
  //  public Map<String, Object> findMenuList(Map paramsMap) {
  //    String cnName = StringUtils.getMapKeyValue(paramsMap, "cnName");
  //    String sql =
  //        "SELECT menu.id,menu.cnName,menu.`code`,menu.enName,menu.icon,menu.type,\n"
  //            + "	menu.isExpand,menu.isUse,menu.parentId,menu.component,menu.isHidden,\n"
  //            + "	menu.remarks,menu.sequence,menu.url,menu.version\n"
  //            + " FROM sys_menu menu";
  //    Map<String, Object> conditionParams = new HashMap<>(3);
  //    if (StringUtils.isNotBlank(cnName)) {
  //      sql += " WHERE menu.cnName =:cnName ";
  //      conditionParams.put("cnName", cnName);
  //    }
  //    Integer pageNo = null;
  //    Integer pageSize = null;
  //
  //    Map<String, Object> dataMap =
  //            sysMenuRepository.findPageByNativeQueryFileds(
  //            sql, conditionParams, Transformers.aliasToBean(SysMenuEntity.class), pageNo,
  // pageSize);
  //    if (dataMap != null) {
  //      List<SysMenuEntity> menuList =
  //          (List<SysMenuEntity>) dataMap.get(SysConstants.ROWS.getValue());
  //      menuList = treeSetMenuList(menuList);
  //      dataMap.put(SysConstants.ROWS.getValue(), menuList);
  //    }
  //    return dataMap;
  //  }

  // 将本级权限的资源分发给下一级，超级管理员可以任意分发资源

  @Override
  public JsonResult insertUpdMenu(SysMenuDto sysMenuDto) {
    SysMenuEntity menu = null;
    String url = sysMenuDto.getUrl();
    String menuName = sysMenuDto.getName();
    String code = sysMenuDto.getCode();
    int sort = sysMenuDto.getSort();
    List<SysMenuEntity> existMenuList = null;
    LinkedHashMap<String, Object> paramMap = new LinkedHashMap<>();
    List<SysRoleEntity> userRoleList = sysUserRepository.userGetRoleList(null, null);
    if (StringUtils.isBlank(sysMenuDto.getId())) {
      if (userRoleList.size() != 1 || !"ROLE_admin".equals(userRoleList.get(0).getName())) {
        return JsonResult.putFail("新建失败，权限不足");
      }
      existMenuList = sysMenuRepository.findByFiled("url", url);
      if (existMenuList.size() != 0) {
        return JsonResult.putFail("已经存在该" + url + "菜单，添加失败");
      }
      existMenuList = sysMenuRepository.findByFiled("name", menuName);
      if (existMenuList.size() != 0) {
        return JsonResult.putFail("已经存在菜单名为 ：" + menuName + "的菜单，添加失败");
      }
      boolean isExistCode = this.isExistCode(code, null);
      if (isExistCode) {
        return JsonResult.putFail(JsonConstans.ERR_CODE_EXISTS);
      }

    } else {
      menu = sysMenuRepository.findById(sysMenuDto.getId());
      if (menu == null || menu.getIsDelete()) {
        return JsonResult.putFail("更新失败，无该菜单");
      }
      JsonResult jsonResult = JsonResult.putFail("无权限操作");

      for (SysRoleEntity sysRoleEntity : userRoleList) {
        paramMap.clear();
        paramMap.put("menuId", menu.getId());
        paramMap.put("roleId", sysRoleEntity.getId());
        List<SysRoleMenuEntity> sysRoleMenuEntities =
            sysMenuRepository.findByFileds(SysRoleMenuEntity.class, paramMap);
        if (sysRoleMenuEntities.size() == 1) {
          jsonResult = null;
          break;
        }
      }
      if (jsonResult != null) {
        return jsonResult;
      }
      //      !StringUtils.trimToEmpty(
      if (!menu.getUrl().equals(url)) {
        existMenuList = sysMenuRepository.findByFiled("url", url);
        if (existMenuList.size() != 0) {
          return JsonResult.putFail("已经存在该" + url + "菜单，添加失败");
        }
      }
      //      !StringUtils.trimToEmpty(
      if (!menu.getName().equals(menuName)) {
        existMenuList = sysMenuRepository.findByFiled("name", menuName);
        if (existMenuList.size() != 0) {
          return JsonResult.putFail("已经存在菜单名为 ：" + menuName + "的菜单，添加失败");
        }
      }
      //      !StringUtils.trimToEmpty()
      if (!menu.getCode().equals(code)) {
        boolean isExistCode = this.isExistCode(code, null);
        if (isExistCode) {
          return JsonResult.putFail(JsonConstans.ERR_CODE_EXISTS);
        }
      }
    }

    JsonResult jsonResult = null;
    List<String> roleIdList = sysMenuDto.getRoleIdList();
    HashSet<SysRoleEntity> maxRoleSet = new HashSet<>();
    if (roleIdList != null && roleIdList.size() != 0) {
      // 顶级权限向下存储
      jsonResult = checkParamBySaveOrUpdMenu(roleIdList, maxRoleSet, userRoleList);
      if (jsonResult != null) {
        return jsonResult;
      }
    }
    String parentId = sysMenuDto.getParentId();
    SysMenuEntity fatherMenu = null;
    if (userRoleList.size() == 1 && "ROLE_admin".equals(userRoleList.get(0).getName())) {
      if (maxRoleSet.size() == 1 && !maxRoleSet.iterator().next().getName().equals("ROLE_admin")) {
        return JsonResult.putFail("参数缺失顶级权限");
      }

      // 子菜单权限不允许有不在父菜单权限内
      if (StringUtils.isNotBlank(parentId)) {

        fatherMenu = sysMenuRepository.findById(parentId);
        if (fatherMenu == null || fatherMenu.getIsDelete()) {
          return JsonResult.putFail("上级菜单不存在");
        }
        //        if(menu!=null){
        //          if(menu.getParentId().equals(fatherMenu.getId())){
        //
        //          }
        //        }
        //        if (fatherMenu.getId().equals(menu.getParentId())) {
        //          if (fatherMenu) {}
        //        }

        // 获取父级菜单所有包括顶级权限
        paramMap.clear();
        paramMap.put("menuId", fatherMenu.getId());
        List<SysRoleMenuEntity> sysRoleMenuEntities =
            sysMenuRepository.findByFileds(SysRoleMenuEntity.class, paramMap);
        jsonResult = menuParentIdSafe(sysRoleMenuEntities, userRoleList);
        if (jsonResult != null) {
          return jsonResult;
        }
        jsonResult = checkSonFatherRoleSome(maxRoleSet, sysRoleMenuEntities);
        if (jsonResult != null) {
          return jsonResult;
        }
      }
    } else {
      // 新增菜单权限是否在本菜单上级菜单权限下
      if (StringUtils.isNotBlank(menu.getParentId())) {
        fatherMenu = sysMenuRepository.findById(menu.getParentId());
        if (fatherMenu == null || fatherMenu.getIsDelete()) {
          return JsonResult.putFail("上级菜单不存在");
        }
        // 获取父级菜单所有包括顶级权限
        paramMap.clear();
        paramMap.put("menuId", fatherMenu.getId());
        List<SysRoleMenuEntity> sysRoleMenuEntities =
            sysMenuRepository.findByFileds(SysRoleMenuEntity.class, paramMap);
        jsonResult = checkSonFatherRoleSome(maxRoleSet, sysRoleMenuEntities);
        if (jsonResult != null) {
          return jsonResult;
        }
      }
    }

    // 新建只需上级菜单权限包含下级菜单权限，不存在编辑菜单父级菜单权限修改为新的父级菜单权限下的问题
    // 编辑菜单权限减少所有 菜单上级父类改变 移除菜单原权限包括下级，新增菜单新父级的权限包括所有下级
    if (menu != null) {
      paramMap.clear();
      paramMap.put("menuId", menu.getId());
      List<SysRoleMenuEntity> menuOldRoles =
          sysMenuRepository.findByFileds(SysRoleMenuEntity.class, paramMap);

      List<String> list = null;
      if (userRoleList.size() == 1
          && "ROLE_admin".equals(userRoleList.get(0).getName())
          && fatherMenu != null
          && !fatherMenu.getId().equals(menu.getParentId())) {
        //      if (!fatherMenu.getId().equals(menu.getParentId())) {

        // 权限
        List<SysRoleMenuEntity> menuOldRoleNeedDel = new ArrayList<>();
        removeRoleList(menuOldRoleNeedDel, menuOldRoles, maxRoleSet);

        if (menuOldRoleNeedDel.size() > 0) {
          list = new ArrayList<>();
          for (SysRoleMenuEntity sysRoleMenuEntity : menuOldRoleNeedDel) {
            list.add(sysRoleMenuEntity.getRoleId());
          }
          delMenuRole(list, menu);
        }
      } else {
        removeRoleListNoParend(menuOldRoles, maxRoleSet);
        list = new ArrayList<>();
        if (menuOldRoles.size() > 0) {
          for (SysRoleMenuEntity sysRoleMenuEntity : menuOldRoles) {
            list.add(sysRoleMenuEntity.getRoleId());
          }
          delMenuRole(list, menu);
        }
      }
    }
    //    else {
    //            if (StringUtils.isNotBlank(parentId)) {
    //              return JsonResult.putFail("低级权限无法更改父级菜单");
    //            }
    //    }
    // 超管新建情况下，超管非超管更新情况下parentId没变的情况下包括传空，必须parentMenu全部拥有其传入的menu的权限
    // 上下级权限一致
    //    Iterator<SysRoleEntity> sysRoleEntityIterator = maxRoleSet.iterator();
    //    while (sysRoleEntityIterator.hasNext()) {
    //      sysRoleEntityIterator.next();
    //    }
    if (menu == null) {
      menu = new SysMenuEntity();
    }
    menu.setParentId(parentId);
    menu.setUrl(url);
    menu.setName(menuName);
    menu.setCode(code);
    menu.setSort(sysMenuDto.getSort());
    if (StringUtils.isNotBlank(menu.getId())) {
      sysMenuRepository.update(menu);
    } else {
      sysMenuRepository.save(menu);
    }
    // sysMenuEntity.setParentId(paramsMap.get("parentId").toString());
    //        //    如果前端知道了一些重要的字段名，添加了这些字段的值就不安全了
    //        //    EntityUtil.putMapDataIntoEntity(paramsMap, sysMenuEntity);

    //        int minlevel = 0;
    Timestamp createTime = new Timestamp(System.currentTimeMillis());

    for (SysRoleEntity maxRole : maxRoleSet) {
      setMenuRole(menu, maxRole, createTime);
      //      saveRoleMenuByRoleTeeSet(maxRole, menu, createTime);
    }

    //        sysMenuEntity.setParentId(paramsMap.get("parentId").toString());
    //    如果前端知道了一些重要的字段名，添加了这些字段的值就不安全了
    //    EntityUtil.putMapDataIntoEntity(paramsMap, sysMenuEntity);
    return JsonResult.putSuccess();

    //    return JsonResult.putFail(JsonConstans.ERR_NOTVALID_AUTH);
  }

  private void setMenuRole(SysMenuEntity menu, SysRoleEntity maxRole, Timestamp createTime) {
    saveRoleMenuByRoleTeeSet(maxRole, menu, createTime);
    Map map = new HashMap();
    map.put("parentId", menu.getId());
    String sql =
        "select m.* from "
            + EntityUtil.getSqlTableName(menu.getClass())
            + " as m where m.parentId=:parentId";
    List<SysMenuEntity> sysMenuEntities =
        sysMenuRepository.findByNativeQuery(
            sql, map, Transformers.aliasToBean(SysMenuEntity.class));
    for (SysMenuEntity sysMenuEntity1 : sysMenuEntities) {
      setMenuRole(sysMenuEntity1, maxRole, createTime);
    }
  }

  private void removeRoleList(
      List<SysRoleMenuEntity> menuOldRoleNeedDel,
      List<SysRoleMenuEntity> menuOldRoles,
      HashSet<SysRoleEntity> maxRoleSet) {
    for (SysRoleMenuEntity sysRoleMenuEntity : menuOldRoles) {
      boolean flg = removeRoleList(sysRoleMenuEntity, maxRoleSet);
      if (flg == false) {
        menuOldRoleNeedDel.add(sysRoleMenuEntity);
      }
    }
  }

  private boolean removeRoleList(
      SysRoleMenuEntity sysRoleMenuEntity, HashSet<SysRoleEntity> maxRoleSet) {
    boolean flg = false;
    for (SysRoleEntity newRole : maxRoleSet) {
      if (newRole.getId().equals(sysRoleMenuEntity.getRoleId())) {
        flg = true;
        return flg;
      }
      if (newRole.getChildRoles().size() > 0) {
        flg = removeRoleList(sysRoleMenuEntity, newRole.getChildRoles());
        if (flg) {
          return flg;
        }
      }
    }
    return flg;
  }

  private void removeRoleListNoParend(
      List<SysRoleMenuEntity> menuOldRoles, HashSet<SysRoleEntity> maxRoleSet) {
    Iterator<SysRoleMenuEntity> iterator = menuOldRoles.iterator();
    while (iterator.hasNext()) {
      removeRoleListNoParend(iterator, iterator.next(), maxRoleSet);
    }
  }

  private boolean removeRoleListNoParend(
      Iterator<SysRoleMenuEntity> iterator,
      SysRoleMenuEntity sysRoleMenuEntity,
      HashSet<SysRoleEntity> maxRoleSet) {
    boolean res = false;
    for (SysRoleEntity sysRoleEntity : maxRoleSet) {
      if (sysRoleEntity.getId().equals(sysRoleMenuEntity.getRoleId())) {
        iterator.remove();
        res = true;
        return res;
      }
      res = removeRoleListNoParend(iterator, sysRoleMenuEntity, sysRoleEntity.getChildRoles());
      if (res) {
        return res;
      }
    }
    return res;
  }

  private void delMenuRole(List<String> roleIdList, SysMenuEntity sysMenuEntity) {
    Map<String, Object> paramMap = new HashMap();
    String hqlSql =
        "delete from "
            + EntityUtil.getHqlTableName(SysRoleMenuEntity.class)
            + " where roleId in(:roleIdList) and menuId=:menuId";

    paramMap.put("roleIdList", roleIdList);
    paramMap.put("menuId", sysMenuEntity.getId());
    sysMenuRepository.deleteByMap(SysRoleMenuEntity.class, hqlSql, paramMap);
    paramMap.clear();
    paramMap.put("parentId", sysMenuEntity.getId());
    hqlSql =
        "select r.* from "
            + EntityUtil.getSqlTableName(SysMenuEntity.class)
            + " as r where r.parentId=:parentId";
    List<SysMenuEntity> sysMenuEntities =
        sysMenuRepository.findByNativeQuery(
            hqlSql, paramMap, Transformers.aliasToBean(SysMenuEntity.class));
    for (SysMenuEntity sysMenuEntity1 : sysMenuEntities) {
      delMenuRole(roleIdList, sysMenuEntity1);
    }
  }

  // 新建编辑父子级权限都需一致
  private JsonResult checkSonFatherRoleSome(
      HashSet<SysRoleEntity> maxRoleSet, List<SysRoleMenuEntity> sysRoleMenuEntities) {
    for (SysRoleEntity sysRoleEntity : maxRoleSet) {
      boolean flg = false;
      roleMenu:
      for (SysRoleMenuEntity roleMenu : sysRoleMenuEntities) {
        if (roleMenu.getRoleId().equals(sysRoleEntity.getId())) {
          flg = true;
          break roleMenu;
        }
      }
      if (flg == false) {
        return JsonResult.putFail("添加失败，该菜单的上级菜单不存在新增的权限");
      }
      HashSet<SysRoleEntity> sysRoleEntities = sysRoleEntity.getChildRoles();
      if (sysRoleEntities.size() > 0) {
        JsonResult jsonResult = checkSonFatherRoleSome(sysRoleEntities, sysRoleMenuEntities);
        if (jsonResult != null) {
          return jsonResult;
        }
      }
    }
    return null;
  }

  //  private void saveSysRoleMenu(String roleId, String menuId, Timestamp createTime) {
  //    SysRoleMenuEntity rm = new SysRoleMenuEntity();
  //    rm.setMenuId(menuId);
  //    rm.setRoleId(roleId);
  //    rm.setCreateTime(createTime);
  //    sysMenuRepository.save(rm);
  //  }

  //    public List<String> getParentId(List<String> idList) {
  //        String sql = "select level from sys_role where ";
  //
  //    }

  //  private List<SysMenuEntity> treeSetMenuList(List<SysMenuEntity> rootMenuEntityList) {
  //    List<SysMenuEntity> menuList = new ArrayList<>();
  //    // 先找到所有的一级菜单
  //    for (SysMenuEntity menuEntity : rootMenuEntityList) {
  //      if (StringUtils.isBlank(menuEntity.getParentId())
  //          || STR_ZERO.equals(menuEntity.getParentId())) {
  //        menuList.add(menuEntity);
  //      }
  //    }
  //    // 为一级菜单设置子菜单，getChild是递归调用的
  //    for (SysMenuEntity menu : menuList) {
  //      menu.setChildMenus(getChildMenu(menu.getId(), rootMenuEntityList));
  //    }
  //    return menuList;
  //  }

  //  /**
  //   * 递归查找子菜单
  //   *
  //   * @param id 当前菜单id
  //   * @param rootMenuEntityList 菜单列表
  //   * @return List<SysMenuEntity> 子菜单列表
  //   */
  //  private TreeSet<SysMenuEntity> getChildMenu(String id, List<SysMenuEntity> rootMenuEntityList)
  // {
  //    // 子菜单
  //    TreeSet<SysMenuEntity> childMenuSet =
  //        new TreeSet<SysMenuEntity>(
  //            new Comparator<SysMenuEntity>() {
  //              @Override
  //              public int compare(SysMenuEntity o1, SysMenuEntity o2) {
  //                if (o1.getId().equals(o2.getId())) {
  //                  return 0;
  //                } else {
  //                  return o2.getSort() - o1.getSort();
  //                }
  //              }
  //            });
  //    for (SysMenuEntity childMenu : rootMenuEntityList) {
  //      // 遍历所有节点，将父菜单id与传过来的id比较
  //      if (StringUtils.isNotBlank(childMenu.getParentId())) {
  //        if (childMenu.getParentId().equals(id)) {
  //          childMenuSet.add(childMenu);
  //        }
  //      }
  //    }
  //    // 把子菜单的子菜单再循环一遍
  //    for (SysMenuEntity menu : childMenuSet) {
  //      if (StringUtils.isNotBlank(menu.getCode())) {
  //        // 递归
  //        menu.setChildMenus(getChildMenu(menu.getId(), rootMenuEntityList));
  //      }
  //    } // 递归退出条件
  //    if (childMenuSet.size() == 0) {
  //      return null;
  //    }
  //    return childMenuSet;
  //  }

  /**
   * 权限上下级别排序
   *
   * @param fatherRole
   * @param roleList
   */
  public void setChilentRole(SysRoleEntity fatherRole, List<SysRoleEntity> roleList) {
    if (fatherRole.getChildRoles() == null) {
      fatherRole.setChildRoles(new HashSet<SysRoleEntity>());
    }
    //    Iterator<SysRoleEntity> it = roleList.iterator();
    //    while (it.hasNext()) {
    //      SysRoleEntity x = it.next();
    //      if (x.equals("del")) {
    //        it.remove();
    //      }
    //    }
    for (SysRoleEntity role : roleList) {
      if (fatherRole.getId().equals(role.getParentId())) {
        fatherRole.getChildRoles().add(role);
      }
    }
    if (fatherRole.getChildRoles().size() == 0) {
      return;
    }
    for (SysRoleEntity nextFatherRole : fatherRole.getChildRoles()) {
      setChilentRole(nextFatherRole, roleList);
    }
  }

  public void removeRoleTreeSet(
      TreeSet<SysRoleEntity> roleTreeSet, HashSet<SysRoleEntity> chilendRoleTreeSet) {
    if (chilendRoleTreeSet.size() == 0) {
      return;
    }
    roleTreeSet.remove(chilendRoleTreeSet);
    for (SysRoleEntity fatherRole : chilendRoleTreeSet) {
      removeRoleTreeSet(roleTreeSet, fatherRole.getChildRoles());
    }
  }

  public void saveRoleMenuByRoleTeeSet(
      SysRoleEntity roleParent, SysMenuEntity sysMenuEntity, Timestamp createTime) {
    if (sysMenuRepository.findMiddleTable(
            SysRoleMenuEntity.class, "menuId", sysMenuEntity.getId(), "roleId", roleParent.getId())
        == null) {
      addRoleMenu(sysMenuEntity.getId(), roleParent.getId(), createTime);
    }
    Set<SysRoleEntity> childRoles = roleParent.getChildRoles();
    if (childRoles.size() > 0) {
      for (SysRoleEntity chiledRole : childRoles) {
        saveRoleMenuByRoleTeeSet(chiledRole, sysMenuEntity, createTime);
      }
    }
  }

  private JsonResult checkParamBySaveOrUpdMenu(
      List roleIdList, HashSet<SysRoleEntity> maxRoleSet, List<SysRoleEntity> userRoleList) {
    Map<String, Object> paramMap = new HashMap();
    //    String roleIdListKey = "roleIdList";
    //    String sql = "select * from sys_role where id in(:" + roleIdListKey + ")";
    paramMap.put("roleIdList", roleIdList);
    List<SysRoleEntity> roleList = sysRoleRepository.findRolesByRoleIdList(paramMap);
    // 指定的权限必须已经存在 roleList.size() == roleIdParamList.size()
    //    判断数量做唯一检验
    if (roleList.size() != roleIdList.size()) {
      return JsonResult.putFail(JsonConstans.ERR_NOTVALID_AUTH);
    }

    //        1.将权限进行树形结构排序
    //        2.
    //          2.1 如果当前账号权限是0则顶级权限是0
    //    //          2.2 如果不是则检验顶级权限是否是当前所在登陆者的其中一个权限下一级如果不是，则是非法操作，
    //              树形结构排序完毕检验是否有额外没有进行排序的权限并且不是账号顶级权限的下一级如果有则是非法操作

    SysRoleEntity adminRole = userRoleList.get(0);
    if (userRoleList.size() == 1 && adminRole.getName().equals("ROLE_admin")) {
      for (SysRoleEntity role : roleList) {
        if (adminRole.getId().equals(role.getId())) {
          maxRoleSet.add(role);
          break;
        }
      }
    } else {
      underUserRole:
      for (SysRoleEntity role : roleList) {
        for (SysRoleEntity userRole : userRoleList) {
          if (userRole.getId().equals(role.getParentId())) {
            maxRoleSet.add(role);
            continue underUserRole;
          }
        }
      }
    }
    if (maxRoleSet.size() == 0) {
      return JsonResult.putFail(JsonConstans.ERR_NOTVALID_AUTH);
    }
    // 对最高权限进行上下级添加下级权限
    for (SysRoleEntity maxRole : maxRoleSet) {
      setChilentRole(maxRole, roleList);
    }
    //        检测roleList是否有不在树形权限内的权限如果有则是非法操作
    for (SysRoleEntity maxRole : maxRoleSet) {
      roleList.remove(maxRole);
      roleListRemoveMaxRoleChild(maxRole, roleList);
    }
    if (roleList.size() != 0) {
      return JsonResult.putFail(JsonConstans.ERR_NOTVALID_AUTH);
    }
    // 除了登陆者权限是0，其他下级无论权限指定的是当前登录者下一级哪一级，都是需要上级存在该菜单
    // 指定的权限最高必须在当前登录者权限的下一级，如果是0超级管理员级别则可以指定本级权限

    // 如果不是超级管理员登录 需要检验树形权限的树根顶级权限的父级权限是否有该资源
    // 如果没有则是非法操作
    //    if (maxRoleSet.iterator().next().getLevel() != 0) {
    //      paramMap.clear();
    //      String sql = "select menuId from sys_role_menu where roleId=:roleId";
    //      for (SysRoleEntity underMaxrole : maxRoleSet) {
    //        paramMap.put("roleId", underMaxrole.getParentId());
    //
    //        List<Map> maxRoleParentHasMenu =
    //            sysMenuRepository.findByNativeQuery(sql, paramMap,
    // Transformers.ALIAS_TO_ENTITY_MAP);
    //        //              当前添加的菜单权限需要登陆者对应的权限如果没有该url菜单权限，操作非法
    //        if (maxRoleParentHasMenu.size() != 1) {
    //          return JsonResult.putFail(JsonConstans.ERR_NOTVALID_AUTH);
    //        }
    //      }
    //    }
    return null;
  }

  public void roleListRemoveMaxRoleChild(SysRoleEntity maxRole, List<SysRoleEntity> roleList) {
    Set<SysRoleEntity> chilendSet = maxRole.getChildRoles();
    if (chilendSet.size() != 0) {
      for (SysRoleEntity role : chilendSet) {
        roleList.remove(role);
        roleListRemoveMaxRoleChild(role, roleList);
      }
    }
  }

  private void deleteRoleMenuByRoleTree(String roleId, String menuId) {
    // 关联表删除没有的话则下一级别权限也是没有直接返回
    if (sysMenuRepository.delMiddleTable(
        SysRoleMenuEntity.class, "roleId", roleId, "menuId", menuId)) {
      List<SysRoleEntity> childRoleList =
          sysMenuRepository.findByFiled(SysRoleEntity.class, "parentId", roleId);
      for (SysRoleEntity parentRole : childRoleList) {
        deleteRoleMenuByRoleTree(roleId, menuId);
      }
    }
  }

  /**
   * 添加权限菜单中间表
   *
   * @param menuId
   * @param roleId
   * @param createTime
   */
  private void addRoleMenu(String menuId, String roleId, Timestamp createTime) {
    SysRoleMenuEntity rm = new SysRoleMenuEntity();
    rm.setMenuId(menuId);
    rm.setRoleId(roleId);
    rm.setCreateTime(createTime);
    sysMenuRepository.save(rm);
  }

  /**
   * 更新菜单权限 添加菜单新权限删除不需要的权限，传入的权限集合同一级别与查询出的集合比较，删除查询出的集合多余的权限
   *
   * @param underMaxRole
   * @param menuId
   * @param createTime
   */
  private void addNewDelNotNeedRoleMenu(
      SysRoleEntity underMaxRole, String menuId, Timestamp createTime) {
    HashSet<SysRoleEntity> underMaxRoleChildRoleSet = underMaxRole.getChildRoles();
    if (underMaxRoleChildRoleSet.size() != 0) {
      List<SysRoleEntity> allChildRoleList =
          sysRoleRepository.findByFiled("parentId", underMaxRole.getId());
      // 移除掉需要的权限剩下的都是无效权限全部删掉
      allChildRoleList.removeAll(underMaxRoleChildRoleSet);
      if (allChildRoleList.size() != 0) {
        for (SysRoleEntity role : allChildRoleList) {
          sysMenuRepository.delMiddleTable(
              SysRoleMenuEntity.class, "roleId", role.getId(), "menuId", menuId);
        }
      }
      for (SysRoleEntity role : underMaxRoleChildRoleSet) {
        SysRoleMenuEntity roleMenu =
            (SysRoleMenuEntity)
                sysMenuRepository.findMiddleTable(
                    SysRoleMenuEntity.class, "roleId", role.getId(), "menuId", menuId);
        if (roleMenu == null) {
          roleMenu = new SysRoleMenuEntity();
          roleMenu.setRoleId(role.getId());
          roleMenu.setMenuId(menuId);
          roleMenu.setCreateTime(createTime);
          sysMenuRepository.save(roleMenu);
        }
        addNewDelNotNeedRoleMenu(role, menuId, createTime);
      }
    }
  }

  /**
   * parentId限定在登录权限下
   *
   * @param userRoleList
   * @return
   */
  private JsonResult menuParentIdSafe(
      List<SysRoleMenuEntity> sysRoleMenuEntities, List<SysRoleEntity> userRoleList) {
    for (SysRoleMenuEntity sysRoleMenuEntity : sysRoleMenuEntities) {
      for (SysRoleEntity sysRoleEntity : userRoleList) {
        if (sysRoleMenuEntity.getRoleId().equals(sysRoleEntity.getId())) {
          return null;
        }
      }
    }
    return JsonResult.putFail("父级菜单不存在权限");
  }

  @Override
  public TreeSet<SysMenuEntity> findData(Object o) {
    StringBuffer sqlSB = new StringBuffer("select id from SysRoleEntity where name in(");

    Iterator<? extends GrantedAuthority> grantedAuthorityIterator =
        SecurityPrincipalContext.getUserDetails().getAuthorities().iterator();
    while (grantedAuthorityIterator.hasNext()) {
      sqlSB.append("'").append(grantedAuthorityIterator.next()).append("'");
      if (grantedAuthorityIterator.hasNext()) {
        sqlSB.append(",");
      } else {
        sqlSB.append(")");
      }
    }
    //    for (int i=0;i<SecurityPrincipalContext.getUserDetails().getAuthorities().size();i++){
    //      SecurityPrincipalContext.getUserDetails().getAuthorities().
    //      sqlSB.append()
    //    }
    List<String> roleIdList = sysRoleRepository.list(String.class, sqlSB.toString());

    sqlSB.setLength(0);
    sqlSB.append("select menuId from SysRoleMenuEntity where roleId in(");
    for (int i = 0; i < roleIdList.size(); i++) {
      sqlSB.append("'").append(roleIdList.get(i)).append("'");
      if (i != roleIdList.size() - 1) {
        sqlSB.append(",");
      } else {
        sqlSB.append(")");
      }
    }
    List<String> menuIdList = sysRoleRepository.list(String.class, sqlSB.toString());
    Map<String, Object> map = new HashMap<>();
    map.put("menuIdList", menuIdList);
    sqlSB.setLength(0);
    sqlSB.append(
        "select d.* from "
            + EntityUtil.getSqlTableName(sysMenuRepository.getClazz())
            + " d where d.isDelete=false and d.status=1 and d.id in(:menuIdList)");

    return SysMenuEntity.upDownMenu(sysMenuRepository.findByNativeQuery(sqlSB.toString(), map));
  }

  /**
   * 挪动菜单到其他菜单下面需要限制在该菜单原本的权限下 除了顶级权限账户可以做本级菜单修改外其他权限账户只能做下级菜单修改 前端传过来默认是该菜单在当前登陆者的权限
   * 如果没有传权限id则是去掉该菜单在当前登陆者的所有权限 将修改权限List集合跟save同样做树形排序，获得该排序后的树形TreeSet顶级权限查询数据库是否一致
   *
   * @param sysMenuDto
   * @return
   */
  @Override
  public JsonResult update(SysMenuDto sysMenuDto) {
    SysMenuEntity menu = sysMenuRepository.findById(SysMenuEntity.class, sysMenuDto.getId());
    if (menu == null) {
      return JsonResult.putFail("菜单获取失败");
    }
    List<SysMenuEntity> existMenuList = sysMenuRepository.findByFiled("url", sysMenuDto.getUrl());
    if (existMenuList.size() != 0) {
      return JsonResult.putFail("已经存在该" + sysMenuDto.getUrl() + "菜单，更新失败");
    }
    // 顶级权限拥有所有菜单，更新菜单的上级菜单必须是原权限下的菜单
    // 比如有个菜单的权限不是本登录者的权限,那么更新菜单的上级菜单就不能为该菜单，
    // 菜单上级还要判断不能为下级
    Timestamp updTime = new Timestamp(System.currentTimeMillis());
    List roleIdList = sysMenuDto.getRoleIdList();
    Map<String, Object> paramMap = new HashMap<>();
    List<String> roleNameList = new ArrayList();
    JsonResult jsonResult = null;
    List<SysRoleEntity> userRoleList = sysUserRepository.userGetRoleList(paramMap, roleNameList);
    // 先对权限排序，再依次检测其上级菜单是否有权限
    jsonResult = menuParentIdSafe(null, userRoleList);
    if (jsonResult != null) {
      return jsonResult;
    }
    menu.setParentId(sysMenuDto.getParentId());
    menu.setUrl(sysMenuDto.getUrl());
    menu.setName(sysMenuDto.getName());
    //    JsonResult jsonResult = menuParentIdSafe(sysMenuDto.getParentId(), userRoleList);
    //    if (jsonResult != null) {
    //      return jsonResult;
    //    }
    //    menu.setParentId(sysMenuDto.getParentId());
    //    if (menu.getId().equals("153333821338858579")) {
    //    //      throw new RuntimeException("0xx");
    //    //    }

    // 顶级权限向下存储
    HashSet<SysRoleEntity> underMaxRoleSet = new HashSet<>();
    if (roleIdList != null && roleIdList.size() != 0) {
      jsonResult = checkParamBySaveOrUpdMenu(roleIdList, underMaxRoleSet, userRoleList);
      if (jsonResult != null) {
        return jsonResult;
      }
    }
    //    对前端没有传进来的权限即是需要删掉的权限进行处理
    List<SysRoleEntity> userRoleNoRemove = new ArrayList<>();

    SysRoleEntity adminRole = null;
    if (underMaxRoleSet.size() == 1
        && underMaxRoleSet.iterator().next().getName().equals("ROLE_admin")) {
      adminRole = underMaxRoleSet.iterator().next();
      userRoleNoRemove.add(adminRole);
    } else {
      underMaxRole:
      for (SysRoleEntity underMaxRole : underMaxRoleSet) {
        for (SysRoleEntity userRole : userRoleList) {
          if (underMaxRole.getParentId().equals(userRole.getId())) {
            userRoleNoRemove.add(userRole);
            continue underMaxRole;
          }
        }
      }
    }
    userRoleList.removeAll(userRoleNoRemove);

    //      处理需要删除的一整个树形权限的菜单
    if (userRoleList.size() != 0) {
      //      if (userRoleList.size() != 1 && userRoleList.get(0).getLevel() != 0) {
      for (SysRoleEntity userRole : userRoleList) {
        //        查询role_menu中间表是否该登陆者权限拥有该资源，
        // 有则需要以该登陆者权限作为树根自顶向下删除拥有该资源的整个树形权限
        //        查询中间表有则删除并查询下一权限

        //  递归：下一权限中间表有关联继续删除并查询下一权限，退出条件下一权限没有中间表关联或者没有下一权限
        //        userRole
        SysRoleMenuEntity roleMenu =
            (SysRoleMenuEntity)
                sysMenuRepository.findMiddleTable(
                    SysRoleMenuEntity.class,
                    "roleId",
                    userRole.getId(),
                    "menuId",
                    sysMenuDto.getId());
        // 登录者有权限才能删除其下级权限的资源
        if (roleMenu != null) {
          List<SysRoleEntity> underSecondMaxRoleList =
              sysMenuRepository.findByFiled(SysRoleEntity.class, "parentId", userRole.getId());
          for (SysRoleEntity underSecondMaxRole : underSecondMaxRoleList) {
            deleteRoleMenuByRoleTree(underSecondMaxRole.getId(), sysMenuDto.getId());
          }
        }
      }
      //        如果删除超级管理员对该菜单的权限，

      // 那么超级管理员无法访问该菜单也就无法对该菜单进行再次update
      //        应该在del操作下可以删除整个菜单以及菜单所有权限，
      // 这里的update不允许删除超级管理员对该菜单拥有的权限，以便能继续修改该菜单信息
      //      } else {
      //        //        超级管理员的权限资源删除
      //        deleteRoleMenuByRoleTree(userRoleList.get(0).getId(), syssysMenuDto.getId());
      //      }
    }
    if (underMaxRoleSet.size() > 0) {
      //    调整该菜单权限为当前树形权限，去掉不在underMaxRoleSet树形权限中的权限，
      // 加入树形underMaxRoleSet树形权限中的权限如果存在就不新增

      String sql = "";

      for (SysRoleEntity underMaxRole : underMaxRoleSet) {

        SysRoleMenuEntity roleMenu =
            (SysRoleMenuEntity)
                sysMenuRepository.findMiddleTable(
                    SysRoleMenuEntity.class,
                    "roleId",
                    underMaxRole.getId(),
                    "menuId",
                    sysMenuDto.getId());
        Timestamp createTime = new Timestamp(System.currentTimeMillis());
        if (roleMenu == null) {
          addRoleMenu(sysMenuDto.getId(), underMaxRole.getId(), createTime);
        }
        if (underMaxRole.getParentId() != null) {
          List<SysRoleEntity> roleList =
              sysMenuRepository.findByFiled(
                  SysRoleEntity.class, "parentId", underMaxRole.getParentId());

          roleList.remove(underMaxRole);
          sql =
              "DELETE FROM "
                  + EntityUtil.getSqlTableName(SysRoleMenuEntity.class)
                  + " where roleId in(:roleIdList) and menuId=:menuId";
          //          for (SysRoleEntity role : roleList) {
          //            middleTableService.delMiddleTable(
          //                SysRoleMenuEntity.class, "roleId", role.getId(), "menuId",
          // syssysMenuDto.getId());
          //          }
          paramMap.clear();
          paramMap.put("roleIdList", roleList);
          paramMap.put("menuId", sysMenuDto.getId());
          sysMenuRepository.findByNativeQuery(sql, paramMap, Transformers.ALIAS_TO_ENTITY_MAP);
        }

        //      if (underMaxRoleChildRoleSet.size() != 0) {
        //        List<SysRoleEntity> allChildRoleList =
        //            sysRoleRepository.findByHql(
        //                SysRoleEntity.class, "parentId",
        // underMaxRoleChildRoleSet.first().getParentId());
        //
        //        allChildRoleList.removeAll(underMaxRoleChildRoleSet);
        //        if (allChildRoleList.size() != 0) {
        //          for (SysRoleEntity role : allChildRoleList) {
        //            middleTableService.delMiddleTable(
        //                SysRoleMenuEntity.class, "roleId", role.getId(), "menuId",
        // syssysMenuDto.getId());
        //          }
        //        }
        //
        //      }
        addNewDelNotNeedRoleMenu(underMaxRole, sysMenuDto.getId(), createTime);
      }
    }
    sysMenuRepository.update(menu);
    return JsonResult.putSuccess();
  }
}
