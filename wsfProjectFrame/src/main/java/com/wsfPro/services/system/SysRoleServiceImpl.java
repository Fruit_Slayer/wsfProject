package com.wsfPro.services.system;

import com.wsfPro.controllers.dto.system.SysRoleDto;
import com.wsfPro.core.system.SysRoleService;
import com.wsfPro.entities.system.SysRoleEntity;
import com.wsfPro.repository.system.SysRoleRepository;
import com.wsfPro.repository.system.SysUserRepository;
import com.wsfPro.security.config.SecurityPrincipalContext;
import com.wsfPro.services.base.BaseDateServiceImpl;
import com.wsfPro.util.controllerUtil.JsonConstans;
import com.wsfPro.util.controllerUtil.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class SysRoleServiceImpl extends BaseDateServiceImpl<SysRoleEntity>
    implements SysRoleService {
  @Autowired private SysRoleRepository sysRoleRepository;
  @Autowired private SysUserRepository sysUserRepository;

  @Override
  public JsonResult addSysRole(SysRoleDto roleItem) {
    SysRoleEntity parentSysRoleEntity =
        sysRoleRepository.findById(SysRoleEntity.class, roleItem.getParentId());
    if (parentSysRoleEntity == null) {
      return JsonResult.putFail(JsonConstans.ERR_NOTVALID_AUTH);
    }
    SysRoleEntity role = roleItem.createSysRoleEntity();
    role.setLevel(parentSysRoleEntity.getLevel() + 1);
    if (checkRole(role)) {
      sysRoleRepository.save(role);
      return JsonResult.putSuccess();
    }
    return JsonResult.putFail(JsonConstans.ERR_NOTVALID_AUTH);
  }

  @Override
  public JsonResult updRole(SysRoleDto roleItem) {
    return null;
  }

  // 针对权限的增删改都是下一级别，不能对不小于本级别的权限进行操作
  public boolean checkRole(SysRoleEntity role) {
    Set<SysRoleEntity> userBigRoleSet = sysUserRepository.userGetMaxLevelSysRoleSet();
    //   添加的权限级别小于该登录账号的某一个权限级别下
    boolean isUnderUserBigRole = false;
    for (SysRoleEntity userBigRole : userBigRoleSet) {
      //      级别低于当前登陆者的其中一个权限，并不一定是当前该登陆者权限的下级权限之一
      // 需要对其进行校验是否是其下的权限
      if (userBigRole.getLevel() < role.getLevel()) {
        //       当前需要校验的权限如果在账户某一角色权限下直接返回
        isUnderUserBigRole = getRoleParent(role.getParentId(), userBigRole);
        if (isUnderUserBigRole) {
          break;
        }
      }
      //       return JsonResult.putFail(JsonConstans.ERR_NOTVALID_AUTH);
    }
    return isUnderUserBigRole;
  }

  /**
   * @param roleParentId
   * @param fatherRole
   * @return
   */
  public boolean getRoleParent(String roleParentId, SysRoleEntity fatherRole) {
    boolean res = false;
    if (roleParentId.equals(fatherRole.getId())) {
      res = true;
      return res;
    }
    List<SysRoleEntity> parentRoleList =
        sysRoleRepository.findByFiled("parentId", fatherRole.getId());

    for (SysRoleEntity role : parentRoleList) {
      res = getRoleParent(roleParentId, role);
      if (res) {
        return res;
      }
    }
    return res;
  }

  @Override
  public List<SysRoleEntity> findData(Object o) {
    List<String> roleNameList = new ArrayList();
    for (GrantedAuthority grantedAuthority :
        SecurityPrincipalContext.getUserDetailsImpl().getAuthorities()) {
      roleNameList.add(grantedAuthority.getAuthority());
    }
    Map<String, Object> paramMap = new HashMap();
    paramMap.put("roleNameList", roleNameList);
    return sysRoleRepository.findRolesByRoleNameList(paramMap);
  }
}
