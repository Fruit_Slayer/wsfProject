// package com.wsfPro.services;
//
// import com.wsfPro.core.base.BaseRepository;
// import com.wsfPro.core.base.BaseSQLParamEnum;
// import org.hibernate.query.NativeQuery;
// import org.hibernate.transform.ResultTransformer;
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.stereotype.Repository;
//
// import javax.persistence.EntityManager;
// import javax.persistence.PersistenceContext;
// import javax.persistence.Query;
// import java.math.BigInteger;
// import java.util.HashMap;
// import java.util.List;
// import java.util.Map;
//
// @Repository
// public abstract class BaseRepositoryImpl implements BaseRepository {
//  private static final Logger logger = LoggerFactory.getLogger(BaseRepositoryImpl.class);
//  @PersistenceContext protected EntityManager entityManager;
//
//  @Override
//  public Map<String, Object> findByNativeQuery(
//      String sql,
//      Map<String, Object> conditionParams,
//      ResultTransformer resultTransformer,
//      Integer pageNo,
//      Integer pageSize) {
//    try {
//      String countSql = "select count(1) from (" + sql + ") u";
//      Query nativeQuery = entityManager.createNativeQuery(sql);
//      Query nativeCountQuery = entityManager.createNativeQuery(countSql);
//      for (String paramKey : conditionParams.keySet()) {
//        nativeCountQuery.setParameter(paramKey, conditionParams.get(paramKey));
//        nativeQuery.setParameter(paramKey, conditionParams.get(paramKey));
//      }
//      List totalList = nativeCountQuery.getResultList();
//      BigInteger total = (BigInteger) totalList.get(0);
//      if (pageNo != null) {
//        nativeQuery.setFirstResult((pageNo - 1) * pageSize);
//      }
//      if (pageSize != null) {
//        nativeQuery.setMaxResults(pageSize);
//      }
//      if (resultTransformer != null) {
//        nativeQuery.unwrap(NativeQuery.class).setResultTransformer(resultTransformer);
//      }
//      List rows = nativeQuery.getResultList();
//      Map<String, Object> resultMap = new HashMap<>(2);
//      resultMap.put(BaseSQLParamEnum.ROWS.getValue(), rows);
//      resultMap.put(BaseSQLParamEnum.TOTAL.getValue(), total);
//      return resultMap;
//    } catch (Exception e) {
//      e.printStackTrace();
//    } finally {
//      entityManager.close();
//    }
//    return null;
//  }
//
//  public static void main(String[] args) throws Exception {
//    System.out.println(BaseSQLParamEnum.TOTAL.getValue());
//  }
// }
