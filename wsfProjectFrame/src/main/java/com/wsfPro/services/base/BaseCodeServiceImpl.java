package com.wsfPro.services.base;

import com.wsfPro.core.base.BaseCodeService;
import com.wsfPro.entities.base.BaseCodeEntity;
import com.wsfPro.repository.base.BaseCodeRepository;
import com.wsfPro.util.EntityUtil;
import com.wsfPro.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;

public abstract class BaseCodeServiceImpl<X extends BaseCodeEntity> extends BaseDateServiceImpl<X>
    implements BaseCodeService<X> {
  @Autowired
protected BaseCodeRepository<X> baseCodeRepository;
  @Override
  public boolean isExistCode(String code, String id) {
   return baseCodeRepository.isExistCode(code,id);
}
}
