package com.wsfPro.services.base;

import com.wsfPro.core.base.BaseDateService;
import com.wsfPro.entities.base.BaseDataEntity;
import com.wsfPro.repository.base.BaseDateRepository;
import com.wsfPro.util.controllerUtil.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public abstract class BaseDateServiceImpl<X extends BaseDataEntity> extends BaseServiceImpl<X>
    implements BaseDateService<X> {
  @Autowired
  protected BaseDateRepository<X> baseDateRepository;

  @Override
  public JsonResult saveOrUpdate(X entity) {
    return baseDateRepository.saveOrUpdate(entity);
  }

  @Override
  public JsonResult batchObject(List<X> entityList) {
    return baseDateRepository.batchObject(entityList);
  }
//
//  abstract List<X> findData(Object o);
}
