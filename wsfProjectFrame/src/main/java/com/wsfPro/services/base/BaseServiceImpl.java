package com.wsfPro.services.base;

import com.wsfPro.core.base.BaseService;
import com.wsfPro.entities.base.BaseEntity;
import com.wsfPro.repository.base.BaseRepository;
import com.wsfPro.services.AbstractServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseServiceImpl<X extends BaseEntity> extends AbstractServiceImpl<X, String>
    implements BaseService<X> {
  @Autowired
  protected BaseRepository<X> baseRepository;

  @Override
  public void softDelete(X entity) {
    baseRepository.softDelete(entity);
  }
}
