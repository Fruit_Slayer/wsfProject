package com.wsfPro.services.LoginDisplay;

import com.wsfPro.controllers.item.LoginDisplay;
import com.wsfPro.core.dao.Dao;
import com.wsfPro.core.dao.LoginDisplayService;
import com.wsfPro.entities.account.AccountEntity;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service(value = "loginDisplayServiceImpl")
public class LoginDisplayServiceImpl implements LoginDisplayService {

  //	@Resource(name = "dao")
  //	private Dao<TestEntity> dao;
  @Resource(name = "dao")
  private Dao<AccountEntity> dao;

  /** 查询登录者并对LoginDisplay初始化 */
  @Override
  public LoginDisplay getLogin(String nickName, String passWord) throws Exception {
    //		List<TestEntity> list = dao.select(TestEntity.class,
    //				"select x from TestEntity as x where x.id= 1");
    //		for (TestEntity test : list) {
    //			System.out.println(test.getNumberTestInteger());
    //			System.out.println(test.getNumberTest());
    //
    //		}
    List<AccountEntity> aeL =
        dao.select(
            AccountEntity.class,
            "select x from AccountEntity as x where x.nickName='"
                + nickName
                + "' and x.passWord='"
                + passWord
                + "'");
    System.out.println(aeL.get(0).getNickName());
    if (aeL != null && aeL.size() == 1) {
      LoginDisplay ld = new LoginDisplay(aeL.get(0).getId());
      //		 ld.set(aeL.get(0));
      return ld;
    }
    return null;
  }
}
