package com.wsfPro.services;

import com.wsfPro.core.PersisTableService;
import com.wsfPro.entities.base.AbstractFreeEntity;
import com.wsfPro.repository.AbstractPersisTableRepository;
import com.wsfPro.repository.PersisTableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersisTableServiceImpl extends AbstractPersisTableServiceImpl<AbstractFreeEntity> implements PersisTableService {
    @Autowired
    PersisTableRepository persisTableRepository;
}
