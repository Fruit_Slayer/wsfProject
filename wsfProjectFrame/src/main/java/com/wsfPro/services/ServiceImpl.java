package com.wsfPro.services;

import com.wsfPro.entities.base.BaseEntity;
import com.wsfPro.repository.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceImpl extends AbstractServiceImpl<BaseEntity,String> implements com.wsfPro.core.Service {
    @Autowired
    Repository repository;
}
