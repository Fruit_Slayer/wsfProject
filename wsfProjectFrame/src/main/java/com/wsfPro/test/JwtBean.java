//package com.wsfPro.test;
//
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.stereotype.Component;
//
//@Component
//@PropertySource("classpath:jwttokenconfig.properties")
//@ConfigurationProperties(prefix = "jwt")
//public class JwtBean {
//  private String name;
//  private String author;
//  private int price;
//  private String clientId;
//
//  private String header;
//
//  private String tokenHead;
//
//  // base64密匙
//
//  private String base64Secret;
//
//  // token 有效时间
//
//  private long effectiveTime;
//
//  public String getName() {
//    return name;
//  }
//
//  public void setName(String name) {
//    this.name = name;
//  }
//
//  public String getAuthor() {
//    return author;
//  }
//
//  public int getPrice() {
//    return price;
//  }
//
//  public void setPrice(int price) {
//    this.price = price;
//  }
//
//  public void setAuthor(String author) {
//    this.author = author;
//  }
//
//  public String getClientId() {
//    return clientId;
//  }
//
//  public void setClientId(String clientId) {
//    this.clientId = clientId;
//  }
//
//  public String getHeader() {
//    return header;
//  }
//
//  public void setHeader(String header) {
//    this.header = header;
//  }
//
//  public String getTokenHead() {
//    return tokenHead;
//  }
//
//  public void setTokenHead(String tokenHead) {
//    this.tokenHead = tokenHead;
//  }
//
//  public String getBase64Secret() {
//    return base64Secret;
//  }
//
//  public void setBase64Secret(String base64Secret) {
//    this.base64Secret = base64Secret;
//  }
//
//  public long getEffectiveTime() {
//    return effectiveTime;
//  }
//
//  public void setEffectiveTime(long effectiveTime) {
//    this.effectiveTime = effectiveTime;
//  }
//}
