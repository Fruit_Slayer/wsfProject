// package com.wsfPro.test;
//
// import org.springframework.boot.context.properties.ConfigurationProperties;
// import org.springframework.context.annotation.PropertySource;
// import org.springframework.stereotype.Component;
//
//// jwtToken配置
// @Component
// @PropertySource("classpath:jwtTokenConfig.properties")
//// prefix = "jwtTokenConfig"为配置文件中属性名的前缀
// @ConfigurationProperties(prefix = "jwtTokenConfig")
// public class JwtTokenConfigBean {
//
//  private String clientId;
//
//  private String header;
//
//  private String tokenHead;
//
//  // base64密匙
//
//  private String base64Secret;
//
//  private String name;
//
//  // token 有效时间
//
//  private long effectiveTime;
//
//  public String getClientId() {
//    return clientId;
//  }
//
//  public void setClientId(String clientId) {
//    this.clientId = clientId;
//  }
//
//  public String getHeader() {
//    return header;
//  }
//
//  public void setHeader(String header) {
//    this.header = header;
//  }
//
//  public String getTokenHead() {
//    return tokenHead;
//  }
//
//  public void setTokenHead(String tokenHead) {
//    this.tokenHead = tokenHead;
//  }
//
//  public String getBase64Secret() {
//    return base64Secret;
//  }
//
//  public void setBase64Secret(String base64Secret) {
//    this.base64Secret = base64Secret;
//  }
//
//  public String getName() {
//    return name;
//  }
//
//  public void setName(String name) {
//    this.name = name;
//  }
//
//  public long getEffectiveTime() {
//    return effectiveTime;
//  }
//
//  public void setEffectiveTime(long effectiveTime) {
//    this.effectiveTime = effectiveTime;
//  }
// }
