package com.wsfPro.repository;

/**
 *
 *
 * <pre>
 * 所有数据的实体，都应该继承该接口;
 * 该接口用于定义数据实体的规范。
 * </pre>
 *
 * @author jackson
 * @version 1.0
 */
public interface Persistable<I> extends IDTypeService<I> {

  /**
   * 实体在数据库中的ID,唯一值。
   *
   * @return ID
   */
  I getId();

  void setId(I id);

  //  boolean isNew();
}
