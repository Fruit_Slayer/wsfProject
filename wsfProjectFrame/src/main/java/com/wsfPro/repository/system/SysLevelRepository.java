package com.wsfPro.repository.system;

import com.wsfPro.core.AbstractService;
import com.wsfPro.entities.system.SysLevelEntity;
import com.wsfPro.repository.AbstractRepository;

public interface SysLevelRepository extends AbstractRepository<SysLevelEntity, String> {}
