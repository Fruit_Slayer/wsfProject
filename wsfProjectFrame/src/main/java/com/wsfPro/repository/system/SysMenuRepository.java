package com.wsfPro.repository.system;

import com.wsfPro.entities.system.SysMenuEntity;
import com.wsfPro.repository.base.BaseCodeRepository;

import java.util.List;
import java.util.Map;

public interface SysMenuRepository extends BaseCodeRepository<SysMenuEntity> {

  //  /** 根据用户角色初始菜单 */
  //  List<SysMenuEntity> initMenuList();
  //
  //  /**
  //   * 菜单维护列表查询
  //   *
  //   * @param paramsMap 查询参数
  //   * @return Map
  //   * @author eiven
  //   */
  //  Map<String, Object> findMenuList(Map paramsMap);
  //
  //  /**
  //   * 菜单以及其拥有的权限信息的保存
  //   *
  //   * @param menuItem
  //   * @return
  //   */
  //  JsonResult addMenu(SysMenuItem menuItem);

  //  /**
  //   * 菜单以及其拥有的权限信息更新
  //   *
  //   * @param sysMenuDto
  //   * @return
  //   */
  //  JsonResult update(SysMenuDto sysMenuDto);

  //  /**
  //   * 菜单以及其拥有的权限信息的保存或新增
  //   *
  //   * @param paramsMap
  //   * @return JsonResult
  //   * @author eiven
  //   */
  //  JsonResult doSaveOrUpdate(Map paramsMap);

  //  /**
  //   * 根据表的id删除数据
  //   *
  //   * @param clazz
  //   */
  //  boolean delete(Class clazz, String id);

  //  */    /**
  ////     * 所有菜单,在url资源菜单匹配需要对菜单从细粒度开始排序
  ////     */

  //    /**
  //     * url去除/拼接查询获取matchUrl 特殊get请求有?再做截取
  //     *
  //     * @param matchUrl
  //     * @return
  //     */

  /** 根据id查询 */
  SysMenuEntity findMenuRoleById(String menuId);

  List<SysMenuEntity> findMenusByMenuIdList(Map<String, Object> paramMap);
}
