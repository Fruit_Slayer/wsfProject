package com.wsfPro.repository.system;

import com.wsfPro.entities.system.SysRoleEntity;
import com.wsfPro.repository.base.BaseDateRepository;

import java.util.List;
import java.util.Map;

public interface SysRoleRepository extends BaseDateRepository<SysRoleEntity> {

    List<SysRoleEntity> findRolesByRoleIdList(Map<String,Object> paramMap);
    List<SysRoleEntity> findRolesByRoleNameList(Map<String,Object> paramMap);
}
