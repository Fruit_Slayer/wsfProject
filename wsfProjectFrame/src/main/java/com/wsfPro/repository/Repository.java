package com.wsfPro.repository;

import com.wsfPro.entities.base.BaseEntity;

public interface Repository extends AbstractRepository<BaseEntity,String> {
}
