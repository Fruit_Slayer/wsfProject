package com.wsfPro.repository;

import java.io.Serializable;

/** @param <I> I定义实体主键类型 */
public interface IDTypeService<I> extends Serializable {}
