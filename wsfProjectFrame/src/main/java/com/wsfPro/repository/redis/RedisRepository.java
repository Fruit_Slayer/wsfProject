package com.wsfPro.repository.redis;

import com.wsfPro.core.base.SysConstants;
import com.wsfPro.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Repository
public class RedisRepository {

  @Autowired private RedisTemplate<String, String> redisTemplate;

  @Resource(name = "redisTemplate")
  private ListOperations<String, String> listOps;

  @Resource(name = "redisTemplate")
  private ValueOperations<String, Object> valueOps;

  public void setSession(Map user) {
    valueOps.set(
        RedisConstans.USER_ACCESS_TOKEN
            + String.valueOf(user.get(SysConstants.USER_TOKEN.getValue())),
        user,
        30,
        TimeUnit.MINUTES);
  }
  //
  //  public void setWxUserSession(WxUserEntity wxUserEntity) {
  //    valueOps.set(
  //        RedisConstans.WX_USER_ACCESS_TOKEN + wxUserEntity.getUserToken(),
  //        wxUserEntity,
  //        30,
  //        TimeUnit.MINUTES);
  //  }

  public void setWxUserSession(Map wxUser) {
    valueOps.set(
        RedisConstans.WX_USER_ACCESS_TOKEN + wxUser.get("userToken"), wxUser, 30, TimeUnit.MINUTES);
  }
  /** 商户信息 */
  public void setMertSession(Map businessMerchantsEntity) {
    valueOps.set(
        RedisConstans.MERT_ACCESS_TOKEN + businessMerchantsEntity.get("token").toString(),
        businessMerchantsEntity,
        30,
        TimeUnit.MINUTES);
  }
  //  /** 商户信息 */
  //  public void setMertSession(BusinessMerchantsEntity businessMerchantsEntity) {
  //    valueOps.set(
  //        RedisConstans.MERT_ACCESS_TOKEN + businessMerchantsEntity.getToken(),
  //        businessMerchantsEntity,
  //        30,
  //        TimeUnit.MINUTES);
  //  }

  public Map getMertSession(String mertAccessToken) {
    Map businessMerchantsEntity =
        (Map) valueOps.get(RedisConstans.MERT_ACCESS_TOKEN + mertAccessToken);
    if (businessMerchantsEntity == null) {
      return null;
    }
    setMertSession(businessMerchantsEntity);
    return businessMerchantsEntity;
  }

  public Map getWxUserSession(String wxUserAccessToken) {
    Map user = (Map) valueOps.get(RedisConstans.WX_USER_ACCESS_TOKEN + wxUserAccessToken);
    if (user == null) {
      return null;
    }
    setWxUserSession(user);
    return user;
  }

  public Map getSession(String userAccessToken) {
    Map user = (Map) valueOps.get(RedisConstans.USER_ACCESS_TOKEN + userAccessToken);
    if (user == null) {
      return null;
    }
    setSession(user);
    return user;
  }

  public Boolean delSession(String userAccessToken) {
    if (StringUtils.isBlank(userAccessToken)) {
      return false;
    }
    return valueOps.getOperations().delete(RedisConstans.USER_ACCESS_TOKEN + userAccessToken);
  }

  public Boolean delMertSession(String mertAccessToken) {
    if (StringUtils.isBlank(mertAccessToken)) {
      return false;
    }
    return valueOps.getOperations().delete(RedisConstans.MERT_ACCESS_TOKEN + mertAccessToken);
  }

  public Boolean delWxUserSession(String wxUserAccessToken) {
    if (StringUtils.isBlank(wxUserAccessToken)) {
      return false;
    }
    return valueOps.getOperations().delete(RedisConstans.WX_USER_ACCESS_TOKEN + wxUserAccessToken);
  }
}
