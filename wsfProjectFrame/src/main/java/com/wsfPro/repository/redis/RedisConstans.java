package com.wsfPro.repository.redis;

import java.util.Random;

/** @Author: eiven @Date: Created in 21:40 2018/5/17 @Description: redis 目录结构 */
public class RedisConstans {
  private static final Random RD = new Random();
  /** redis 库名 */
  public static final String REDIS_DB = "EPOS:";

  /** USER_ACCESS_TOKEN 目录 存放用户信息 用于登录验证 */
  public static final String USER_ACCESS_TOKEN = "EPOS:USERACCESSTOKEN:";
  /** USER_ACCESS_TOKEN 目录 存放微信用户信息 用于登录验证 */
  public static final String WX_USER_ACCESS_TOKEN = "EPOS:WXUSERACCESSTOKEN:";
  /** USER_ACCESS_TOKEN 目录 存放商户信息 用于登录验证 */
  public static final String MERT_ACCESS_TOKEN = "EPOS:MERT_ACCESS_TOKEN:";
}
