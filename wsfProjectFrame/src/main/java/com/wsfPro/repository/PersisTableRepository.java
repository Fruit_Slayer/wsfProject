package com.wsfPro.repository;

import com.wsfPro.entities.base.AbstractFreeEntity;

public interface PersisTableRepository extends AbstractPersisTableRepository<AbstractFreeEntity> {
}
