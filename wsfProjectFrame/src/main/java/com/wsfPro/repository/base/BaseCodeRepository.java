package com.wsfPro.repository.base;

import com.wsfPro.entities.base.BaseCodeEntity;

public interface BaseCodeRepository<X extends BaseCodeEntity> extends BaseDateRepository<X> {
  /**
   * 检验表是否存在code , 保证code的唯一性
   *
   * @param code 表字段代码
   * @param id 表字段id
   * @return true(存在) or false(不存在)
   * @author eiven
   */
  boolean isExistCode(String code, String id);
}
