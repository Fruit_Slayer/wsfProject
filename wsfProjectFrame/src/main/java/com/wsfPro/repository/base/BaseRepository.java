package com.wsfPro.repository.base;

import com.wsfPro.entities.base.BaseEntity;
import com.wsfPro.repository.AbstractRepository;

public interface BaseRepository<X extends BaseEntity> extends AbstractRepository<X, String> {
  void softDelete(X entity);
}
