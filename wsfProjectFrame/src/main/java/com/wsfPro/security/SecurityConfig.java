package com.wsfPro.security;

import com.wsfPro.security.config.*;
import com.wsfPro.util.jwt.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  /** 注入url资源检验过滤器，返回该url所拥有的权限 */
  @Autowired UrlFilterInvocationSecurityMetadataSource urlFilterInvocationSecurityMetadataSource;
  /** 注入url资源权限匹配管理器，检验当前url所在权限是否属于当前登录者所拥有的权限 */
  @Autowired UrlAccessDecisionManager urlAccessDecisionManager;
  /** 注入权限不足返回信息类 */
  @Autowired AuthenticationAccessDeniedHandler authenticationAccessDeniedHandler;

  // @Autowired SecurityUsernamePasswordAuthenticationFilter
  // securityUsernamePasswordAuthenticationFilter;
  /** 注入登录成功需要执行的操作Handler */
  @Resource(name = "authenticationSuccessHandlerImpl")
  private AuthenticationSuccessHandler authenticationSuccessHandler;

  @Autowired private JwtUtil jwtUtil;

  /** 自定义 登录 过滤器 */
  @Autowired private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;

  @Resource(name = "sysUserServiceImpl")
  private UserDetailsService sysUserServiceImpl;

  public static void setAuthenticationToSecurityContext(
      UserDetails userDetails, HttpServletRequest request) {
    if (userDetails != null) {
      UsernamePasswordAuthenticationToken authentication =
          new UsernamePasswordAuthenticationToken(
              userDetails, userDetails.getPassword(), userDetails.getAuthorities());
      authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
      SecurityContextHolder.getContext().setAuthentication(authentication);
    }
  }

  /**
   * 注入登陆服务，请求login url时会执行该服务的loadUserByUsername()
   *
   * @param auth
   * @throws Exception
   */
  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(sysUserServiceImpl).passwordEncoder(new BCryptPasswordEncoder());
  }

  @Override
  public void configure(WebSecurity web) throws Exception {
    // .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
    //    //    // 允许对于网站静态资源的无授权访问
    //    //            .antMatchers(
    //    //            "/", "/*.html", "/favicon.ico", "/**/*.html", "/**/*.css", "/**/*.js")
    //    //            .permitAll()
    web.ignoring()
        .antMatchers("/index.html", "/static/**", "/login_p", "/sysUser/**")
        .antMatchers("/swagger-ui.html")
        .antMatchers("/webjars/**")
        .antMatchers("/v2/**")
        .antMatchers("/swagger-resources/**");
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
        //            加入url资源过滤器url权限过滤器
        .withObjectPostProcessor(
            new ObjectPostProcessor<FilterSecurityInterceptor>() {
              @Override
              public <O extends FilterSecurityInterceptor> O postProcess(O o) {
                o.setSecurityMetadataSource(urlFilterInvocationSecurityMetadataSource);
                o.setAccessDecisionManager(urlAccessDecisionManager);
                return o;
              }
            })
        .and()
        .formLogin()
        // 定义loginPage url为login_p抛出异常最终都会走login_p服务
        .loginPage("/login_p")
        .loginProcessingUrl("/login")
        .usernameParameter("username")
        .passwordParameter("password")
        .failureHandler(
            new AuthenticationFailureHandler() {
              @Override
              public void onAuthenticationFailure(
                  HttpServletRequest httpServletRequest,
                  HttpServletResponse httpServletResponse,
                  AuthenticationException e)
                  throws IOException, ServletException {
                httpServletResponse.setContentType("application/json;charset=utf-8");
                PrintWriter out = httpServletResponse.getWriter();
                StringBuffer sb = new StringBuffer();
                sb.append("{\"status\":\"error\",\"msg\":\"");
                if (e instanceof UsernameNotFoundException
                    || e instanceof BadCredentialsException) {
                  sb.append("用户名或密码输入错误，登录失败!");
                } else if (e instanceof DisabledException) {
                  sb.append("账户被禁用，登录失败，请联系管理员!");
                } else {
                  sb.append("登录失败!");
                }
                sb.append("\"}");
                out.write(sb.toString());
                out.flush();
                out.close();
              }
            })
        .successHandler(authenticationSuccessHandler)
        .and()
        .logout()
        .permitAll()
        //        .and().authorizeRequests()
        //            // 所有/trace/user/ 的所有请求 都放行
        //            .antMatchers("/trace/users/**").permitAll()
        //            // swagger start
        //            .antMatchers("/swagger-ui.html").permitAll()
        //            .antMatchers("/swagger-resources/**").permitAll()
        //            .antMatchers("/images/**").permitAll()
        //            .antMatchers("/webjars/**").permitAll()
        //            .antMatchers("/v2/**").permitAll()
        //            .antMatchers("/configuration/ui").permitAll()
        //            .antMatchers("/configuration/security").permitAll()
        //            // 除上面外的所有请求全部需要鉴权认证
        //            .anyRequest()
        //            .authenticated()
        //            // swagger end
        .and()
        // 由于使用的是JWT，我们这里不需要csrf
        .csrf()
        .disable()
        // 基于token，所以不需要session
        // 如果没有设置，则当jwttoken验证不通过例如jwttoken过期了，
        // 由于session还会保存账号信息导致后面依然能获取当前线程session对应登录者的权限，从而使得请求的url资源依然能够通过
        // 设置不需要session则每次請求都无法从session中获取到登录者权限信息，
        // 通过jwttoken验证，通过了便设置一次本次请求的一个临时session，供后面的url资源需要的权限匹配验证
        //            完事后应该是清空该session，使得下次的请求无法从session获得权限，
        // 需要jwttoken通过后从新设置一遍当前的权限

        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .exceptionHandling()
        .accessDeniedHandler(authenticationAccessDeniedHandler);

    //        // 对于获取token的rest api要允许匿名访问
    //        .antMatcher("/login/**")
    //        .permitAll()

    // 添加 filter
    http.addFilterBefore(jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
    http.addFilterAt(jwtTokenAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    // 禁用缓存
    http.headers().cacheControl();
  }

  /** 自定义登录过滤器 */
  private JwtTokenAuthenticationFilter jwtTokenAuthenticationFilter() throws Exception {
    JwtTokenAuthenticationFilter jwtTokenAuthenticationFilter = new JwtTokenAuthenticationFilter();
    jwtTokenAuthenticationFilter.setJwtUtil(jwtUtil);
    // SimpleUrlAuthenticationSuccessHandler successHandler = new
    // SimpleUrlAuthenticationSuccessHandler();
    // successHandler.setAlwaysUseDefaultTargetUrl(true);
    // successHandler.setDefaultTargetUrl("/user");

    jwtTokenAuthenticationFilter.setAuthenticationManager(this.authenticationManager());
    jwtTokenAuthenticationFilter.setAuthenticationSuccessHandler(authenticationSuccessHandler);
    return jwtTokenAuthenticationFilter;
  }
}
