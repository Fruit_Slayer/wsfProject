// package com.wsfPro.security.config;
//
// import com.alibaba.fastjson.JSON;
// import com.wsfPro.entities.system.SysUserEntity;
// import com.wsfPro.util.jwt.JwtUtil;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.core.Authentication;
// import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
// import org.springframework.stereotype.Component;
//
// import javax.servlet.ServletException;
// import javax.servlet.http.HttpServletRequest;
// import javax.servlet.http.HttpServletResponse;
// import java.io.IOException;
// import java.util.HashMap;
// import java.util.Map;
//
// @Component
// public class AjaxAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
//  @Autowired JwtUtil jwtUtil;
//
//  @Override
//  public void onAuthenticationSuccess(
//      HttpServletRequest request, HttpServletResponse response, Authentication authentication)
//      throws IOException, ServletException {
//    AjaxResponseBody responseBody = new AjaxResponseBody();
//
//    responseBody.setStatus("00");
//    responseBody.setMsg("Login Success!");
//
//    SysUserEntity sysUser = (SysUserEntity) authentication.getPrincipal();
//
//    Map map = new HashMap();
//    map.put("userName", sysUser.getUsername());
//    String jwtToken = jwtUtil.generateToken(map);
//    responseBody.setJwtToken(jwtToken);
//
//    response.getWriter().write(JSON.toJSONString(responseBody));
//  }
// }
