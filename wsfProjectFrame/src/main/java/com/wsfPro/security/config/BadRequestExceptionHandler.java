// package com.wsfPro.security.config;
//
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.http.HttpHeaders;
// import org.springframework.http.HttpStatus;
// import org.springframework.http.ResponseEntity;
// import org.springframework.validation.BindException;
// import org.springframework.validation.BindingResult;
// import org.springframework.validation.FieldError;
// import org.springframework.validation.ObjectError;
// import org.springframework.web.bind.annotation.ControllerAdvice;
// import org.springframework.web.context.request.WebRequest;
// import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
//
// import java.util.HashMap;
// import java.util.List;
// import java.util.Map;
//
//// Spring Boot的异常处理＋Bean Validation
//// Spring　Boot 中有一个专门处理错误信息的一个类叫做ResponseEntityExceptionHandler。
//// 其中有很多关于400的错误处理，也就是参数错误的处理，其中就有一个专门用来处理没有通过校验的参数的方法。
//// 我们重写这个类的这个方法即可。
//// 这时候controller中的方法中就不需要BindingResult或者Errors类型的参数了。
//// https://blog.csdn.net/starexplode/article/details/81198703
//
// @ControllerAdvice // Spring 的异常处理的注解
// public class BadRequestExceptionHandler extends ResponseEntityExceptionHandler {
//
//  //  private final Logger logger = LoggerFactory.getLogger(getClass());
//  private final Logger logger = LoggerFactory.getLogger(getClass());
//
//  @Override
//  protected ResponseEntity<Object> handleBindException(
//      BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
//
//    Map<String, String> messages = new HashMap<>();
//    BindingResult result = ex.getBindingResult();
//    if (result.hasErrors()) {
//      List<ObjectError> errors = result.getAllErrors();
//      for (ObjectError error : errors) {
//        FieldError fieldError = (FieldError) error;
//        messages.put(fieldError.getField(), fieldError.getDefaultMessage());
//      }
//      logger.error(messages.toString());
//    }
//    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(messages);
//  }
// }
