package com.wsfPro.security.config;

import com.wsfPro.controllers.dto.system.userDetails.UserDetailsImpl;
import com.wsfPro.core.base.SysConstants;
import com.wsfPro.core.system.SysUserService;
import com.wsfPro.util.jwt.JwtUtil;
import com.wsfPro.util.jwt.JwttokenconfigEnum;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@Component("authenticationSuccessHandlerImpl")
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {
  @Autowired private SysUserService sysUserService;
  @Autowired private JwtUtil jwtUtil;

  @Override
  public void onAuthenticationSuccess(
      HttpServletRequest httpServletRequest,
      HttpServletResponse httpServletResponse,
      Authentication authentication)
      throws IOException, ServletException {
    Map<String, Object> paramMap = new HashMap<>();
    //    SystemUserDto systemUserDto=null;
    UserDetailsImpl userDetails = null;
    try {
      userDetails = SecurityPrincipalContext.getSystemUserDto();
    } catch (Exception e) {
      userDetails = SecurityPrincipalContext.getUserDetailsImpl();
    }

    sysUserService.updUserParamByLoginSuccess(httpServletRequest);
    //        u.setLoginTime(new Timestamp(System.currentTimeMillis()));
    //        u.setLoginIp(IpUtil.getIpAddr1(httpServletRequest));
    //        sysUserService.update(u);
    paramMap.clear();
    httpServletResponse.setContentType("application/json;charset=utf-8");
    paramMap.put(JwttokenconfigEnum.jti.getValue(), userDetails.getId());
    paramMap.put(JwttokenconfigEnum.sub.getValue(), userDetails.getUsername());
    paramMap.put(SysConstants.PASSWORD.getValue(), userDetails.getPassword());
    paramMap.put(JwttokenconfigEnum.roles.getValue(), userDetails.getAuthorities());
    //    paramMap.put(JwttokenconfigEnum.aud.getValue(), user.getLoginIp());
    String jwtToken = jwtUtil.generateToken(paramMap);
    jwtUtil.addJwtTokenToRedisHash(userDetails.getId(), jwtToken);

    StringBuffer sb = new StringBuffer(jwtUtil.getJwttokenconfigBean().getTokenHead());
    sb.append(jwtToken);
    String jwtHeader = sb.toString();
    httpServletResponse.setHeader(jwtUtil.getJwttokenconfigBean().getHeader(), jwtHeader);
    PrintWriter out = httpServletResponse.getWriter();
    ObjectMapper objectMapper = new ObjectMapper();
    //    LoginItem login = new LoginItem(systemUserDto);
    String s =
        "{\"status\":\"success\",\"msg\":" + objectMapper.writeValueAsString(userDetails) + "}";
    out.write(s);

    out.flush();
    out.close();
  }
}
