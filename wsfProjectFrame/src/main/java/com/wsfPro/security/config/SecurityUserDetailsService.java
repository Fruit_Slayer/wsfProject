// package com.wsfPro.security.config;
//
// import com.wsfPro.entities.system.SysUserEntity;
// import com.wsfPro.repository.system.SysUserRepository;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.core.userdetails.UserDetails;
// import org.springframework.security.core.userdetails.UserDetailsService;
// import org.springframework.security.core.userdetails.UsernameNotFoundException;
// import org.springframework.stereotype.Service;
//
// import java.util.List;
//
// @Service
// public class SecurityUserDetailsService implements UserDetailsService {
//  @Autowired private SysUserRepository sysUserRepository;
//
//  @Override
//  public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
//    List<SysUserEntity> sysUserList = sysUserRepository.findByHql(SysUserEntity.class, "code", s);
//    if (sysUserList.size() > 1) {
//      throw new RuntimeException("loginName has same with" + sysUserList.size());
//    }
//    if (sysUserList.size() == 0) {
//      return null;
//    }
//    SysUserEntity su = sysUserList.get(0);
//    //    su.setRoles();
//    return su;
//  }
// }
