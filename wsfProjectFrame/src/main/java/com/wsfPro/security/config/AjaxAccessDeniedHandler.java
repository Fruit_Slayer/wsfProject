// package com.wsfPro.security.config;
//
// import com.wsfPro.util.HttpUtils;
// import org.springframework.security.access.AccessDeniedException;
// import org.springframework.security.web.access.AccessDeniedHandler;
// import org.springframework.stereotype.Service;
//
// import javax.servlet.ServletException;
// import javax.servlet.http.HttpServletRequest;
// import javax.servlet.http.HttpServletResponse;
// import java.io.IOException;
//
// @Service
// public class AjaxAccessDeniedHandler implements AccessDeniedHandler {
//  @Override
//  public void handle(
//      HttpServletRequest httpServletRequest,
//      HttpServletResponse httpServletResponse,
//      AccessDeniedException e)
//      throws IOException, ServletException {
//    if (HttpUtils.isAjaxRequest(httpServletRequest)) { // AJAX请求,使用response发送403
//      httpServletResponse.sendError(403);
//    } else if (!httpServletResponse.isCommitted()) { // 非AJAX请求，跳转系统默认的403错误界面，在web.xml中配置
//      httpServletResponse.sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
//    }
//  }
// }
