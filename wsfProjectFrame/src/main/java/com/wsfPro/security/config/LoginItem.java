// package com.wsfPro.security.config;
//
// import com.wsfPro.entities.system.SysMenuEntity;
// import com.wsfPro.entities.system.SysUserEntity;
// import com.wsfPro.dto.AbstractItem;
//
// import java.util.TreeSet;
//
// public class LoginItem extends AbstractItem {
//  private TreeSet<SysMenuEntity> menuSet;
//
//  private LoginItem() {};
//
//  public LoginItem(SysUserEntity user) {
//    this.setId(user.getId());
//    this.setName(user.getName());
//    this.setCreateDate(user.getCreateTime());
//    this.setUpdateDate(user.getUpdateTime());
//    this.setMenuSet(user.getMenus());
//  }
//
//  public TreeSet<SysMenuEntity> getMenuSet() {
//    return menuSet;
//  }
//
//  public void setMenuSet(TreeSet<SysMenuEntity> menuSet) {
//    this.menuSet = menuSet;
//  }
// }
