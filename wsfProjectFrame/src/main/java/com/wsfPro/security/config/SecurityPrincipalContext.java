package com.wsfPro.security.config;

import com.wsfPro.controllers.dto.system.SysUserDto;
import com.wsfPro.controllers.dto.system.userDetails.UserDetailsImpl;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public class SecurityPrincipalContext {
  //  public static Authentication getAuthentication() {
  //    return SecurityContextHolder.getContext().getAuthentication();
  //  }
  public static UserDetails getUserDetails() {
    return (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  }

  public static UserDetailsImpl getUserDetailsImpl() {
    return (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  }

  public static SysUserDto getSystemUserDto() {
    return (SysUserDto) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  }
  //  public static SysUserEntity getCurrentSysUser() {
  //    return (SysUserEntity)getAuthentication().getPrincipal();
  //  }

  //  public static Set<SysRoleEntity> getMaxLevelUserRoleSet() {
  //    List<SysRoleEntity> roleList = SysUserUtils.getCurrentSysUser().getRoles();
  //    //        账户有多个权限则按最大的算 如果权限等级相同全部获得
  //    HashSet<SysRoleEntity> set = new HashSet<SysRoleEntity>();
  //    SysRoleEntity bigRole = null;
  //    if (roleList.size() > 0) {
  //      bigRole = roleList.get(0);
  //      set.add(bigRole);
  //      for (int i = 1; i < roleList.size(); i++) {
  //        SysRoleEntity roleNext = roleList.get(i);
  //        if (bigRole.getLevel() > roleNext.getLevel()) {
  //          bigRole = roleNext;
  //          set.clear();
  //          set.add(bigRole);
  //        } else if (bigRole.getLevel() == roleNext.getLevel()) {
  //          bigRole = roleNext;
  //          set.add(bigRole);
  //        }
  //      }
  //      return set;
  //    }
  //    return null;
  //  }
}
