package com.wsfPro.entities;

import com.wsfPro.entities.File.Persistable;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public class AbstractPersistableEntity implements Persistable {

  /** */
  private static final long serialVersionUID = 889085121351086169L;

  @Id
  @Column(name = "c_id")
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  // 基类实体就不加name属性了有些不需要比如status状态只需数字属性不同数字代表不同状态

  @Column(name = "c_createAt")
  private Date createAt;

  // @Temporal标签的作用很简单：
  //
  // (1) 如果在某类中有Date类型的属性，数据库中存储可能是'yyyy-MM-dd
  // hh:MM:ss'要在查询时获得年月日，在该属性上标注@Temporal(TemporalType.DATE) 会得到形如'yyyy-MM-dd'
  // 格式的日期。
  // (2)如果在某类中有Date类型的属性，数据库中存储可能是'yyyy-MM-dd hh:MM:ss'要获得时分秒，在该属性上标注
  // @Temporal(TemporalType.TIME) 会得到形如'HH:MM:SS' 格式的日期。
  // (3)如果在某类中有Date类型的属性，数据库中存储可能是'yyyy-MM-dd hh:MM:ss'要获得'是'yyyy-MM-dd
  // hh:MM:ss'，在该属性上标注 @Temporal(TemporalType.TIMESTAMP) 会得到形如'HH:MM:SS' 格式的日期
  /** 最后一次的更新时间，如果人第一次添加，更新时间可以为空也可以为创建时间 */
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "c_updateAt")
  private Date updateAt;

  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  @Override
  public Long getId() {
    return id;
  }

  @Override
  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public boolean isNew() {
    return false;
  }

  public Date getCreateAt() {
    return createAt;
  }

  public void setCreateAt(Date createAt) {
    this.createAt = createAt;
  }

  public Date getUpdateAt() {
    return updateAt;
  }

  public void setUpdateAt(Date updateAt) {
    this.updateAt = updateAt;
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object obj) {
    if (null == obj) {
      return false;
    }
    if (this == obj) {
      return true;
    }
    if (!getClass().equals(obj.getClass())) {
      return false;
    }
    AbstractPersistableEntity that = (AbstractPersistableEntity) obj;
    return null == this.getId() ? false : this.getId().equals(that.getId());
  }
}
