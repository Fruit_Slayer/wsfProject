package com.wsfPro.entities.test;

import com.wsfPro.entities.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "t_test2")
public class TestEntity extends BaseEntity {

  /** */
  private static final long serialVersionUID = 7783668323110291753L;

  @Column(name = "numberTest")
  private Double numberTest;

  @Column(name = "numberTestInteger")
  private Integer numberTestInteger;

  public Double getNumberTest() {
    return numberTest;
  }

  public void setNumberTest(Double numberTest) {
    this.numberTest = numberTest;
  }

  public Integer getNumberTestInteger() {
    return numberTestInteger;
  }

  public void setNumberTestInteger(Integer numberTestInteger) {
    this.numberTestInteger = numberTestInteger;
  }
}
