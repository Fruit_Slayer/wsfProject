package com.wsfPro.entities.test;

import javax.persistence.*;

@Entity
@Table(name = "logtest")
public class LogTest {
  @Id
  @Column(name = "id")
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;

  @Column(name = "n")
  private String n;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getN() {
    return n;
  }

  public void setN(String n) {
    this.n = n;
  }
}
