package com.wsfPro.entities.system;

import com.wsfPro.entities.base.BaseCodeEntity;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Objects;
import java.util.TreeSet;

@Entity
@Table(name = "sys_role", schema = "db_wsfpro", catalog = "")
public class SysRoleEntity extends BaseCodeEntity implements GrantedAuthority {
  private String parentId;
  private HashSet<SysRoleEntity> childRoles;
  private TreeSet<SysMenuEntity> menus;
  //  权限级别以0开始为最大级别
  private int level;
  /** 非数据库字段 */
  private Boolean isCheck;

  //  SysRoleEntity(){
  ////    初始
  //    level=-1;
  //  }
  @Transient
  public HashSet<SysRoleEntity> getChildRoles() {
    return childRoles;
  }

  public void setChildRoles(HashSet<SysRoleEntity> childRoles) {
    this.childRoles = childRoles;
  }

  @Transient
  public TreeSet<SysMenuEntity> getMenus() {
    return menus;
  }

  /** @param menus */
  public void setMenus(TreeSet<SysMenuEntity> menus) {
    if (menus == null) {
      menus =
              new TreeSet<SysMenuEntity>(
                      new Comparator<SysMenuEntity>() {
                        @Override
                        public int compare(SysMenuEntity o1, SysMenuEntity o2) {
                          if (o1.getId().equals(o2.getId())) {
                            return 0;
                          }
                          return o1.getSort() - o2.getSort();
                        }
                      });
    }
    this.menus = menus;
  }

  @Basic
  @Column(name = "parentId", nullable = true, length = 18)
  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  @Transient
  public Boolean getIsCheck() {
    return isCheck;
  }

  public void setIsCheck(Boolean check) {
    this.isCheck = check;
  }

  @Basic
  @Column(name = "level", nullable = false, length = 4)
  public int getLevel() {
    return level;
  }

  public void setLevel(int level) {
    this.level = level;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof SysRoleEntity)) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    SysRoleEntity that = (SysRoleEntity) o;
    return this.getId().equals(that.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), parentId, childRoles, level, isCheck);
  }

  @Transient
  @Override
  public String getAuthority() {
    return this.getName();
  }
}
