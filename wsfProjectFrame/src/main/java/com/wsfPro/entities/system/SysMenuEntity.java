package com.wsfPro.entities.system;

import com.wsfPro.entities.base.BaseCodeEntity;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "sys_menu", schema = "db_wsfpro", catalog = "")
public class SysMenuEntity extends BaseCodeEntity {
  private String parentId;
  private String url;
  private int level;
  //  同级菜单排序
  private int sort;
  //   url: test1/test2  matchUrl: test1test2
  // 不需要matchUrl
  // 登录成功后因为返回了该账户的角色所拥有的所有菜单权限包括菜单id，
  // 所以使用 id作为url即可直接根据idkey查有无该url资源菜单
  //    private String matchUrl;

  private String icon;
  private Integer sequence;

  private Boolean isExpand;
  private String component;
  private Boolean isHidden;
  private String redirect;

  /** 1菜单 0按钮 */
  private Integer type;

  /** 是否已分配权限 */
  private Boolean isChecked;
  /** 子菜单 */
  private TreeSet<SysMenuEntity> childMenus;

  /** 菜单所拥有的所有权限 */
  private List<SysRoleEntity> sysRoleList;

  public static void setChilendMenu(
      SysMenuEntity parentMenu, List<SysMenuEntity> sysMenuEntityList) {
    TreeSet<SysMenuEntity> childMenuSet = parentMenu.getChildMenus();
    if (childMenuSet == null) {
      parentMenu.setChildMenus(null);
      childMenuSet = parentMenu.getChildMenus();
    }
    for (SysMenuEntity childenMenu : sysMenuEntityList) {
      if (childenMenu.getParentId() != null
          && childenMenu.getParentId().equals(parentMenu.getId())) {
        childMenuSet.add(childenMenu);
      }
    }
    sysMenuEntityList.remove(parentMenu);
    if (childMenuSet.size() == 0) {
      return;
    }
    for (SysMenuEntity childMenu : childMenuSet) {
      sysMenuEntityList.remove(childMenu);
    }
    for (SysMenuEntity nextParentMenu : childMenuSet) {
      setChilendMenu(nextParentMenu, sysMenuEntityList);
    }
  }

  public static TreeSet<SysMenuEntity> upDownMenu(List<SysMenuEntity> sysMenuEntityList) {

    if (sysMenuEntityList.size() > 1) {
      HashSet<SysMenuEntity> vertexMenuSet = new HashSet<SysMenuEntity>();
      //      还是要考虑多个顶级菜单
      //      菜单集合剩余都是顶点菜单集则排序结束

      while (!vertexMenuSet.containsAll(sysMenuEntityList)) {
        SysMenuEntity menuEntity = null;
        for (int i = 0; i < sysMenuEntityList.size(); i++) {
          menuEntity = sysMenuEntityList.get(i);
          if (!vertexMenuSet.contains(menuEntity)) {
            break;
          }
        }

        //        SysMenuEntity levelMenu = allReadySortMenuSet.first();
        //        if (levelMenu == null) {
        //          allReadySortMenuSet.add(menuEntity);
        //        } else {
        //          if (menuEntity.getLevel() < levelMenu.getLevel()) {
        //            allReadySortMenuSet.clear();
        //            allReadySortMenuSet.add(menuEntity);
        //          } else if (menuEntity.getLevel() == levelMenu.getLevel()) {
        //            allReadySortMenuSet.add(menuEntity);
        //          }
        //        }

        //        顶点菜单赋予其所有下级菜单并从菜单集合去除其所有下级菜单
        setChilendMenu(menuEntity, sysMenuEntityList);
        //     首选顶点菜单可能会存在上级需要重新加回来
        sysMenuEntityList.add(menuEntity);
        //        每个作为顶点菜单都有可能包含之前所有作为顶点菜单的顶点
        //        检验顶点菜单集合是否有被作其上级顶点菜单包含了
        Iterator<SysMenuEntity> vertexMenuIterator = vertexMenuSet.iterator();
        while (vertexMenuIterator.hasNext()) {
          SysMenuEntity vertexMenu = vertexMenuIterator.next();
          //          如果剩余菜单已经不存在顶点菜单则已经有其上级顶点菜单包含了需要移除其作为顶点菜单资格
          if (!sysMenuEntityList.contains(vertexMenu)) {
            vertexMenuIterator.remove();
          }
        }
        //        新顶点菜单
        vertexMenuSet.add(menuEntity);
      }
      //      setParentMenu(menuEntity, sysMenuEntityList);
      TreeSet<SysMenuEntity> realVertexMenuSet =
          new TreeSet<SysMenuEntity>(
              new Comparator<SysMenuEntity>() {
                @Override
                public int compare(SysMenuEntity o1, SysMenuEntity o2) {
                  if (o2.getId().equals(o1.getId())) {
                    return 0;
                  }
                  if (o1.getSort() - o2.getSort() == 0) {
                    return 1;
                  }
                  return o1.getSort() - o2.getSort();
                }
              });
      realVertexMenuSet.addAll(vertexMenuSet);
      return realVertexMenuSet;
    }
    return null;
  }

  @Basic
  @Column(name = "parentId", nullable = true, length = 18)
  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  @Basic
  @Column(name = "level", nullable = false, length = 8)
  public int getLevel() {
    return level;
  }

  //  @Basic
  //  @Column(name = "matchUrl", nullable = true, length = 500)
  //  public String getMatchUrl() {
  //    return matchUrl;
  //  }
  //
  //  public void setMatchUrl(String matchUrl) {
  //    this.matchUrl = matchUrl;
  //  }

  public void setLevel(int level) {
    this.level = level;
  }

  @Basic
  @Column(name = "url", nullable = true, length = 500, unique = true)
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  @Basic
  @Column(name = "sort", nullable = false, length = 8)
  public int getSort() {
    return sort;
  }

  public void setSort(int sort) {
    this.sort = sort;
  }

  @Basic
  @Column(name = "icon", nullable = true, length = 255)
  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  @Basic
  @Column(name = "sequence", nullable = true)
  public Integer getSequence() {
    return sequence;
  }

  public void setSequence(Integer sequence) {
    this.sequence = sequence;
  }

  @Basic
  @Column(name = "isExpand", nullable = true)
  public Boolean getExpand() {
    return isExpand;
  }

  public void setExpand(Boolean expand) {
    isExpand = expand;
  }

  @Basic
  @Column(name = "component", nullable = true, length = 255)
  public String getComponent() {
    return component;
  }

  public void setComponent(String component) {
    this.component = component;
  }

  @Basic
  @Column(name = "isHidden", nullable = true)
  public Boolean getIsHidden() {
    return isHidden;
  }

  public void setIsHidden(Boolean hidden) {
    isHidden = hidden;
  }

  @Basic
  @Column(name = "redirect", nullable = true)
  public String getRedirect() {
    return redirect;
  }

  public void setRedirect(String redirect) {
    this.redirect = redirect;
  }

  @Basic
  @Column(name = "type", nullable = true)
  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  /** 非数据库字段 */
  @Transient
  public TreeSet<SysMenuEntity> getChildMenus() {
    return childMenus;
  }

  public void setChildMenus(TreeSet<SysMenuEntity> childMenus) {
    if (childMenus == null) {
      childMenus =
          new TreeSet<SysMenuEntity>(
              new Comparator<SysMenuEntity>() {
                @Override
                public int compare(SysMenuEntity o1, SysMenuEntity o2) {
                  if (o1.getId().equals(o2.getId())) {
                    return 0;
                  }
                  if (o1.getSort() == o2.getSort()) {
                    return 1;
                  }
                  return o1.getSort() - o2.getSort();
                }
              });
    }
    this.childMenus = childMenus;
  }

  @Transient
  public Boolean getIsChecked() {
    return isChecked;
  }

  public void setIsChecked(Boolean isChecked) {
    this.isChecked = isChecked;
  }

  @Transient
  public List<SysRoleEntity> getSysRoleList() {
    return sysRoleList;
  }

  public void setSysRoleList(List<SysRoleEntity> sysRoleList) {
    this.sysRoleList = sysRoleList;
  }

  public void setSortMenu(List<SysMenuEntity> sysMenuEntityList) {
    for (SysMenuEntity sysMenuEntity : sysMenuEntityList) {}
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof SysMenuEntity)) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    SysMenuEntity that = (SysMenuEntity) o;
    return Objects.equals(parentId, that.parentId)
        && Objects.equals(url, that.url)
        && Objects.equals(icon, that.icon)
        && Objects.equals(sequence, that.sequence)
        && Objects.equals(isExpand, that.isExpand)
        && Objects.equals(component, that.component)
        && Objects.equals(isHidden, that.isHidden)
        && Objects.equals(redirect, that.redirect)
        && Objects.equals(type, that.type)
        && Objects.equals(isChecked, that.isChecked)
        && Objects.equals(childMenus, that.childMenus)
        && Objects.equals(sysRoleList, that.sysRoleList);
  }

  @Override
  public int hashCode() {

    return Objects.hash(
        super.hashCode(),
        parentId,
        url,
        icon,
        sequence,
        isExpand,
        component,
        isHidden,
        redirect,
        type,
        isChecked,
        childMenus,
        sysRoleList);
  }
}
