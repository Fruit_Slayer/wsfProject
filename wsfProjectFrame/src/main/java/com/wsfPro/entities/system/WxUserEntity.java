package com.wsfPro.entities.system;

import com.wsfPro.core.base.TokenConstants;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

/** @author: eiven @Date: Created in 13:55 2018/6/21 */
@Entity
@Table(name = "wx_user", schema = "db_wsfpro", catalog = "")
public class WxUserEntity {
  private String id;
  private String orgId;
  private String deptId;
  private String openId;
  private String userToken;
  private String password;
  private String cnName;
  private String enName;
  private Integer age;
  private Integer sex;
  private Timestamp birthday;
  private String identityCard;
  private String email;
  private String phone;
  private String mobile;
  private String photo;
  private String remarks;
  private Boolean isDelete;

  public WxUserEntity() {
    this.id = TokenConstants.generateUserId();
  }

  @Id
  @Column(name = "id", nullable = false, length = 18)
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @Basic
  @Column(name = "orgId", nullable = true, length = 18)
  public String getOrgId() {
    return orgId;
  }

  public void setOrgId(String orgId) {
    this.orgId = orgId;
  }

  @Basic
  @Column(name = "deptId", nullable = true, length = 18)
  public String getDeptId() {
    return deptId;
  }

  public void setDeptId(String deptId) {
    this.deptId = deptId;
  }

  @Basic
  @Column(name = "openId", nullable = false, length = 64)
  public String getOpenId() {
    return openId;
  }

  public void setOpenId(String openId) {
    this.openId = openId;
  }

  @Basic
  @Column(name = "userToken", nullable = true, length = 32)
  public String getUserToken() {
    return userToken;
  }

  public void setUserToken(String userToken) {
    this.userToken = userToken;
  }

  @Basic
  @Column(name = "password", nullable = true, length = 200)
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Basic
  @Column(name = "cnName", nullable = true, length = 200)
  public String getCnName() {
    return cnName;
  }

  public void setCnName(String cnName) {
    this.cnName = cnName;
  }

  @Basic
  @Column(name = "enName", nullable = true, length = 200)
  public String getEnName() {
    return enName;
  }

  public void setEnName(String enName) {
    this.enName = enName;
  }

  @Basic
  @Column(name = "age", nullable = true)
  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  @Basic
  @Column(name = "sex", nullable = true)
  public Integer getSex() {
    return sex;
  }

  public void setSex(Integer sex) {
    this.sex = sex;
  }

  @Basic
  @Column(name = "birthday", nullable = true)
  public Timestamp getBirthday() {
    return birthday;
  }

  public void setBirthday(Timestamp birthday) {
    this.birthday = birthday;
  }

  @Basic
  @Column(name = "identityCard", nullable = true, length = 18)
  public String getIdentityCard() {
    return identityCard;
  }

  public void setIdentityCard(String identityCard) {
    this.identityCard = identityCard;
  }

  @Basic
  @Column(name = "email", nullable = true, length = 200)
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Basic
  @Column(name = "phone", nullable = true, length = 200)
  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  @Basic
  @Column(name = "mobile", nullable = true, length = 200)
  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  @Basic
  @Column(name = "photo", nullable = true, length = 1000)
  public String getPhoto() {
    return photo;
  }

  public void setPhoto(String photo) {
    this.photo = photo;
  }

  @Basic
  @Column(name = "remarks", nullable = true, length = 800)
  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

  @Basic
  @Column(name = "isDelete", nullable = true)
  public Boolean getDelete() {
    return isDelete;
  }

  public void setDelete(Boolean delete) {
    isDelete = delete;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    WxUserEntity that = (WxUserEntity) o;
    return Objects.equals(id, that.id)
        && Objects.equals(orgId, that.orgId)
        && Objects.equals(deptId, that.deptId)
        && Objects.equals(openId, that.openId)
        && Objects.equals(userToken, that.userToken)
        && Objects.equals(password, that.password)
        && Objects.equals(cnName, that.cnName)
        && Objects.equals(enName, that.enName)
        && Objects.equals(age, that.age)
        && Objects.equals(sex, that.sex)
        && Objects.equals(birthday, that.birthday)
        && Objects.equals(identityCard, that.identityCard)
        && Objects.equals(email, that.email)
        && Objects.equals(phone, that.phone)
        && Objects.equals(mobile, that.mobile)
        && Objects.equals(photo, that.photo)
        && Objects.equals(remarks, that.remarks)
        && Objects.equals(isDelete, that.isDelete);
  }

  @Override
  public int hashCode() {

    return Objects.hash(
        id,
        orgId,
        deptId,
        openId,
        userToken,
        password,
        cnName,
        enName,
        age,
        sex,
        birthday,
        identityCard,
        email,
        phone,
        mobile,
        photo,
        remarks,
        isDelete);
  }
}
