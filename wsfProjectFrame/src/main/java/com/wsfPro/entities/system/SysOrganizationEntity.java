package com.wsfPro.entities.system;

import com.wsfPro.entities.base.BaseEntity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "sys_organization", schema = "db_wsfpro", catalog = "")
public class SysOrganizationEntity extends BaseEntity {
  private String address;
  private String phone;
  private String fax;
  private String email;

  @Basic
  @Column(name = "address", nullable = true, length = 255)
  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  @Basic
  @Column(name = "phone", nullable = true, length = 200)
  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  @Basic
  @Column(name = "fax", nullable = true, length = 200)
  public String getFax() {
    return fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  @Basic
  @Column(name = "email", nullable = true, length = 200)
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}
