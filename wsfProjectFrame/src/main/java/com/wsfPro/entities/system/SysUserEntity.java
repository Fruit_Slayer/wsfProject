package com.wsfPro.entities.system;

import com.wsfPro.controllers.dto.system.SysUserDto;
import com.wsfPro.entities.base.BaseCodeEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;
import java.util.*;

/** @author eiven */
@Entity
@Table(name = "sys_user", schema = "db_wsfpro", catalog = "")
public class SysUserEntity extends BaseCodeEntity implements UserDetails {

  private String password;
  //  所有权限
  private List<SysRoleEntity> roles;
  // 该账户可访问的所有url菜单资源
  private TreeSet<SysMenuEntity> menus;
  private Integer age;
  private boolean sex;
  private Timestamp birthTime;
  private Timestamp birthday;
  private String identityCard;
  private String email;
  private String phone;
  private String mobile;
  private String photo;
  private String loginIp;
  //  private Timestamp loginOutTime;
  private boolean accountNonExpired;
  private boolean accountNonLocked;
  private boolean credentialsNonExpired;
  private boolean enabled;

  public static Set<SysRoleEntity> getMaxLevelSysRoleSet(List<SysRoleEntity> roleList) {
    //    List<SysRoleEntity> roleList = SysUserUtils.getCurrentSysUser().getRoles();
    //        账户有多个权限则按最大的算 如果权限等级相同全部获得
    HashSet<SysRoleEntity> set = new HashSet<SysRoleEntity>();
    SysRoleEntity bigRole = null;
    if (roleList.size() > 0) {
      bigRole = roleList.get(0);
      set.add(bigRole);
      for (int i = 1; i < roleList.size(); i++) {
        SysRoleEntity roleNext = roleList.get(i);
        if (bigRole.getLevel() > roleNext.getLevel()) {
          bigRole = roleNext;
          set.clear();
          set.add(bigRole);
        } else if (bigRole.getLevel() == roleNext.getLevel()) {
          bigRole = roleNext;
          set.add(bigRole);
        }
      }
      return set;
    }
    return null;
  }

  @Transient
  @Override
  public String getUsername() {
    return super.getCode();
  }

  public void setUsename(String userName) {
    super.setCode(userName);
  }

  @NotBlank(message = "密码不能为空")
  @Override
  @Basic
  @Column(name = "password", nullable = false, length = 200)
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Basic
  @Column(name = "age", nullable = true)
  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  @Basic
  @Column(name = "sex", nullable = false)
  public boolean getSex() {
    return sex;
  }

  public void setSex(boolean sex) {
    this.sex = sex;
  }

  @Basic
  @Column(name = "birthTime", nullable = false)
  public Timestamp getBirthTime() {
    return birthTime;
  }

  public void setBirthTime(Timestamp birthTime) {
    this.birthTime = birthTime;
  }

  @Basic
  @Column(name = "birthday", nullable = true)
  public Timestamp getBirthday() {
    return birthday;
  }

  public void setBirthday(Timestamp birthday) {
    this.birthday = birthday;
  }

  @Basic
  @Column(name = "identityCard", nullable = false, length = 18)
  public String getIdentityCard() {
    return identityCard;
  }

  public void setIdentityCard(String identityCard) {
    this.identityCard = identityCard;
  }

  @Basic
  @Column(name = "email", nullable = false, length = 200)
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Basic
  @Column(name = "phone", nullable = true, length = 200)
  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  @Basic
  @Column(name = "mobile", nullable = false, length = 200)
  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  @Basic
  @Column(name = "photo", nullable = true, length = 1000)
  public String getPhoto() {
    return photo;
  }

  public void setPhoto(String photo) {
    this.photo = photo;
  }

  @Basic
  @Column(name = "loginIp", nullable = true, length = 100)
  public String getLoginIp() {
    return loginIp;
  }

  public void setLoginIp(String loginIp) {
    this.loginIp = loginIp;
  }

  // 帐户是否过期
  @Override
  public boolean isAccountNonExpired() {
    return this.accountNonExpired;
  }

  public void setAccountNonExpired(boolean accountNonExpired) {
    this.accountNonExpired = accountNonExpired;
  }

  // 帐户是否被冻结
  @Override
  public boolean isAccountNonLocked() {
    return this.accountNonLocked;
  }

  //  @Basic
  //  @Column(name = "loginOutTime", nullable = false)
  //  public Timestamp getLoginOutTime() {
  //    return loginOutTime;
  //  }
  //
  //  public void setLoginOutTime(Timestamp loginOutTime) {
  //    this.loginOutTime = loginOutTime;
  //  }

  public void setAccountNonLocked(boolean accountNonLocked) {
    this.accountNonLocked = accountNonLocked;
  }

  // 帐户密码是否过期，一般有的密码要求性高的系统会使用到，比较每隔一段时间就要求用户重置密码
  @Basic
  @Column(name = "credentialsNonExpired", nullable = false)
  @Override
  public boolean isCredentialsNonExpired() {
    return this.credentialsNonExpired;
  }

  public void setCredentialsNonExpired(boolean credentialsNonExpired) {
    this.credentialsNonExpired = credentialsNonExpired;
  }

  @Override
  public boolean isEnabled() {
    return this.enabled;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  /**
   * UserDetails需要实现的权限集
   *
   * @return
   */
  @Transient
  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    //    List<GrantedAuthority> authorities = new ArrayList<>();
    //    if (roles != null) {
    //      for (SysRoleEntity role : roles) {
    //        authorities.add(new SimpleGrantedAuthority(role.getName()));
    //      }
    //    }
    //
    //    return authorities;
    //    List<GrantedAuthority> authorities = new ArrayList<>();
    //        if (roles != null) {
    //      for (SysRoleEntity role : roles) {
    //        authorities.add(new SimpleGrantedAuthority(role.getName()));
    //      }
    //    }
    return roles;
  }

  @Transient
  public List<SysRoleEntity> getRoles() {
    return roles;
  }

  public void setRoles(List<SysRoleEntity> roles) {
    this.roles = roles;
  }

  @Transient
  public List<SysRoleEntity> getCopyRoles() {
    return new ArrayList<>(roles);
  }

  @Transient
  public TreeSet<SysMenuEntity> getMenus() {
    return menus;
  }

  public void setMenus(TreeSet<SysMenuEntity> menus) {
    this.menus = menus;
  }
  //  @Transient
  //  public  Set<SysRoleEntity> getMaxLevelUserRoleSet() {
  //    List<SysRoleEntity> roleList = this.getRoles();
  //    //        账户有多个权限则按最大的算 如果权限等级相同全部获得
  //    HashSet<SysRoleEntity> set = new HashSet<SysRoleEntity>();
  //    SysRoleEntity bigRole = null;
  //    if (roleList.size() > 0) {
  //      bigRole = roleList.get(0);
  //      set.add(bigRole);
  //      for (int i = 1; i < roleList.size(); i++) {
  //        SysRoleEntity roleNext = roleList.get(i);
  //        if (bigRole.getLevel() > roleNext.getLevel()) {
  //          bigRole = roleNext;
  //          set.clear();
  //          set.add(bigRole);
  //        } else if (bigRole.getLevel() == roleNext.getLevel()) {
  //          bigRole = roleNext;
  //          set.add(bigRole);
  //        }
  //      }
  //      return set;
  //    }
  //    return null;
  //  }

  public SysUserDto createSystemUserDto() {
    HashSet<SimpleGrantedAuthority> simpleGrantedAuthorities =
        new HashSet<SimpleGrantedAuthority>();
    for (GrantedAuthority grantedAuthority : this.getAuthorities()) {
      simpleGrantedAuthorities.add(new SimpleGrantedAuthority(grantedAuthority.getAuthority()));
    }
    return new SysUserDto(
        this.getId(),
        this.getUsername(),
        this.getPassword(),
        simpleGrantedAuthorities,
        this.menus,
        this.isAccountNonExpired(),
        this.isAccountNonLocked(),
        this.isCredentialsNonExpired(),
        this.isEnabled());
  }
}
