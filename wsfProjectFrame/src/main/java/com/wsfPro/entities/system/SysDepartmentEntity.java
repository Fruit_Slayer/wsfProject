package com.wsfPro.entities.system;

import com.wsfPro.entities.base.BaseEntity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "sys_department", schema = "db_wsfpro", catalog = "")
public class SysDepartmentEntity extends BaseEntity {

  private String orgId;
  private String parentId;
  private Integer sequence;
  private String phone;
  private String fax;
  private String principal;


  @Basic
  @Column(name = "orgId", nullable = true, length = 18)
  public String getOrgId() {
    return orgId;
  }

  public void setOrgId(String orgId) {
    this.orgId = orgId;
  }

  @Basic
  @Column(name = "parentId", nullable = true, length = 18)
  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  @Basic
  @Column(name = "sequence", nullable = true)
  public Integer getSequence() {
    return sequence;
  }

  public void setSequence(Integer sequence) {
    this.sequence = sequence;
  }

  @Basic
  @Column(name = "phone", nullable = true, length = 100)
  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  @Basic
  @Column(name = "fax", nullable = true, length = 100)
  public String getFax() {
    return fax;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  @Basic
  @Column(name = "principal", nullable = true, length = 18)
  public String getPrincipal() {
    return principal;
  }

  public void setPrincipal(String principal) {
    this.principal = principal;
  }


}
