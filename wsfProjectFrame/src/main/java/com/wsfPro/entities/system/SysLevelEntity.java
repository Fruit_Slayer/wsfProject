package com.wsfPro.entities.system;

import com.wsfPro.entities.base.BaseDataEntity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "sys_level", schema = "db_wsfpro", catalog = "")
public class SysLevelEntity extends BaseDataEntity {

    private int level;

    public SysLevelEntity(){}
    public SysLevelEntity(int level) {
        this.level = level;
    }

    @Basic
    @Column(name = "level", nullable = true, length = 4)
    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

}
