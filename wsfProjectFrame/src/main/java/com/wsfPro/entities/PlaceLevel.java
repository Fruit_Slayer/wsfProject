package com.wsfPro.entities;
/**
 * 地区的级别
 *
 * @author jackson
 */
public enum PlaceLevel {

  /** 国家 */
  COUNTRY("国家"),
  /** 省 */
  PROVINCE("省(区、市)"),
  /** 市 */
  CITY("市(州)"),
  /** 县 */
  COUNTY("县(市、区、旗)"),
  /** 镇 */
  TOWN("镇(乡、街道)"),
  /** 村 */
  VILLAGE("行政村"),
  /** 其它 */
  OTHER("其它");
  private final String value;

  private PlaceLevel(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  public static boolean isProvince(int level) {
    return level == PROVINCE.ordinal();
  }

  public static boolean isCity(int level) {
    return level == CITY.ordinal();
  }

  public static boolean isCounty(int level) {
    return level == COUNTY.ordinal();
  }

  public static boolean isTown(int level) {
    return level == TOWN.ordinal();
  }

  public static boolean isVillage(int level) {
    return level == VILLAGE.ordinal();
  }
}
