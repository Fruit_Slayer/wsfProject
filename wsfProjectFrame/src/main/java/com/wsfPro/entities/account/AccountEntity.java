package com.wsfPro.entities.account;

import com.wsfPro.entities.File.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "t_account")
public class AccountEntity extends AbstractPersistable {

  /** */
  private static final long serialVersionUID = -1794134426344427935L;

  @Column(name = "C_NN")
  private String nickName;

  @Column(name = "C_RN")
  private String realName;

  @Column(name = "C_PW")
  private String passWord;

  // 头像
  @Column(name = "C_HP")
  private String headPortrait;

  @Column(name = "C_R")
  private String root;

  @Column(name = "C_CD")
  private String createDate;

  @Column(name = "C_EA")
  private String emailAddress;

  public String getNickName() {
    return nickName;
  }

  public void setNickName(String nickName) {
    this.nickName = nickName;
  }

  public String getRealName() {
    return realName;
  }

  public void setRealName(String realName) {
    this.realName = realName;
  }

  public String getPassWord() {
    return passWord;
  }

  public void setPassWord(String passWord) {
    this.passWord = passWord;
  }

  public String getHeadPortrait() {
    return headPortrait;
  }

  public void setHeadPortrait(String headPortrait) {
    this.headPortrait = headPortrait;
  }

  public String getRoot() {
    return root;
  }

  public void setRoot(String root) {
    this.root = root;
  }

  public String getCreateDate() {
    return createDate;
  }

  public void setCreateDate(String createDate) {
    this.createDate = createDate;
  }

  public String getEmailAddress() {
    return emailAddress;
  }

  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }
}
