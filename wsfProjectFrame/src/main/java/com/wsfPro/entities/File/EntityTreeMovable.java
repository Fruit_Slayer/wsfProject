package com.wsfPro.entities.File;

public interface EntityTreeMovable<E extends EntityTreeMovable<E>>
    extends EntityTree<E>, EntityMovable {}
