package com.wsfPro.entities.File;

/**
 *
 *
 * <pre>
 * 表示该实体需要限制用户的操作范围，
 * 如：
 * 如果使用域范围限制，表示操作该表的记录时，必须满足该用户的ownerid与
 * 被操作的记录的ownerid一致，也就是相同域才允许操作
 * </pre>
 *
 * @author jackson
 */
public interface EntityScopable {

  Long getCreatorId();

  long getOwnerId();
}
