package com.wsfPro.entities.File;

import java.io.Serializable;

public interface Persistable extends Serializable {

  /**
   * 实体在数据库中的ID,唯一值。
   *
   * @return ID
   */
  Long getId();

  void setId(Long id);

  boolean isNew();
}
