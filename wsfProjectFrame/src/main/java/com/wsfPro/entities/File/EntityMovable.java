package com.wsfPro.entities.File;

public interface EntityMovable extends Persistable {

  int getOrdinal();

  void setOrdinal(int ordinal);
}
