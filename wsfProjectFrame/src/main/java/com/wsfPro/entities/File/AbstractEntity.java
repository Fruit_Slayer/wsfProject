package com.wsfPro.entities.File;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 *
 *
 * <pre>
 * 该实体记录对数据库操作的创建人、创建时间及修改操作时的修改人、修改时间、备注
 * 是否删除
 * 所有者(ownerid)
 * </pre>
 *
 * @author jackson
 * @version 1.1
 */
@MappedSuperclass
public abstract class AbstractEntity extends BaseEntity implements EntityScopable {

  /** */
  private static final long serialVersionUID = -1327198790530653756L;

  /**
   *
   *
   * <pre>
   * 该数据的所有者,跟创建者creatorId不一样.这里应该是指向该创建者所属的企业,组织等.
   * 有些角色的访问可以访问他所属的企业组织等.如:企业的管理人员,那就应该可以访问员工所创建的目录,文件等.
   * 但普通的员工就只能查看他自己的目录或文件.
   * </pre>
   */
  @Column(name = "C_OWNERID")
  private long ownerId;

  public long getOwnerId() {
    return ownerId;
  }

  public void setOwnerId(long ownerId) {
    this.ownerId = ownerId;
  }
}
