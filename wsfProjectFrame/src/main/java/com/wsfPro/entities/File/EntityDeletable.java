package com.wsfPro.entities.File;

public interface EntityDeletable extends Persistable {

  boolean isDeleted();

  void setDeleted(boolean deleted);
}
