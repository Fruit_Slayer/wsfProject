package com.wsfPro.entities.log;

import com.wsfPro.entities.AbstractPersistableEntity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class SuperLog extends AbstractPersistableEntity {
  @Column(name = "c_className")
  private String className;

  @Column(name = "c_method")
  private String method;

  @Column(name = "c_logLevel")
  private String logLevel;

  @Column(name = "c_msg")
  private String msg;

  public String getClassName() {
    return className;
  }

  public void setClassName(String className) {
    this.className = className;
  }

  public String getMethod() {
    return method;
  }

  public void setMethod(String method) {
    this.method = method;
  }

  public String getLogLevel() {
    return logLevel;
  }

  public void setLogLevel(String logLevel) {
    this.logLevel = logLevel;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }
}
