package com.wsfPro.entities.log;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "t_log")
public class LogEntity extends SuperLog {

  /** */
  private static final long serialVersionUID = 5251238703399894614L;

  @Column(name = "c_url")
  private String url;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }
}
