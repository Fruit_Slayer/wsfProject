package com.wsfPro.entities.middle;

import java.util.Objects;

public class SysRoleLevelEntityPK {
  private String roleId;
  private String levelId;

  public String getRoleId() {
    return roleId;
  }

  public void setRoleId(String roleId) {
    this.roleId = roleId;
  }

  public String getLevelId() {
    return levelId;
  }

  public void setLevelId(String levelId) {
    this.levelId = levelId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof SysRoleLevelEntityPK)) return false;
    SysRoleLevelEntityPK that = (SysRoleLevelEntityPK) o;
    return Objects.equals(roleId, that.roleId) && Objects.equals(levelId, that.levelId);
  }

  @Override
  public int hashCode() {

    return Objects.hash(roleId, levelId);
  }
}
