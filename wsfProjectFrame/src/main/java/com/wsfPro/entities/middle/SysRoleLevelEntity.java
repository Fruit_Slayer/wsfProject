//package com.wsfPro.entities.system;
//
//import javax.persistence.*;
//import java.util.Objects;
//
//@Entity
//@Table(name = "sys_role_level", schema = "db_wsfpro", catalog = "")
//@IdClass(SysRoleLevelEntityPK.class)
//public class SysRoleLevelEntity {
//    private String roleId;
//    private String levelId;
//    @Id
//    @Column(name = "roleId", nullable = false, length = 18)
//    public String getRoleId() {
//        return roleId;
//    }
//
//    public void setRoleId(String roleId) {
//        this.roleId = roleId;
//    }
//    @Id
//    @Column(name = "levelId", nullable = false, length = 18)
//    public String getLevelId() {
//        return levelId;
//    }
//
//    public void setLevelId(String levelId) {
//        this.levelId = levelId;
//    }
//
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof SysRoleLevelEntity)) return false;
//        SysRoleLevelEntity that = (SysRoleLevelEntity) o;
//        return Objects.equals(roleId, that.roleId) &&
//                Objects.equals(levelId, that.levelId);
//    }
//
//    @Override
//    public int hashCode() {
//
//        return Objects.hash(roleId, levelId);
//    }
//}
