package com.wsfPro.entities.base;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseRemarksEntity extends BaseCodeEntity {
    private String remarks;

    @Basic
    @Column(name = "remarks", nullable = true, length = 800)
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
