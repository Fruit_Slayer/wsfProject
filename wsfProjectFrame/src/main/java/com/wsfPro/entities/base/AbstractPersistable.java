package com.wsfPro.entities.base;

import com.wsfPro.repository.Persistable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Objects;

@MappedSuperclass
public class AbstractPersistable implements Persistable<String> {
  private String id;

  @Override
  @Id
  @Column(name = "id", nullable = false, length = 18)
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  //  @Override
  //  public boolean isNew() {
  //    return false;
  //  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof AbstractPersistable)) {
      return false;
    }
    AbstractPersistable that = (AbstractPersistable) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {

    return Objects.hash(id);
  }
}
