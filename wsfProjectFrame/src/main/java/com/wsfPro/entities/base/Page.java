package com.wsfPro.entities.base;

import java.util.Collections;
import java.util.List;
/**
 * 分页的数据结构
 *
 * @author jackson
 */
public class Page {

  public static Page EMPTY = new Page(Collections.emptyList());

  private final long total;
  private List<Object> rows;
  //	private List<Object> footer;

  public Page(List<Object> rows) {
    this(rows.size(), rows);
  }

  public Page(long total, List<Object> rows) {
    this.total = total;
    this.rows = rows;
  }

  public Page convert(List<Object> rows) {
    this.rows = rows;
    return this;
  }

  public long getTotal() {
    return total;
  }

  public List<Object> getRows() {
    return rows;
  }

  /**
   * 根据页长计算总页数
   *
   * @param pageSize
   * @return
   */
  public int getTotalPage(int pageSize) {
    if (this.getTotal() % pageSize == 0) {
      return (int) (this.getTotal() / pageSize);
    }
    return (int) (this.getTotal() / pageSize + 1);
  }

  //	public List<Object> getFooter() {
  //		return footer;
  //	}
}
