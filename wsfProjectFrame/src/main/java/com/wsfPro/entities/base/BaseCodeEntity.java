package com.wsfPro.entities.base;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotBlank;

@MappedSuperclass
public abstract class BaseCodeEntity extends BaseDataEntity {
  private String code;

  @NotBlank(message = "code不能为空")
  @Basic
  @Column(name = "code", nullable = false, length = 64, unique = true)
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
