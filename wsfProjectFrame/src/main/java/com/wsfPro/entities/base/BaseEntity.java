package com.wsfPro.entities.base;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseEntity extends AbstractPersistable {

  // delete为数据库关键属性无法使用
  private Boolean isDelete;

  @Basic
  @Column(name = "isDelete", nullable = false)
  public Boolean getIsDelete() {
    return isDelete;
  }

  public void setIsDelete(Boolean isDelete) {
    this.isDelete = isDelete;
  }

  //
  //  @Ignore
  //  public void generateID() {
  //    this.setId(TokenConstants.generateUserId());
  //  }
}
