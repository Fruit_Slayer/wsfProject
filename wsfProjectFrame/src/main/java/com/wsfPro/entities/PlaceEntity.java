// package com.wsfPro.entities;
//
// import javax.persistence.*;
// import java.util.Collection;
// import java.util.Objects;
//
//// , schema = "epos"
// @Entity
// @Table(name = "place")
// public class PlaceEntity {
//
//  /** 地区编码； 12位的地区编码：省(2)+市(2)+县(2)+镇(3)+村(3) 共12位 */
//  private String id;
//  /** 级别 eg：国家级别：0 省级：1 依次递增 */
//  private PlaceLevel level;
//
//  private String name;
//  /** 经度 */
//  private Double longitude;
//  /** 纬度 */
//  private Double latitude;
//  /** 上级地区 */
//  private PlaceEntity placeByRParentId;
//  /** 下级地区 */
//  private Collection<PlaceEntity> placesById;
//
//  @Id
//  @Column(name = "id")
//  public String getId() {
//    return id;
//  }
//
//  public void setId(String id) {
//    this.id = id;
//  }
//
//  @Basic
//  @Column(name = "level")
//  public PlaceLevel getLevel() {
//    return level;
//  }
//
//  public void setLevel(PlaceLevel level) {
//    this.level = level;
//  }
//
//  @Basic
//  @Column(name = "name")
//  public String getName() {
//    return name;
//  }
//
//  public void setName(String name) {
//    this.name = name;
//  }
//
//  @Basic
//  @Column(name = "longitude")
//  public Double getLongitude() {
//    return longitude;
//  }
//
//  public void setLongitude(Double longitude) {
//    this.longitude = longitude;
//  }
//
//  @Basic
//  @Column(name = "latitude")
//  public Double getLatitude() {
//    return latitude;
//  }
//
//  public void setLatitude(Double latitude) {
//    this.latitude = latitude;
//  }
//
//  @Override
//  public boolean equals(Object o) {
//    if (this == o) {
//      return true;
//    }
//    if (o == null || getClass() != o.getClass()) {
//      return false;
//    }
//    PlaceEntity that = (PlaceEntity) o;
//    return Objects.equals(id, that.id)
//        && Objects.equals(level, that.level)
//        && Objects.equals(name, that.name)
//        && Objects.equals(longitude, that.longitude)
//        && Objects.equals(latitude, that.latitude);
//  }
//
//  @Override
//  public int hashCode() {
//
//    return Objects.hash(id, level, name, longitude, latitude);
//  }
//
//  @ManyToOne
//  @JoinColumn(name = "r_parent_id", referencedColumnName = "id")
//  public PlaceEntity getPlaceByRParentId() {
//    return placeByRParentId;
//  }
//
//  public void setBasePlaceByRParentId(PlaceEntity placeByRParentId) {
//    this.placeByRParentId = placeByRParentId;
//  }
//
//  @OneToMany(mappedBy = "basePlaceByRParentId")
//  public Collection<PlaceEntity> getBasePlacesById() {
//    return placesById;
//  }
//
//  public void setBasePlacesById(Collection<PlaceEntity> placesById) {
//    this.placesById = placesById;
//  }
// }
