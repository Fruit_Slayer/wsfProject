package com.wsfPro.entities.Files;

import com.wsfPro.entities.account.AccountEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "t_file")
public class FileEntity extends FilesSuperEntity {

  /** */
  private static final long serialVersionUID = -9039243589465507252L;

  @ManyToOne
  @JoinColumn(name = "R_account")
  private AccountEntity account;

  @ManyToOne
  @JoinColumn(name = "R_fileFolder")
  private FolderEntity fileFolder;

  public AccountEntity getAccount() {
    return account;
  }

  public void setAccount(AccountEntity account) {
    this.account = account;
  }

  public FolderEntity getFileFolder() {
    return fileFolder;
  }

  public void setFileFolder(FolderEntity fileFolder) {
    this.fileFolder = fileFolder;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }
}
