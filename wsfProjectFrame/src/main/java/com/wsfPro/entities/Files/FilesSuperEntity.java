package com.wsfPro.entities.Files;

import com.wsfPro.entities.BaseEntity;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class FilesSuperEntity extends BaseEntity {

  /** */
  private static final long serialVersionUID = -9005574303285851926L;

  // 相对路径
  private String relativePath;

  // 绝对路径
  private String AbsolutePath;

  // 每一次增删改都计算下文件夹的大小
  private long length;

  public String getRelativePath() {
    return relativePath;
  }

  public void setRelativePath(String relativePath) {
    this.relativePath = relativePath;
  }

  public String getAbsolutePath() {
    return AbsolutePath;
  }

  public void setAbsolutePath(String absolutePath) {
    AbsolutePath = absolutePath;
  }

  public long getLength() {
    return length;
  }

  public void setLength(long length) {
    this.length = length;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }
}
