package com.wsfPro.entities.Files;

import com.wsfPro.entities.account.AccountEntity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "t_folder")
public class FolderEntity extends FilesSuperEntity {
  /** */
  private static final long serialVersionUID = -104473403436716687L;

  @ManyToOne
  @JoinColumn(name = "R_account")
  private AccountEntity account;

  // // 下级文件夹
  // @OrderBy("C_ID desc")
  // @OneToMany(mappedBy = "folders")
  // private List<FolderEntity> folders;

  // 解决实体onetoMany本身实体的方式
  // @OneToMany(mappedBy = "parent")
  // private List<FolderEntity> children;
  // mappedBy = "parent"找到外键是本类属性为parent=？的所有本实体组为一个集合列表给本children属性
  @ManyToOne
  @JoinColumn(name = "R_parent_id")
  private FolderEntity parent;

  @OneToMany(mappedBy = "parent")
  private List<FolderEntity> children;

  // 本级文件夹拥有的文件

  //	@OrderBy("C_ID desc")
  @OneToMany(mappedBy = "fileFolder")
  private List<FileEntity> fileSet;

  public AccountEntity getAccount() {
    return account;
  }

  public void setAccount(AccountEntity account) {
    this.account = account;
  }

  public FolderEntity getParent() {
    return parent;
  }

  public void setParent(FolderEntity parent) {
    this.parent = parent;
  }

  public List<FolderEntity> getChildren() {
    return children;
  }

  public void setChildren(List<FolderEntity> children) {
    this.children = children;
  }

  public List<FileEntity> getFileSet() {
    return fileSet;
  }

  public void setFileSet(List<FileEntity> fileSet) {
    this.fileSet = fileSet;
  }

  // /**
  // * 子目录的数量
  // */
  // @Column(name = "C_SIZE")
  // private int size;

  // /**
  // * 目录下的文件数量
  // */
  // @Column(name = "C_COUNT")
  // private int count;

}
