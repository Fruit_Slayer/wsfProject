package com.wsfPro.entities;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class BaseEntity extends AbstractPersistableEntity {

  /** */
  private static final long serialVersionUID = 2796058005162094843L;

  @Column(name = "C_name")
  private String name;

  @Column(name = "C_deleted")
  private boolean deleted;

  public boolean isDeleted() {
    return deleted;
  }

  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
