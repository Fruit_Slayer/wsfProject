package com.wsfPro.repositoryImpl;

import com.wsfPro.entities.base.AbstractFreeEntity;
import com.wsfPro.repository.PersisTableRepository;
import org.springframework.stereotype.Repository;

@Repository
public class PersisTableRepositoryImpl extends AbstractPersisTableRepositoryImpl<AbstractFreeEntity> implements PersisTableRepository {
}
