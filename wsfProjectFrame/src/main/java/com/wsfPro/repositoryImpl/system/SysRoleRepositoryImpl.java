package com.wsfPro.repositoryImpl.system;

import com.wsfPro.entities.system.SysRoleEntity;
import com.wsfPro.repository.system.SysRoleRepository;
import com.wsfPro.repositoryImpl.base.BaseDateRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class SysRoleRepositoryImpl extends BaseDateRepositoryImpl<SysRoleEntity>
    implements SysRoleRepository {
  @Override
  public List<SysRoleEntity> findRolesByRoleIdList(Map<String, Object> paramMap) {
    String sql = null;
    for (Map.Entry<String, Object> entry : paramMap.entrySet()) {
      sql = "SELECT r.* from sys_role r where r.id in (:" + entry.getKey() + ")";
      break;
    }
    return findByNativeQuery(sql, paramMap);
  }

  @Override
  public List<SysRoleEntity> findRolesByRoleNameList(Map<String, Object> paramMap) {
    String sql = null;
    for (Map.Entry<String, Object> entry : paramMap.entrySet()) {
      sql = "SELECT r.* from sys_role r where r.name in (:" + entry.getKey() + ")";
      break;
    }
    return findByNativeQuery(sql, paramMap);
  }
}
