package com.wsfPro.repositoryImpl.system;

import com.wsfPro.entities.middle.SysRoleMenuEntity;
import com.wsfPro.entities.system.SysMenuEntity;
import com.wsfPro.entities.system.SysRoleEntity;
import com.wsfPro.repository.system.SysMenuRepository;
import com.wsfPro.repository.system.SysRoleRepository;
import com.wsfPro.repositoryImpl.base.BaseCodeRepositoryImpl;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SysMenuRepositoryImpl extends BaseCodeRepositoryImpl<SysMenuEntity>
    implements SysMenuRepository {
  @Autowired private SysRoleRepository sysRoleRepository;
  //
  //  @Override
  //  public JsonResult doSaveOrUpdate(Map paramsMap) {
  //
  //    try {
  //      String id = StringUtils.getMapKeyValue(paramsMap, "id");
  //      String code = StringUtils.getMapKeyValue(paramsMap, "code");
  //      boolean isExistCode = this.isExistCode(code, id);
  //      if (isExistCode) {
  //        return JsonResult.putFail(JsonConstans.ERR_CODE_EXISTS);
  //      }
  //      SysMenuEntity sysMenuEntity = null;
  //      if (StringUtils.isNotBlank(id)) {
  //        sysMenuEntity = this.findById(SysMenuEntity.class, id);
  //        if (sysMenuEntity == null) {
  //          return JsonResult.putFail(JsonConstans.ERR_NOT_EXISTED);
  //        }
  //        EntityUtil.putMapDataIntoEntity(paramsMap, sysMenuEntity);
  //        this.update(sysMenuEntity);
  //      } else {
  //        sysMenuEntity = new SysMenuEntity();
  //        EntityUtil.putMapDataIntoEntity(paramsMap, sysMenuEntity);
  //        this.save(sysMenuEntity);
  //      }
  //      return JsonResult.putSuccess();
  //    } catch (Exception e) {
  //      e.printStackTrace();
  //      return JsonResult.putFail(JsonConstans.OPERATION_FAILURE);
  //    }
  //  }

  //  public boolean delete(String id) {
  //    deleteChildMenus(id);
  //    return super.deleteById(id);
  //  }

  @Override
  public SysMenuEntity findMenuRoleById(String menuId) {
    Map<String, Object> paramMap = new HashMap<>();
    paramMap.put("id", menuId);
    String sql = "select m.* from sys_menu m where m.id=:id";

    SysMenuEntity menu = this.findById(SysMenuEntity.class, menuId);
    if (menu == null) {
      return null;
    }
    sql = "SELECT * FROM sys_role_menu rm WHERE rm.menuId=:menuId";

    paramMap.clear();
    paramMap.put("menuId", menuId);
    // 菜单角色权限中间表
    List<SysRoleMenuEntity> roleMenuList =
        this.findByNativeQuery(sql, paramMap, Transformers.aliasToBean(SysRoleMenuEntity.class));

    if (roleMenuList == null || roleMenuList.size() == 0) {
      return menu;
    }
    sql = "SELECT * FROM sys_role r WHERE r.id in (:roleIdList)";
    List<String> roleIdList = new ArrayList<>();
    for (SysRoleMenuEntity roleMenu : roleMenuList) {
      roleIdList.add(roleMenu.getRoleId());
    }
    paramMap.clear();
    paramMap.put("roleIdList", roleIdList);
    // 所有菜单的拥有的权限
    List<SysRoleEntity> roleList =
        this.findByNativeQuery(sql, paramMap, Transformers.aliasToBean(SysRoleEntity.class));
    if (roleList == null || roleList.size() == 0) {
      return menu;
    }
    menu.setSysRoleList(roleList);
    //        for (SysMenuEntity menu : menuList) {
    //        if (menu.getSysRoleList() == null) {
    //            menu.setSysRoleList(new ArrayList<SysRoleEntity>());
    //        }
    //        roleMenu:
    //        for (SysRoleMenuEntity roleMenu : roleMenuList) {
    //            if (menu.getId().equals(roleMenu.getMenuId())) {
    //                for (SysRoleEntity role : roleList) {
    //                    if (roleMenu.getRoleId().equals(role.getId())) {
    //                        menu.getSysRoleList().add(role);
    //                        continue roleMenu;
    //                    }
    //                }
    //            }
    //        }
    //        }
    return menu;
  }

  @Override
  public List<SysMenuEntity> findMenusByMenuIdList(Map<String, Object> paramMap) {
    String sql = null;
    for (Map.Entry<String, Object> entry : paramMap.entrySet()) {
      sql = "SELECT * from sys_menu where id in(:" + entry.getKey() + ")";
      break;
    }
    return findByNativeQuery(sql, paramMap);
  }

  public void deleteChildMenus(String parentId) {
    // 删除子菜单
    List<SysMenuEntity> sysMenuEntityList = this.findByFiled("parentId", parentId);
    for (SysMenuEntity sysMenuEntity : sysMenuEntityList) {
      deleteChildMenus(String.valueOf(sysMenuEntity.getId()));
      this.remove(sysMenuEntity);
    }
    // 删除角色菜单
    List<SysRoleMenuEntity> sysRoleMenuEntityList =
        this.findByFiled(SysRoleMenuEntity.class, "menuId", parentId);
    for (SysRoleMenuEntity sysRoleMenuEntity : sysRoleMenuEntityList) {
      this.remove(sysRoleMenuEntity);
    }
  }

  //
  //  // 将本级权限的资源分发给下一级，超级管理员可以任意分发资源
  //  @Override
  //  public JsonResult addMenu(SysMenuItem menuItem) {
  //    List<String> roleIdList = menuItem.getRoleIdList();
  //    String url = menuItem.getUrl();
  //    List<SysMenuEntity> existMenuList = this.findByFiled("url", url);
  //    if (existMenuList.size() != 0) {
  //      return JsonResult.putFail("已经存在该" + url + "菜单，添加失败");
  //    }
  //    String code = menuItem.getCode();
  //    String menuName = menuItem.getName();
  //   Map<String,Object> paramMap=new HashMap<>();
  //    List<String> roleNameList=new ArrayList();
  //    paramMap.put("roleNameList",roleNameList);
  //    for (GrantedAuthority
  // grantedAuthority:SecurityPrincipalContext.getUserDetails().getAuthorities()){
  //      roleNameList.add(grantedAuthority.getAuthority());
  //    }
  //    List<SysRoleEntity> userRoleList =sysRoleRepository.findRolesByRoleNameList(paramMap);
  //    // 顶级权限向下存储
  //    HashSet<SysRoleEntity> maxRoleSet = new HashSet<>();
  //    if (roleIdList != null && roleIdList.size() != 0) {
  //      JsonResult jsonResult = checkParamBySaveOrUpdMenu(roleIdList, maxRoleSet,userRoleList);
  //      if (jsonResult != null) {
  //        return jsonResult;
  //      }
  //    }
  //    //    Map paramsMap = new HashMap();
  //
  //    //        String[] urlArray = url.split("/");
  //    //    //    //    //        String matchUrl = null;
  //    //    //    //    //        for (String urlSon : urlArray) {
  //    //    //    //    //          matchUrl += urlSon;
  //    //    //    //    //        }
  //
  //    boolean isExistCode = this.isExistCode(code, null);
  //    if (isExistCode) {
  //      return JsonResult.putFail(JsonConstans.ERR_CODE_EXISTS);
  //    }
  //
  //    SysMenuEntity sysMenuEntity = new SysMenuEntity();
  //    sysMenuEntity.setIsDelete(false);
  //    sysMenuEntity.setUrl(url);
  //    //        sysMenuEntity.setMatchUrl(matchUrl);
  //    sysMenuEntity.setName(menuName);
  //    sysMenuEntity.setCode(code);
  //    //
  //    // sysMenuEntity.setParentId(paramsMap.get("parentId").toString());
  //    //        //    如果前端知道了一些重要的字段名，添加了这些字段的值就不安全了
  //    //        //    EntityUtil.putMapDataIntoEntity(paramsMap, sysMenuEntity);
  //    this.save(sysMenuEntity);
  //    //        int minlevel = 0;
  //    Timestamp createTime = new Timestamp(System.currentTimeMillis());
  //
  //    for (SysRoleEntity maxRole : maxRoleSet) {
  //      saveRoleMenuByRoleTeeSet(maxRole, sysMenuEntity, createTime);
  //    }
  //
  //    //        sysMenuEntity.setParentId(paramsMap.get("parentId").toString());
  //    //    如果前端知道了一些重要的字段名，添加了这些字段的值就不安全了
  //    //    EntityUtil.putMapDataIntoEntity(paramsMap, sysMenuEntity);
  //
  //    return JsonResult.putSuccess();
  //
  //    //    return JsonResult.putFail(JsonConstans.ERR_NOTVALID_AUTH);
  //  }

  //  /**
  //   * 前端传过来默认是该菜单在当前登陆者的权限 如果没有传权限id则是去掉该菜单在当前登陆者的所有权限
  //   * 将修改权限List集合跟save同样做树形排序，获得该排序后的树形TreeSet顶级权限查询数据库是否一致
  //   *
  //   * @param sysMenuDto
  //   * @return
  //   */
  //  @Override
  //  public JsonResult update(SysMenuDto sysMenuDto) {
  //    SysMenuEntity menu = this.findById(SysMenuEntity.class, sysMenuDto.getId());
  //    if (menu == null) {
  //      return JsonResult.putFail("菜单获取失败");
  //    }
  //    menu.setName(sysMenuDto.getName());
  //    menu.setUrl(sysMenuDto.getUrl());
  //    this.update(menu);
  //    //
  //    //    if (menu.getId().equals("153333821338858579")) {
  //    //      throw new RuntimeException("0xx");
  //    //    }
  //    Timestamp updTime = new Timestamp(System.currentTimeMillis());
  //    List roleIdList = sysMenuDto.getRoleIdList();
  //    // 顶级权限向下存储
  //    HashSet<SysRoleEntity> underMaxRoleSet = new HashSet<>();
  //    Map<String, Object> paramMap = new HashMap<>();
  //    List<String> roleNameList = new ArrayList();
  //    paramMap.put("roleNameList", roleNameList);
  //    for (GrantedAuthority grantedAuthority :
  //        SecurityPrincipalContext.getUserDetails().getAuthorities()) {
  //      roleNameList.add(grantedAuthority.getAuthority());
  //    }
  //    List<SysRoleEntity> userRoleList = sysRoleRepository.findRolesByRoleNameList(paramMap);
  //    if (roleIdList != null && roleIdList.size() != 0) {
  //      JsonResult jsonResult = checkParamBySaveOrUpdMenu(roleIdList, underMaxRoleSet,
  // userRoleList);
  //      if (jsonResult != null) {
  //        return jsonResult;
  //      }
  //    }
  //    //    对前端没有传进来的权限即是需要删掉的权限进行处理
  //    List<SysRoleEntity> userRoleNoRemove = new ArrayList<>();
  //
  //    SysRoleEntity adminRole = null;
  //    if (underMaxRoleSet.size() == 1 && underMaxRoleSet.iterator().next().getLevel() == 0) {
  //      adminRole = underMaxRoleSet.iterator().next();
  //      userRoleNoRemove.add(adminRole);
  //    } else {
  //      underMaxRole:
  //      for (SysRoleEntity underMaxRole : underMaxRoleSet) {
  //        for (SysRoleEntity userRole : userRoleList) {
  //          if (underMaxRole.getParentId().equals(userRole.getId())) {
  //            userRoleNoRemove.add(userRole);
  //            continue underMaxRole;
  //          }
  //        }
  //      }
  //    }
  //    userRoleList.removeAll(userRoleNoRemove);
  //
  //    //      处理需要删除的一整个树形权限的菜单
  //    if (userRoleList.size() != 0) {
  //      //      if (userRoleList.size() != 1 && userRoleList.get(0).getLevel() != 0) {
  //      for (SysRoleEntity userRole : userRoleList) {
  //        //        查询role_menu中间表是否该登陆者权限拥有该资源，
  //        // 有则需要以该登陆者权限作为树根自顶向下删除拥有该资源的整个树形权限
  //        //        查询中间表有则删除并查询下一权限
  //
  //        //  递归：下一权限中间表有关联继续删除并查询下一权限，退出条件下一权限没有中间表关联或者没有下一权限
  //        //        userRole
  //        SysRoleMenuEntity roleMenu =
  //            (SysRoleMenuEntity)
  //                super.findMiddleTable(
  //                    SysRoleMenuEntity.class,
  //                    "roleId",
  //                    userRole.getId(),
  //                    "menuId",
  //                    sysMenuDto.getId());
  //        // 登录者有权限才能删除其下级权限的资源
  //        if (roleMenu != null) {
  //          List<SysRoleEntity> underSecondMaxRoleList =
  //              this.findByFiled(SysRoleEntity.class, "parentId", userRole.getId());
  //          for (SysRoleEntity underSecondMaxRole : underSecondMaxRoleList) {
  //            deleteRoleMenuByRoleTree(underSecondMaxRole.getId(), sysMenuDto.getId());
  //          }
  //        }
  //      }
  //      //        如果删除超级管理员对该菜单的权限，
  //
  //      // 那么超级管理员无法访问该菜单也就无法对该菜单进行再次update
  //      //        应该在del操作下可以删除整个菜单以及菜单所有权限，
  //      // 这里的update不允许删除超级管理员对该菜单拥有的权限，以便能继续修改该菜单信息
  //      //      } else {
  //      //        //        超级管理员的权限资源删除
  //      //        deleteRoleMenuByRoleTree(userRoleList.get(0).getId(), sysMenuItem.getId());
  //      //      }
  //    }
  //    if (underMaxRoleSet.size() > 0) {
  //      //    调整该菜单权限为当前树形权限，去掉不在underMaxRoleSet树形权限中的权限，
  //      // 加入树形underMaxRoleSet树形权限中的权限如果存在就不新增
  //
  //      String sql = "";
  //
  //      for (SysRoleEntity underMaxRole : underMaxRoleSet) {
  //
  //        SysRoleMenuEntity roleMenu =
  //            (SysRoleMenuEntity)
  //                super.findMiddleTable(
  //                    SysRoleMenuEntity.class,
  //                    "roleId",
  //                    underMaxRole.getId(),
  //                    "menuId",
  //                    sysMenuDto.getId());
  //        Timestamp createTime = new Timestamp(System.currentTimeMillis());
  //        if (roleMenu == null) {
  //          addRoleMenu(sysMenuDto.getId(), underMaxRole.getId(), createTime);
  //        }
  //        if (underMaxRole.getParentId() != null) {
  //          List<SysRoleEntity> roleList =
  //              this.findByFiled(SysRoleEntity.class, "parentId", underMaxRole.getParentId());
  //
  //          roleList.remove(underMaxRole);
  //          sql =
  //              "DELETE FROM "
  //                  + EntityUtil.getSqlTableName(SysRoleMenuEntity.class)
  //                  + " where roleId in(:roleIdList) and menuId=:menuId";
  //          //          for (SysRoleEntity role : roleList) {
  //          //            middleTableService.delMiddleTable(
  //          //                SysRoleMenuEntity.class, "roleId", role.getId(), "menuId",
  //          // sysMenuItem.getId());
  //          //          }
  //          paramMap.clear();
  //          paramMap.put("roleIdList", roleList);
  //          paramMap.put("menuId", sysMenuDto.getId());
  //          this.findByNativeQuery(sql, paramMap, Transformers.ALIAS_TO_ENTITY_MAP);
  //        }
  //
  //        //      if (underMaxRoleChildRoleSet.size() != 0) {
  //        //        List<SysRoleEntity> allChildRoleList =
  //        //            sysRoleRepository.findByHql(
  //        //                SysRoleEntity.class, "parentId",
  //        // underMaxRoleChildRoleSet.first().getParentId());
  //        //
  //        //        allChildRoleList.removeAll(underMaxRoleChildRoleSet);
  //        //        if (allChildRoleList.size() != 0) {
  //        //          for (SysRoleEntity role : allChildRoleList) {
  //        //            middleTableService.delMiddleTable(
  //        //                SysRoleMenuEntity.class, "roleId", role.getId(), "menuId",
  //        // sysMenuItem.getId());
  //        //          }
  //        //        }
  //        //
  //        //      }
  //        addNewDelNotNeedRoleMenu(underMaxRole, sysMenuDto.getId(), createTime);
  //      }
  //    }
  //    return JsonResult.putSuccess();
  //  }

  //  private void saveSysRoleMenu(String roleId, String menuId, Timestamp createTime) {
  //    SysRoleMenuEntity rm = new SysRoleMenuEntity();
  //    rm.setMenuId(menuId);
  //    rm.setRoleId(roleId);
  //    rm.setCreateTime(createTime);
  //    this.save(rm);
  //  }
  //    public List<String> getParentId(List<String> idList) {
  //        String sql = "select level from sys_role where ";
  //
  //    }

  //
  //  private List<SysMenuEntity> treeSetMenuList(List<SysMenuEntity> rootMenuEntityList) {
  //    List<SysMenuEntity> menuList = new ArrayList<>();
  //    // 先找到所有的一级菜单
  //    for (SysMenuEntity menuEntity : rootMenuEntityList) {
  //      if (StringUtils.isBlank(menuEntity.getParentId())
  //          || STR_ZERO.equals(menuEntity.getParentId())) {
  //        menuList.add(menuEntity);
  //      }
  //    }
  //    // 为一级菜单设置子菜单，getChild是递归调用的
  //    for (SysMenuEntity menu : menuList) {
  //      menu.setChildMenus(getChildMenu(menu.getId(), rootMenuEntityList));
  //    }
  //    return menuList;
  //  }
  //
  //  /**
  //   * 递归查找子菜单
  //   *
  //   * @param id 当前菜单id
  //   * @param rootMenuEntityList 菜单列表
  //   * @return List<SysMenuEntity> 子菜单列表
  //   */
  //  private TreeSet<SysMenuEntity> getChildMenu(String id, List<SysMenuEntity> rootMenuEntityList)
  // {
  //    // 子菜单
  //    TreeSet<SysMenuEntity> childMenuSet =
  //        new TreeSet<SysMenuEntity>(
  //            new Comparator<SysMenuEntity>() {
  //              @Override
  //              public int compare(SysMenuEntity o1, SysMenuEntity o2) {
  //                if (o1.getId().equals(o2.getId())) {
  //                  return 0;
  //                } else {
  //                  return o2.getSort() - o1.getSort();
  //                }
  //              }
  //            });
  //    for (SysMenuEntity childMenu : rootMenuEntityList) {
  //      // 遍历所有节点，将父菜单id与传过来的id比较
  //      if (StringUtils.isNotBlank(childMenu.getParentId())) {
  //        if (childMenu.getParentId().equals(id)) {
  //          childMenuSet.add(childMenu);
  //        }
  //      }
  //    }
  //    // 把子菜单的子菜单再循环一遍
  //    for (SysMenuEntity menu : childMenuSet) {
  //      if (StringUtils.isNotBlank(menu.getCode())) {
  //        // 递归
  //        menu.setChildMenus(getChildMenu(menu.getId(), rootMenuEntityList));
  //      }
  //    } // 递归退出条件
  //    if (childMenuSet.size() == 0) {
  //      return null;
  //    }
  //    return childMenuSet;
  //  }

  //  /**
  //   * 权限上下级别排序
  //   *
  //   * @param fatherRole
  //   * @param roleList
  //   */
  //  public void setChilentRole(SysRoleEntity fatherRole, List<SysRoleEntity> roleList) {
  //    if (fatherRole.getChildRoles() == null) {
  //      fatherRole.setChildRoles(new HashSet<SysRoleEntity>());
  //    }
  //    //    Iterator<SysRoleEntity> it = roleList.iterator();
  //    //    while (it.hasNext()) {
  //    //      SysRoleEntity x = it.next();
  //    //      if (x.equals("del")) {
  //    //        it.remove();
  //    //      }
  //    //    }
  //    for (SysRoleEntity role : roleList) {
  //      if (fatherRole.getId().equals(role.getParentId())) {
  //        fatherRole.getChildRoles().add(role);
  //      }
  //    }
  //
  //    //        if (roleList.size() == 0) {
  //    //    //            return;
  //    //    //        }
  //    if (fatherRole.getChildRoles().size() == 0) {
  //      return;
  //    }
  //    for (SysRoleEntity nextFatherRole : fatherRole.getChildRoles()) {
  //      setChilentRole(nextFatherRole, roleList);
  //    }
  //  }
  //
  //  public void removeRoleTreeSet(
  //      TreeSet<SysRoleEntity> roleTreeSet, HashSet<SysRoleEntity> chilendRoleTreeSet) {
  //    if (chilendRoleTreeSet.size() == 0) {
  //      return;
  //    }
  //    roleTreeSet.remove(chilendRoleTreeSet);
  //    for (SysRoleEntity fatherRole : chilendRoleTreeSet) {
  //      removeRoleTreeSet(roleTreeSet, fatherRole.getChildRoles());
  //    }
  //  }
  //
  //  public void removeRoleList(SysRoleEntity maxRole, List<SysRoleEntity> roleList) {
  //    roleList.remove(maxRole);
  //    Set<SysRoleEntity> chilendSet = maxRole.getChildRoles();
  //    if (chilendSet.size() != 0) {
  //      roleList.removeAll(chilendSet);
  //      for (SysRoleEntity role : chilendSet) {
  //        removeRoleList(role, roleList);
  //      }
  //    }
  //  }
  //
  //  public void saveRoleMenuByRoleTeeSet(
  //      SysRoleEntity roleParent, SysMenuEntity sysMenuEntity, Timestamp createTime) {
  //    addRoleMenu(sysMenuEntity.getId(), roleParent.getId(), createTime);
  //    Set<SysRoleEntity> childRoles = roleParent.getChildRoles();
  //    if (childRoles.size() > 0) {
  //      for (SysRoleEntity chiledRole : childRoles) {
  //        saveRoleMenuByRoleTeeSet(chiledRole, sysMenuEntity, createTime);
  //      }
  //    }
  //  }
  //
  //  JsonResult checkParamBySaveOrUpdMenu(
  //      List roleIdList, HashSet<SysRoleEntity> maxRoleSet, List<SysRoleEntity> userRoleList) {
  //    Map<String, Object> paramMap = new HashMap();
  //    //    String roleIdListKey = "roleIdList";
  //    //    String sql = "select * from sys_role where id in(:" + roleIdListKey + ")";
  //    paramMap.put("roleIdList", roleIdList);
  //    List<SysRoleEntity> roleList = sysRoleRepository.findRolesByRoleIdList(paramMap);
  //    // 指定的权限必须已经存在 roleList.size() == roleIdParamList.size()
  //    //    判断数量做唯一检验
  //    if (roleList.size() != roleIdList.size()) {
  //      return JsonResult.putFail(JsonConstans.ERR_NOTVALID_AUTH);
  //    }
  //
  //    //        1.将权限进行树形结构排序
  //    //        2.
  //    //          2.1 如果当前账号权限是0则顶级权限是0
  //    //    //          2.2 如果不是则检验顶级权限是否是当前所在登陆者的其中一个权限下一级如果不是，则是非法操作，
  //    //              树形结构排序完毕检验是否有额外没有进行排序的权限并且不是账号顶级权限的下一级如果有则是非法操作
  //
  //    SysRoleEntity adminRole = userRoleList.get(0);
  //    if (userRoleList.size() == 1 && adminRole.getLevel() == 0) {
  //      for (SysRoleEntity role : roleList) {
  //        if (adminRole.getId().equals(role.getId())) {
  //          maxRoleSet.add(role);
  //          break;
  //        }
  //      }
  //    } else {
  //      underUserRole:
  //      for (SysRoleEntity role : roleList) {
  //        for (SysRoleEntity userRole : userRoleList) {
  //          if (userRole.getId().equals(role.getParentId())) {
  //            maxRoleSet.add(role);
  //            continue underUserRole;
  //          }
  //        }
  //      }
  //    }
  //    if (maxRoleSet.size() == 0) {
  //      return JsonResult.putFail(JsonConstans.ERR_NOTVALID_AUTH);
  //    }
  //    // 对最高权限进行上下级添加下级权限
  //    for (SysRoleEntity maxRole : maxRoleSet) {
  //      setChilentRole(maxRole, roleList);
  //    }
  //    //        检测roleList是否有不在树形权限内的权限如果有则是非法操作
  //    for (SysRoleEntity maxRole : maxRoleSet) {
  //      removeRoleList(maxRole, roleList);
  //    }
  //    if (roleList.size() != 0) {
  //      return JsonResult.putFail(JsonConstans.ERR_NOTVALID_AUTH);
  //    }
  //    // 除了登陆者权限是0，其他下级无论权限指定的是当前登录者下一级哪一级，都是需要上级存在该菜单
  //    // 指定的权限最高必须在当前登录者权限的下一级，如果是0超级管理员级别则可以指定本级权限
  //
  //    // 如果不是超级管理员登录 需要检验树形权限的树根顶级权限的父级权限是否有该资源
  //    // 如果没有则是非法操作
  //    if (maxRoleSet.iterator().next().getLevel() != 0) {
  //      paramMap.clear();
  //      String sql = "select menuId from sys_role_menu where roleId=:roleId";
  //      for (SysRoleEntity underMaxrole : maxRoleSet) {
  //        paramMap.put("roleId", underMaxrole.getParentId());
  //
  //        List<Map> maxRoleParentHasMenu =
  //            this.findByNativeQuery(sql, paramMap, Transformers.ALIAS_TO_ENTITY_MAP);
  //        //              当前添加的菜单权限需要登陆者对应的权限如果没有该url菜单权限，操作非法
  //        if (maxRoleParentHasMenu.size() != 1) {
  //          return JsonResult.putFail(JsonConstans.ERR_NOTVALID_AUTH);
  //        }
  //      }
  //    }
  //    return null;
  //  }
  //
  //  private void deleteRoleMenuByRoleTree(String roleId, String menuId) {
  //    // 关联表删除没有的话则下一级别权限也是没有直接返回
  //    if (super.delMiddleTable(SysRoleMenuEntity.class, "roleId", roleId, "menuId", menuId)) {
  //      List<SysRoleEntity> childRoleList = this.findByFiled(SysRoleEntity.class, "parentId",
  // roleId);
  //      for (SysRoleEntity parentRole : childRoleList) {
  //        deleteRoleMenuByRoleTree(roleId, menuId);
  //      }
  //    }
  //  }
  //
  //  /**
  //   * 添加权限菜单中间表
  //   *
  //   * @param menuId
  //   * @param roleId
  //   * @param createTime
  //   */
  //  private void addRoleMenu(String menuId, String roleId, Timestamp createTime) {
  //    SysRoleMenuEntity rm = new SysRoleMenuEntity();
  //    rm.setMenuId(menuId);
  //    rm.setRoleId(roleId);
  //    rm.setCreateTime(createTime);
  //    this.save(rm);
  //  }
  //
  //  /**
  //   * 更新菜单权限 添加菜单新权限删除不需要的权限
  //   *
  //   * @param underMaxRole
  //   * @param menuId
  //   * @param createTime
  //   */
  //  private void addNewDelNotNeedRoleMenu(
  //      SysRoleEntity underMaxRole, String menuId, Timestamp createTime) {
  //    HashSet<SysRoleEntity> underMaxRoleChildRoleSet = underMaxRole.getChildRoles();
  //    if (underMaxRoleChildRoleSet.size() != 0) {
  //      List<SysRoleEntity> allChildRoleList =
  //          this.findByFiled(SysRoleEntity.class, "parentId", underMaxRole.getId());
  //      // 移除掉需要的权限剩下的都是无效权限全部删掉
  //      allChildRoleList.removeAll(underMaxRoleChildRoleSet);
  //      if (allChildRoleList.size() != 0) {
  //        for (SysRoleEntity role : allChildRoleList) {
  //          super.delMiddleTable(SysRoleMenuEntity.class, "roleId", role.getId(), "menuId",
  // menuId);
  //        }
  //      }
  //      for (SysRoleEntity role : underMaxRoleChildRoleSet) {
  //        SysRoleMenuEntity roleMenu =
  //            (SysRoleMenuEntity)
  //                super.findMiddleTable(
  //                    SysRoleMenuEntity.class, "roleId", role.getId(), "menuId", menuId);
  //        if (roleMenu == null) {
  //          roleMenu = new SysRoleMenuEntity();
  //          roleMenu.setRoleId(role.getId());
  //          roleMenu.setMenuId(menuId);
  //          roleMenu.setCreateTime(createTime);
  //          this.save(roleMenu);
  //        }
  //        addNewDelNotNeedRoleMenu(role, menuId, createTime);
  //      }
  //    }
  //  }
}
