package com.wsfPro.repositoryImpl.system;

import com.wsfPro.core.system.SysLevelService;
import com.wsfPro.entities.system.SysLevelEntity;
import com.wsfPro.repository.system.SysLevelRepository;
import com.wsfPro.repositoryImpl.AbstractRepositoryImpl;
import com.wsfPro.services.AbstractServiceImpl;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class SysLevelRepositoryImpl extends AbstractRepositoryImpl<SysLevelEntity, String>
    implements SysLevelRepository {}
