package com.wsfPro.repositoryImpl.base;

import com.wsfPro.entities.base.BaseEntity;
import com.wsfPro.repository.base.BaseRepository;
import com.wsfPro.repositoryImpl.AbstractRepositoryImpl;
import org.springframework.stereotype.Repository;

public abstract class BaseRepositoryImpl<X extends BaseEntity>
    extends AbstractRepositoryImpl<X, String> implements BaseRepository<X> {
  @Override
  public void softDelete(X entity) {
    if (!entity.getIsDelete()) {
      entity.setIsDelete(true);
    }
    super.update(entity);
  }
}
