package com.wsfPro.repositoryImpl.base;

import com.wsfPro.core.base.ResponseCodeConstans;
import com.wsfPro.core.base.TokenConstants;
import com.wsfPro.core.base.ValidationUtils;
import com.wsfPro.entities.base.BaseDataEntity;
import com.wsfPro.repository.base.BaseDateRepository;
import com.wsfPro.security.config.SecurityPrincipalContext;
import com.wsfPro.util.StringUtils;
import com.wsfPro.util.controllerUtil.JsonResult;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

public abstract class BaseDateRepositoryImpl<X extends BaseDataEntity> extends BaseRepositoryImpl<X>
    implements BaseDateRepository<X> {

  @Override
  public X save(X entity) {
    Timestamp timeByCreateOrUpd = new Timestamp(System.currentTimeMillis());
    //    entity.setId(TokenConstants.generateUserId());
    //    String createId = SysUserUtils.getCurrentSysUser().getId();
    //    entity.setCreateTime(timeByCreateOrUpd);
    //    entity.setIsDelete(false);
    //    entity.setStatus(1);
    //    entity.setVersion(0);
    //    entity.setCreateId(createId);
    setCreateParam(entity, timeByCreateOrUpd);
    setUpdParam(entity, timeByCreateOrUpd);
    super.save(entity);
    return entity;
  }

  @Override
  public X update(X entity) {

    //      String isVali = ValidationUtils.validation(entity);
    //      if (!isVali.equals(ResponseCodeConstans.SUCCXSS)) {
    //        return JsonResult.putFail(isVali);
    //      }

    setUpdParam(entity, new Timestamp(System.currentTimeMillis()));
    return super.update(entity);
  }

  @Override
  public JsonResult saveOrUpdate(X entity) {
    try {
      //      JsonResult jsonResult = validationXntity(entity);
      //      if (jsonResult != null) {
      //        return jsonResult;
      //      }

      if (StringUtils.isBlank((entity.getId()))) {
        save(entity);
      } else {
        update(entity);
      }
      return JsonResult.putSuccess();
    } catch (Exception e) {
      e.printStackTrace();
      return JsonResult.putFail();
    }
  }

  @Override
  public JsonResult batchObject(List<X> entityList) {
    for (X entity : entityList) {
      String isVali = ValidationUtils.validation(entity);
      if (!isVali.equals(ResponseCodeConstans.SUCCESS)) {
        return JsonResult.putFail(isVali);
      }
      //      if (entity instanceof BaseDataEntity) {
      //      if (((BaseDataEntity) entity).getIsDelete()) {
      //        entityManager.remove(entityManager.merge(entity));
      //      } else {
      Timestamp timestamp = new Timestamp(System.currentTimeMillis());
      if (StringUtils.isBlank(entity.getId())) {
        setCreateParam(entity, timestamp);
        entityManager.persist(entity);
      } else {
        setUpdParam(entity, timestamp);
        entityManager.merge(entity);
      }
      //      }
      //      } else {
      //        entityManager.merge(entity);
      //      }
    }
    return JsonResult.putSuccess();
  }

  private void setCreateParam(X entity, Timestamp createTime) {
    entity.setId(TokenConstants.generateUserId());
    entity.setCreateTime(createTime);
    entity.setIsDelete(false);
    entity.setStatus(1);
    entity.setVersion(0);
    entity.setCreateId(SecurityPrincipalContext.getUserDetailsImpl().getId());
  }

  private void setUpdParam(X entity, Timestamp updTime) {
    entity.setUpdateId(SecurityPrincipalContext.getUserDetailsImpl().getId());
    entity.setUpdateTime(updTime);
  }
}
