package com.wsfPro.util.webService;

import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@WebService
public class a implements WebServiceInterfaceTest {

  @Override
  public String getWeatherByCityName(String cityName) {

    System.out.println("接收到客户端发送的城市名称：" + cityName);
    // 查询天气
    String resultString = "暖和点儿了";
    System.out.println("返回天气信息：" + resultString);

    return resultString;
  }

  public static void main(String[] args) {
    // 第一个参数：服务发布的url
    // 第二个参数：SEI实现类对象
    Endpoint.publish("http://127.0.0.1:12345/weather", new a());
  }
}
