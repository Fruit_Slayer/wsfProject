package com.wsfPro.util.math;

public class GetKaiFangNumber {
  private static final GetKaiFangNumber getKaiFangNumber = new GetKaiFangNumber();

  public static GetKaiFangNumber getKaiFangNumber() {
    return getKaiFangNumber;
  }

  private GetKaiFangNumber() {};
  /**
   * 对分x/2 有(x/2)*(x/2)>x的解为x<=0,x>=4，又因为sqlId x>0,所以 在对分x 即x/2时必须有x>4 需要在
   *
   * @param x
   * @return
   */
  public long get(long x) {
    if (x < 4) {
      if (x == 1) return x;
      else return 4;
    } else {
      return kaiFangNumber(x);
    }
  }

  private long kaiFangNumber(long x) {
    if (x >= Long.MAX_VALUE) throw new RuntimeException(">Long.MAX_VALUE error");
    long href = 0;
    if (x % 2 != 0) href = x / 2 + 1;
    else href = x / 2;
    for (long i = 0; i <= href; i++) {
      if (i * i == x) {
        return x;
      }
    }
    return kaiFangNumber(x + 1);
  }

  public static void main(String[] args) {
    System.out.println(GetKaiFangNumber.getKaiFangNumber().get(99999));
  }
}
