package com.wsfPro.util.mail;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
@PropertySource("classpath:mailsender.properties")
@ConfigurationProperties(prefix = "mailsender")
public class MailSender {
  private String host;
  private int port;
  private String form;
  private String formPassword;
  private String replayAddress;
  private String auth;
  private String debug;
  private String defaultEncoding;
}
