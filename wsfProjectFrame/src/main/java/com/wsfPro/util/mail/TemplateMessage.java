package com.wsfPro.util.mail;

import lombok.Data;

@Data
public class TemplateMessage {

  private String messageCode;

  private String messageStatus;

  private String cause;
}
