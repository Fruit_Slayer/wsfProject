package com.wsfPro.util.mail;

import com.wsfPro.entities.system.SysMenuEntity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class RunableTest implements Runnable {
  private final SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss.SSS");
  private SysMenuEntity menuEntity;

  public RunableTest(SysMenuEntity menuEntity) {
    this.menuEntity = menuEntity;
  }

  @Override
  public void run() {
    System.out.println(
        "thread:"
            + Thread.currentThread().getName()
            + ",time:"
            + format.format(new Date())
            + ",menuEntity Name:"
            + menuEntity.getName());
    try { // 使线程睡眠，模拟线程阻塞情况
      TimeUnit.SECONDS.sleep(1);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
