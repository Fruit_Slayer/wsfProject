// package com.wsfPro.util.mail;
//
// import lombok.Getter;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Component;
//
// import javax.mail.Authenticator;
// import javax.mail.PasswordAuthentication;
// import javax.mail.Session;
// import javax.mail.internet.MimeMessage;
// import java.util.Properties;
//
// @Getter
// @Component
// public final class MimeMessageFactory {
//
//  @Autowired private MailSender mailSender;
//
//  public MimeMessage createMimeMessage(final Properties p) {
//    p.setProperty("mail.smtp.host", mailSender.getHost());
//    p.setProperty("mail.smtp.auth", mailSender.getAuth());
//    p.setProperty("mail.smtp.user", mailSender.getFormName());
//    p.setProperty("mail.smtp.pass", mailSender.getFormPassword());
//    p.setProperty("mail.debug", mailSender.getDebug());
//    return new MimeMessage(
//        Session.getInstance(
//            p,
//            new Authenticator() {
//              @Override
//              protected PasswordAuthentication getPasswordAuthentication() {
//                return new PasswordAuthentication(
//                    p.getProperty("mail.smtp.user"), p.getProperty("mail.smtp.pass"));
//              }
//            }));
//  }
// }
