package com.wsfPro.util.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import java.util.ArrayList;
import java.util.List;

public class SendMail implements Runnable {
  private final Logger log = LoggerFactory.getLogger(SendMail.class);

  private List<Message> messageList;

  public SendMail(List<Message> messageList) {
    this.messageList = messageList;
  }

  public SendMail(Message message) {
    this.messageList = new ArrayList<>();
    messageList.add(message);
  }

  @Override
  public void run() {
    for (int i = 0; i < messageList.size(); i++) {
      try {
        //        Transport transport = messageList.get(i).getSession().getTransport("smtp");
        //        transport.connect("smtp.163.com", "17876571503@163.com", "wap5259177");
        //        transport.sendMessage(messageList.get(i), messageList.get(i).getAllRecipients());
        Transport transport = messageList.get(i).getSession().getTransport("smtp");
        transport.connect("smtp.163.com", "17876571503@163.com", "wap5259177");
        transport.sendMessage(
            messageList.get(i), messageList.get(i).getRecipients(Message.RecipientType.TO)); // 发送邮件
        transport.close();
      } catch (MessagingException e) {
        log.info("mail error:" + e.getMessage());
        e.printStackTrace();
      }
    }
  }

  //  private static final ExecutorService executorPool;
  //
  //  static {
  //    executorPool = Executors.newFixedThreadPool(4);
  //  }
  //
  //  public static void sendMail(List<MailRecipient> infos, MailSender mailSender) {
  //    executorPool.submit(new MailSendUtil(infos, mailSender));
  //    //    executorPool.shutdown();
  //  }

}
