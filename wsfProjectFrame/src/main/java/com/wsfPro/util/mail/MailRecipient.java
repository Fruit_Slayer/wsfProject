package com.wsfPro.util.mail;

import lombok.Getter;
import lombok.Setter;

import java.io.File;

@Getter
@Setter
public class MailRecipient {
  //  private String host;
  //  private String formName;
  //  private String formPassword;
  //  private String replayAddress;
  private String toAddress;
  private String bcc;
  private String cc;
  private String subject;
  private String content;
  private File contentImage;
  private String contentImageID;
  private File[] mailAttachments;

  public MailRecipient(
      String toAddress,
      String bcc,
      String cc,
      String subject,
      String content,
      File contentImage,
      String contentImageID,
      File[] mailAttachments) {
    super();
    this.toAddress = toAddress;
    this.bcc = bcc;
    this.cc = cc;
    this.subject = subject;
    this.content = content;
    this.contentImage = contentImage;
    this.contentImageID = contentImageID;
    this.mailAttachments = mailAttachments;
  }

  public static MailRecipient getMailInfo(
      String toAddress,
      String bcc,
      String cc,
      String subject,
      String content,
      File contentImage,
      String contentImageID,
      File[] mailAttachments) {
    return new MailRecipient(
        toAddress, bcc, cc, subject, content, contentImage, contentImageID, mailAttachments);
  }
  //
  //  public String getHost() {
  //    return host;
  //  }
  //
  //  public void setHost(String host) {
  //    this.host = host;
  //  }
  //
  //  public String getFormName() {
  //    return formName;
  //  }
  //
  //  public void setFormName(String formName) {
  //    this.formName = formName;
  //  }
  //
  //  public String getFormPassword() {
  //    return formPassword;
  //  }
  //
  //  public void setFormPassword(String formPassword) {
  //    this.formPassword = formPassword;
  //  }
  //
  //  public String getReplayAddress() {
  //    return replayAddress;
  //  }
  //
  //  public void setReplayAddress(String replayAddress) {
  //    this.replayAddress = replayAddress;
  //  }

}
