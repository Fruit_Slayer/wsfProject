package com.wsfPro.util.jwt;

public enum JwttokenconfigEnum {
//  jti: jwt的唯一身份标识，主要用来作为一次性token,从而回避重放攻击。
  jti,
  JWT,
//  主题
  sub,
//  aud: 接收jwt的一方
  aud,
  roles,
  header,
  tokenHead,
  exp,
//  iat: jwt的签发时间
  iat,
  createDate,
//  jwtToken的一個Redis緩存Hash集合
  jwtTokenHash,
  type;
  //  private final String value;
  //
  //  private JwttokenconfigEnum(String value) {
  //    this.value = value;
  //  }
  //
    public String getValue() {
      return this.toString();
    }
}
