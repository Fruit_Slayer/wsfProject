package com.wsfPro.util.urlUtil;

import com.wsfPro.filter.Url;

public class UrlUtil {

    /**
     * 不需要进行过滤的url
     *
     * @param url
     * @return false 传入的url不需要过滤
     */
    public static boolean filterUrl(String url) {
        for (Url var : Url.values()) {
            if (url.indexOf(var.toString()) != -1) {
                return false;
            }
        }
        return true;
    }
}
