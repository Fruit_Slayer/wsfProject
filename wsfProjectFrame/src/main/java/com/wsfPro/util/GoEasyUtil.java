package com.wsfPro.util;

import io.goeasy.GoEasy;
import io.goeasy.publish.GoEasyError;
import io.goeasy.publish.PublishListener;
import org.springframework.stereotype.Component;

// 账号1426256791@qq.com pw:wap5259177 channel=applicatonName=wsf
// Subscribe keyBS-e9636157bc6148618d3bf1e2d8d6625f
// Common keyBC-342dbd217d414bbf9312a58b237a358a
@Component(value = "goEasyUtil")
public class GoEasyUtil {
  //  @Resource(name = "goEasy")
  GoEasy goEasy = new GoEasy("BC-342dbd217d414bbf9312a58b237a358a");

  public boolean sendMsg(String msg) {

    goEasy.publish(
        "wsf",
        msg,
        new PublishListener() {
          @Override
          public void onSuccess() {
            System.out.print("消息发布成功 ");
          }

          @Override
          public void onFailed(GoEasyError error) {
            System.out.print("消息发布失败 ,  错误编码：" + error.getCode());
          }
        });
    return false;
  }
}
