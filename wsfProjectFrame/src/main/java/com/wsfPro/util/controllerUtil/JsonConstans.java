package com.wsfPro.util.controllerUtil;

/**
 * 自定义返回的结果值
 */
public enum JsonConstans {
  // 通用返回结果
  ERR_UNKNOWN("未知错误"),
  ERR_WRONG_PARAM("请求参数不完整"),
  ERR_NOTVALID_SIGN("请求校验无效"),
  ERR_NOTVALID_LOGIN("登录失效"),
  ERR_NOTVALID_AUTH("非法用户操作"),
  ERR_NOTVALID_AUTH_WX("微信端用户身份认证不通过"),
  ERR_DUAL_LOGIN("其它地方登陆"),
  ERR_NOT_SUPPORTED("暂不支持该操作"),
  ERR_NOT_EXISTED("记录不存在"),
  ERR_WRONG_DOMAIN("无效的域名"),
  OPERATION_FAILURE("操作失败"),

  // 用户相关的返回结果标识
  ERR_CODE_EXISTS("Code已存在"),
  ERR_USER_NOTFOUND("用户不存在"),
  ERR_USER_PASSORD_NOTMATCH("Password_Not_Match"),
  ERR_USER_EXISTS("用户已存在"),
  ERR_USER_WRONG_PASSWORD("密码格式不正确"),
  ERR_USER_LIMIT("用户被禁止使用"),
  ERR_USER_WXID_NOTMATCH("用户微信号不匹配"),
  ERR_USER_PASSORD_NOTEQUAL("账号或密码输入错误"),
  ERR_PASSORD_NOTEQUAL("原密码输入错误"),

  // 标准返回
  RESULT_SUCCESS_MSG("success"),
  // 标准返回
  RESULT_FAIL_MSG("failed"),

  /** 接口返回成功状态true:成功 */
  RESULT_SUCCESS(true),
  /** 接口返回成功状态 false:失败 */
  RESULT_FAIL(false),
  // 文件上传状态返回
  FILE_SUCCESS_UPLOAD("文件上传成功"),

  FILE_FAIL_UPLOAD("文件上传失败"),

  FILE_CHUNK_EXIST("分片已存在"),

  FILE_CHUNK_NOTEXIST("分片不存在"),

  FILE_CHUNK_MERGER_FAILURE("分片合并失败"),

  FILE_ERR_UPLOAD("文件上传出错"),

  FILE_UPLOADING("文件上传中......");
  private final Object value;

  public Object getValue() {
    return value;
  }

  private JsonConstans(Object value) {
    this.value = value;
  }
}
