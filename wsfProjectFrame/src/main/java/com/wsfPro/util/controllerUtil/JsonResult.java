package com.wsfPro.util.controllerUtil;

import com.wsfPro.util.StringUtils;

/** Json返回结果 */
public class JsonResult {

  /** 是否成功 */
  private boolean rel;
  /** 返回信息 */
  private String msg;
  /** 返回数据 */
  private Object data = "";

  public JsonResult() {}

  public JsonResult(JsonConstans rel, JsonConstans msg, Object data, int count) {
    this.rel = (boolean) rel.getValue();
    this.msg = StringUtils.toStringByObject(msg.getValue());
    this.data = data;
  }

  public JsonResult setError(final JsonResult error, String info) {
    this.rel = error.isRel();
    this.msg = error.getMsg();
    return this;
  }

  public static JsonResult putFail() {
    JsonResult result = new JsonResult();
    setMsgByJsonContext(result, JsonConstans.OPERATION_FAILURE);
    result.setRel((boolean) JsonConstans.RESULT_FAIL.getValue());
    return result;
  }

  public static JsonResult putFail(JsonConstans msg) {
    JsonResult result = new JsonResult();
    result.setMsg(StringUtils.toStringByObject(msg.getValue()));
    result.setRel((boolean) JsonConstans.RESULT_FAIL.getValue());
    return result;
  }

  public static JsonResult putFail(String msg) {
    JsonResult result = new JsonResult();
    result.setMsg(msg);
    result.setRel((boolean) JsonConstans.RESULT_FAIL.getValue());
    return result;
  }

  public static JsonResult putSuccess(Object data) {
    JsonResult result = new JsonResult();
    result.setRel((boolean) JsonConstans.RESULT_SUCCESS.getValue());
    setMsgByJsonContext(result, JsonConstans.RESULT_SUCCESS_MSG);
    result.setData(data);
    return result;
  }

  public static JsonResult putSuccess() {
    JsonResult result = new JsonResult();
    result.setRel((boolean) JsonConstans.RESULT_SUCCESS.getValue());
    result.setMsg(StringUtils.toStringByObject(JsonConstans.RESULT_SUCCESS_MSG));
    return result;
  }

  public static JsonResult putSuccess(JsonConstans msg) {
    JsonResult result = new JsonResult();
    result.setRel((boolean) JsonConstans.RESULT_SUCCESS.getValue());
    setMsgByJsonContext(result, msg);
    return result;
  }

  public static JsonResult getResult(boolean b) {
    JsonResult result = new JsonResult();
    if (b) {
      result.setRel((boolean) JsonConstans.RESULT_SUCCESS.getValue());
      setMsgByJsonContext(result, JsonConstans.RESULT_SUCCESS_MSG);
    } else {
      setMsgByJsonContext(result, JsonConstans.OPERATION_FAILURE);
      result.setRel((boolean) JsonConstans.RESULT_FAIL.getValue());
    }
    return result;
  }

  public boolean isRel() {
    return rel;
  }

  public void setRel(boolean rel) {
    this.rel = rel;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }

  public static String toJson(JsonResult result) {
    return JsonContext.toJson(result);
  }

  private static void setMsgByJsonContext(JsonResult result, JsonConstans jsonConstans) {
    result.setMsg(StringUtils.toStringByObject(jsonConstans.getValue()));
  }
}
