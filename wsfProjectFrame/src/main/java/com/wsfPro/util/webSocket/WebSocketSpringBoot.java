//package com.wsfPro.util.webSocket;
//
//import com.wsfPro.controllers.item.LoginDisplay;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.messaging.handler.annotation.MessageMapping;
//import org.springframework.messaging.simp.SimpMessagingTemplate;
//import org.springframework.stereotype.Service;
//
//@Service
//public class WebSocketSpringBoot {
//
//// private SimpMessagingTemplate simpMessagingTemplate=new SimpMessagingTemplate();
//  /**
//   * WebSocket聊天的相应接收方法和转发方法
//   *
//   * @param login 关于用户聊天的各个信息
//   */
//  @MessageMapping("/userChat")
//  public void userChat(LoginDisplay login) {
//
//    // 找到需要发送的地址
//
//    String dest = "/userChat/chat" + login.getEncryptionId();
//
//    // 发送用户的聊天记录
//
//    this.template.convertAndSend(dest, login);
//  }
//}
