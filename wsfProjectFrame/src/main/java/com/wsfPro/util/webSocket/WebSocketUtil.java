package com.wsfPro.util.webSocket;

import com.wsfPro.controllers.item.LoginDisplay;
import com.wsfPro.controllers.session.LoginSession_WebSocketSession;
import com.wsfPro.core.login.LoginSession_WebSocketSessionInterface;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

//
@ServerEndpoint(value = "/{sid}/{lid}/webSocket", configurator = HttpSessionConfigurator.class)
@Component
public class WebSocketUtil {
  // @Resource(name = "loginSession_WebSocketSession")
  // private LoginSession_WebSocketSessionInterface
  // loginSession_WebSocketSessionInterface;
  private LoginSession_WebSocketSessionInterface loginSession_WebSocketSessionInterface =
      LoginSession_WebSocketSession.getSession();

  // start(Session session)与onOpen(Session session)只能调用其中一个
  @OnOpen
  public void start(
      @PathParam("sid") String sid,
      @PathParam("lid") Long lid,
      Session session,
      EndpointConfig config)
        // 创建webSocket需账号登录成功
      {
    HttpSession httpSession = loginSession_WebSocketSessionInterface.getHttpSessionBySid(sid);
    LoginDisplay login = null;
    if (httpSession != null) {login = (LoginDisplay) httpSession.getAttribute("login");}
    if (httpSession == null || login == null) {
      try {
        session.close();
        return;
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    loginSession_WebSocketSessionInterface.saveWebSocketSession(login.DecryptByDisplay(), session);
    System.out.println(
        "httpSession:" + httpSession.getId() + " webSocket session:" + session.getId());
  }

  /** 连接成功 */
  // @OnOpen
  // public void onOpen(Session session, EndpointConfig config) {
  //
  // System.out.println("webSocket onOpen成功！ " + session.getId());
  // HttpSession httpSession = (HttpSession) config.getUserProperties().get(
  // HttpSession.class.getName());
  // System.out.println("webSocket get SessionID:" + httpSession.getId());
  // LoginDisplay login = (LoginDisplay) httpSession.getAttribute("login");
  // System.out.println(httpSession + "通過webSocket獲取到的login: " + login);
  //
  // }

  @OnMessage
  public void reMessage(
      @PathParam("sid") String sid,
      @PathParam("lid") Long lid,
      HttpServletRequest request,
      Session session,
      String str) {

    // HttpSession httpSession = LoginSessionMap.SESSIONID_SESSION
    // .get(sessionId);
    //
    // httpSession.getId();

    try {
      session.getBasicRemote().sendText(str + "welcome myWeb");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @OnError
  public void error(
      @PathParam("sid") String sid, @PathParam("lid") Long lid, Session session, Throwable t) {

    t.printStackTrace();
  }

  @OnClose
  public void close(@PathParam("sid") String sid, @PathParam("lid") Long lid, Session session) {
    /** 如果httpSession监听器移除了则获取不到 那么应该在监听器中移除socket */
    HttpSession httpSession = loginSession_WebSocketSessionInterface.getHttpSessionBySid(sid);
    if (httpSession != null) {

      LoginDisplay login = (LoginDisplay) httpSession.getAttribute("login");
      if (login != null) {
        loginSession_WebSocketSessionInterface.delWebSocketSessionByLid(login.DecryptByDisplay());
        System.out.println("close webSocket: " + session.getId());
      }
    }
  }
}
