//package com.wsfPro.util.webSocket.websocket.domain;
//
///**
// * @author: eiven
// * @Date: Created in 11:08 2018/6/23
// * 消息接收实体
// */
//public class InMessage {
//    private String name;
//    private String id;
//
//    public InMessage() {
//    }
//
//    public InMessage(String name) {
//        this.name = name;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//}
