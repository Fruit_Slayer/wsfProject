//package com.wsfPro.util.webSocket.websocket.service;
//
//import com.wsfPro.util.webSocket.websocket.contants.WebSocketContants;
//import com.wsfPro.util.webSocket.websocket.domain.OutMessage;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.messaging.simp.SimpMessagingTemplate;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
///**
// * @author: eiven
// * @Date: Created in 13:02 2018/6/23
// */
//@Service
//public class WebSocketService {
//
//    @Autowired
//    private SimpMessagingTemplate template;
//
//    /**
//     * 广播
//     * 发给所有在线用户
//     *
//     * @param msg
//     */
//    public void sendMsg(OutMessage msg) {
//        template.convertAndSend(WebSocketContants.PRODUCERPATH, msg);
//    }
//
//    /**
//     * 发送给指定用户
//     * @param users
//     * @param msg
//     */
//    public void sendToUsers(List<String> users, OutMessage msg) {
//        users.forEach(userName -> {
//            template.convertAndSendToUser(userName, WebSocketContants.P2PPUSHPATH, msg);
//        });
//    }
//    /**
//     * 发送给指定用户
//     * @param user
//     * @param msg
//     */
//    public void sendToUsers(String user, OutMessage msg) {
//        template.convertAndSendToUser(user, WebSocketContants.P2PPUSHPATH, msg);
//    }
//}
