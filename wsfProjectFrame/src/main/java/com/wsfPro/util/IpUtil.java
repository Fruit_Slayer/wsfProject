package com.wsfPro.util;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class IpUtil {
  //  // 没有伪造ip情况下使用
  //  public static String getIpAddr1(HttpServletRequest request) {
  //    String ipAddress = request.getHeader("x-forwarded-for");
  //    if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
  //      ipAddress = request.getHeader("Proxy-Client-IP");
  //    }
  //    if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
  //      ipAddress = request.getHeader("WL-Proxy-Client-IP");
  //    }
  //    if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
  //      ipAddress = request.getRemoteAddr();
  //      if (ipAddress.equals("127.0.0.1") || ipAddress.equals("0:0:0:0:0:0:0:1")) {
  //        // 根据网卡取本机配置的IP
  //        InetAddress inet = null;
  //        try {
  //          inet = InetAddress.getLocalHost();
  //        } catch (UnknownHostException e) {
  //          e.printStackTrace();
  //        }
  //        ipAddress = inet.getHostAddress();
  //      }
  //    }
  //    // 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
  //    if (ipAddress != null && ipAddress.length() > 15) { // "***.***.***.***".length()
  //      // = 15
  //      if (ipAddress.indexOf(",") > 0) {
  //        ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
  //      }
  //    }
  //    return ipAddress;
  //  }

  public static String getIpAddrByNginx(HttpServletRequest request) throws UnknownHostException {

    // 从Nginx中X-Real-IP获取真实ip
    String ipAddress = request.getHeader("X-Real-IP");

    if (ipAddress != null && ipAddress.length() > 0 && !"unknown".equalsIgnoreCase(ipAddress)) {
      return ipAddress;
    }

    // 从Nginx中x-forwarded-for获取真实ip
    ipAddress = request.getHeader("x-forwarded-for");

    if (ipAddress != null && ipAddress.length() > 0 && !"unknown".equalsIgnoreCase(ipAddress)) {
      // 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
      int index = ipAddress.indexOf(",");
      if (index > 0) {
        ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
      }
      return ipAddress;
    }

    ipAddress = request.getRemoteAddr();
    if ("127.0.0.1".equals(ipAddress) || "0:0:0:0:0:0:0:1".equals(ipAddress)) {
      // 根据网卡取本机配置的IP
      ipAddress = InetAddress.getLocalHost().getHostAddress();
    }
    return ipAddress;
  }

  //  public static String getIpAddrByApache(HttpServletRequest request) {
  //    String ipAddress = request.getHeader("Proxy-Client-IP");
  //    if (ipAddress != null && ipAddress.length() > 0 && !"unknown".equalsIgnoreCase(ipAddress)) {
  //      // 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
  //      int index = ipAddress.indexOf(",");
  //      if (index > 0) {
  //        ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
  //      }
  //      logger.info("从x-forwarded-for中获取到ip:" + ipAddress);
  //      return ipAddress;
  //    }
  //  }
  //
  //  public static String getIpAddrByWebLogic(HttpServletRequest request) {
  //    String ipAddress = request.getHeader("WL-Proxy-Client-IP");
  //    if (ipAddress != null && ipAddress.length() > 0 && !"unknown".equalsIgnoreCase(ipAddress)) {
  //      // 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
  //      int index = ipAddress.indexOf(",");
  //      if (index > 0) {
  //        ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
  //      }
  //      logger.info("从x-forwarded-for中获取到ip:" + ipAddress);
  //    }
  //  }
}
