package com.wsfPro.controllers.Account;

import com.wsfPro.WsfApplication;
import com.wsfPro.core.dao.LoginDisplayService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WsfApplication.class)
public class LoginControllerTest {
  /** 日志（slf4j->logback） */
  private static final Logger logger = LoggerFactory.getLogger(LoginDisplayService.class);
  // 注入WebApplicationContext
  @Autowired private WebApplicationContext wac;

  // 模拟MVC对象，通过MockMvcBuilders.webAppContextSetup(this.wac).build()初始化。
  private MockMvc mockMvc;

  @Resource(name = "loginDisplayServiceImpl")
  private LoginDisplayService loginDisplayServiceImpl;

  @Before // 在测试开始前初始化工作
  public void setup() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
  }

  @After
  public void tearDown() throws Exception {}

  @Test
  public void login() {
    try {
      loginDisplayServiceImpl.getLogin("1", "1");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Test
  public void loginPage() {
    Map<String, Object> map = new HashMap<>();
    map.put("address", "合肥");
    //
    //    MvcResult result =
    // mockMvc.perform(post("/q1?address=合肥").content(JSONObject.toJSONString(map)))
    //            .andExpect(status().isOk())// 模拟向testRest发送get请求
    //            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))//
    // 预期返回值的媒体类型text/plain;charset=UTF-8
    //            .andReturn();// 返回执行请求的结果
    String responseString = null;
    try {
      responseString =
          mockMvc
              .perform(
                  get("/login/loginPage")
                      .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                      .header(
                          "authorization",
                          "bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJuYmYiOjE1Mjk0ODQ5NDAsInVzZXJOYW1lIjoid3NmIiwiZXhwIjoxNTI5NDg2NzQwLCJpYXQiOjE1Mjk0ODQ5NDB9.Pewl4d1z4i_8m0KBFXU5OQAKSo6yjkR5480HQALpFU5osQ4Dj--8o8iwEdRKGdZW01wQLmNdxG_gDYlpxQUPLw")
                      .param("pcode", "root"))
              .andExpect(status().isOk())
              .andDo(print())
              .andReturn()
              .getResponse()
              .getContentAsString();
    } catch (Exception e) {
      e.printStackTrace();
    }
    System.out.println("--------返回的json = " + responseString);
  }
}
