package com.wsfPro.controllers.LoginControllerTest;

import com.wsfPro.WsfApplication;
import com.wsfPro.core.dao.LoginDisplayService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WsfApplication.class)
public class LoginControllerTest {

  /** 日志（slf4j->logback） */
  private static final Logger logger = LoggerFactory.getLogger(LoginDisplayService.class);

  @Resource(name = "loginDisplayServiceImpl")
  private LoginDisplayService loginDisplayServiceImpl;

  @Test
  public void insertTest() throws Exception {
    Map map = new HashMap();
    map.put("pageNo", 0);
    map.put("pageSize", 10);
    map.put("id", "440000000000");
    logger.debug("loggerTest");
    System.out.println(loginDisplayServiceImpl.getLogin("1", "1").getEncryptionId());
    //    System.out.println(basePlaceService.getPlaceChildrenByRPanentId(map).get("rows"));
  }
  
}
