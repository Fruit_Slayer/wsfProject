package com.wsfPro;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListIteratorRemoveTest extends AbstractJunitTest<ListIteratorRemoveTest> {
  @Test
  public void listIteratorRemoveTest() {
    List<String> list = new ArrayList<>();
    list.add("t");
    list.add("y");
    list.add("x");
    Iterator<String> iterator = list.iterator();
    while (iterator.hasNext()) {
      if (iterator.next().equals("y")) {
        iterator.remove();
      }
    }
    log.info(list.toString());
  }
}
