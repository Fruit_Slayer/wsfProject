package com.wsfPro;

import com.wsfPro.util.jwt.JwtUtilTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseLog4jConfig<T extends BaseLog4jConfig> {
  private Logger log;

  public BaseLog4jConfig() {};

  public void setBaseLog4jConfig(T t) {
    log = LoggerFactory.getLogger(t.getClass());
  }
}
