package com.wsfPro;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.ParameterizedType;

public abstract class AbstractJunitTest<T> {
  protected Logger log = null;
  protected Class<T> clazz;

  public AbstractJunitTest() {
    //    获取当前new的对象的泛型父类
    ParameterizedType pType = (ParameterizedType) this.getClass().getGenericSuperclass();
    // 获取类型参数的真实值，就是泛型参数的个数；
    this.clazz = (Class<T>) pType.getActualTypeArguments()[0];
    log = LoggerFactory.getLogger(this.clazz);
    log.info("AbstractJunitTest For the:" + clazz);
  }
}
