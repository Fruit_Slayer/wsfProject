package com.wsfPro.entities.system;

import com.wsfPro.repository.system.SysUserRepository;
import com.wsfPro.util.jwt.JwtUtilTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class SysMenuEntityTest {
  /** 日志（slf4j->logback） */
  private final Logger log = LoggerFactory.getLogger(JwtUtilTest.class);
@Autowired
private SysUserRepository sysUserRepository;
  @Test
  public void setChilendParentMenu() {
    List<SysMenuEntity> sysMenuEntityList = new ArrayList<>();
    SysMenuEntity sysMenuEntity = new SysMenuEntity();
    sysMenuEntity.setId("1");
    sysMenuEntity.setSort(1);
    SysMenuEntity sysMenuEntity2 = new SysMenuEntity();
    sysMenuEntity2.setId("2");
    sysMenuEntity2.setSort(2);
    sysMenuEntity2.setParentId("1");
    SysMenuEntity sysMenuEntity3 = new SysMenuEntity();
    sysMenuEntity3.setId("3");
    sysMenuEntity3.setSort(3);
    sysMenuEntity3.setParentId("1");
    SysMenuEntity sysMenuEntity4 = new SysMenuEntity();
    sysMenuEntity4.setId("4");
    sysMenuEntity4.setSort(4);
    sysMenuEntity4.setParentId("2");
    SysMenuEntity sysMenuEntity5 = new SysMenuEntity();
    sysMenuEntity5.setId("5");
    sysMenuEntity5.setSort(5);
    sysMenuEntity5.setParentId("4");
    SysMenuEntity sysMenuEntity6 = new SysMenuEntity();
    sysMenuEntity6.setId("6");
    sysMenuEntity6.setSort(6);
    sysMenuEntity6.setParentId("4");
    SysMenuEntity sysMenuEntity7 = new SysMenuEntity();
    sysMenuEntity7.setId("7");
    sysMenuEntity7.setSort(7);
    sysMenuEntity7.setParentId("3");
    sysMenuEntityList.add(sysMenuEntity);
    sysMenuEntityList.add(sysMenuEntity2);
    sysMenuEntityList.add(sysMenuEntity3);
    sysMenuEntityList.add(sysMenuEntity4);
    sysMenuEntityList.add(sysMenuEntity5);
    sysMenuEntityList.add(sysMenuEntity6);
    sysMenuEntityList.add(sysMenuEntity7);
    TreeSet<SysMenuEntity> sysMenuEntities = SysMenuEntity.upDownMenu(sysMenuEntityList);
    showChild(sysMenuEntities);
  }

  private void showChild(TreeSet<SysMenuEntity> sysMenuEntities) {
    for (SysMenuEntity sysMenuEntity : sysMenuEntities) {
      log.info("id:" + sysMenuEntity.getId() + "parentId:" + sysMenuEntity.getParentId());
      showChild(sysMenuEntity.getChildMenus());
    }
  }
}
