package com.wsfPro;

import com.wsfPro.entities.system.SysMenuEntity;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;

public class LinkHashMapTest {

    /**
     * 日志（slf4j->logback）
     */
    private final Logger log = LoggerFactory.getLogger(LinkHashMapTest.class);

    @Test
    public void linkHashMapTest() {

        log.info("info");

        LinkedHashMap<String, SysMenuEntity> linkedHashMap = new LinkedHashMap();

        SysMenuEntity sysLevelEntity1 = new SysMenuEntity();

        sysLevelEntity1.setId("1");

        SysMenuEntity sysLevelEntity2 = new SysMenuEntity();

        sysLevelEntity2.setId("2");

        SysMenuEntity sysLevelEntity3 = new SysMenuEntity();

        sysLevelEntity3.setId("3");

        SysMenuEntity sysLevelEntity4 = new SysMenuEntity();

        sysLevelEntity4.setId("4");

        SysMenuEntity sysLevelEntity5 = new SysMenuEntity();

        sysLevelEntity5.setId("5");

        linkedHashMap.put("1", sysLevelEntity1);

        linkedHashMap.put("2", sysLevelEntity2);

        linkedHashMap.put("3", sysLevelEntity3);

        linkedHashMap.put("4", sysLevelEntity4);

        linkedHashMap.put("5", sysLevelEntity5);
        Collection collection = linkedHashMap.values();
        Iterator<SysMenuEntity> iterator = collection.iterator();

        while (iterator.hasNext()) {
            log.info("value:" + ((SysMenuEntity) iterator.next()).getId());
        }
    }
}
