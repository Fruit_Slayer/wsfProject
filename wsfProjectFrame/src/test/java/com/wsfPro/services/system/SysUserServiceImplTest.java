package com.wsfPro.services.system;

import com.wsfPro.WsfApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WsfApplication.class)
public class SysUserServiceImplTest {
  @Autowired SysUserServiceImpl sysUserServiceImpl;

  @Test
  public void loadUserByUsername() {
    sysUserServiceImpl.loadUserByUsername("wsf");
  }
}
