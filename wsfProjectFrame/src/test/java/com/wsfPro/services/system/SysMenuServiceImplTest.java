package com.wsfPro.services.system;

import com.wsfPro.WsfApplication;
import com.wsfPro.entities.system.SysRoleEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WsfApplication.class)
public class SysMenuServiceImplTest {
  //    @Autowired
  //    SysMenuService sysMenuService;
  @Autowired SysMenuServiceImpl sysMenuServiceImpl;

  @Test
  public void findMenuList() {
    sysMenuServiceImpl.findMenuRoleById("1");
  }

  @Test
  public void setSysMenuService() {

    List<SysRoleEntity> roleEntityList = new ArrayList<>();

    SysRoleEntity role = new SysRoleEntity();
    role.setLevel(0);
    role.setId("1");

    SysRoleEntity role2 = new SysRoleEntity();
    role2.setLevel(1);
    role2.setId("2");
    role2.setParentId("1");
    SysRoleEntity role3 = new SysRoleEntity();
    role3.setLevel(1);
    role3.setId("3");
    role3.setParentId("1");
    SysRoleEntity role4 = new SysRoleEntity();
    role4.setLevel(1);
    role4.setId("4");
    role4.setParentId("1");
    SysRoleEntity role5 = new SysRoleEntity();
    role5.setLevel(2);
    role5.setId("5");
    role5.setParentId("2");
    SysRoleEntity role6 = new SysRoleEntity();
    role6.setLevel(2);
    role6.setId("6");
    role6.setParentId("3");
    SysRoleEntity role7 = new SysRoleEntity();
    role7.setLevel(2);
    role7.setId("7");
    role7.setParentId("4");
    SysRoleEntity role8 = new SysRoleEntity();
    role8.setLevel(3);
    role8.setId("8");
    role8.setParentId("5");
    SysRoleEntity role9 = new SysRoleEntity();
    role9.setLevel(4);
    role9.setId("9");
    role9.setParentId("8");
    roleEntityList.add(role4);
    roleEntityList.add(role7);
    roleEntityList.add(role5);
    roleEntityList.add(role2);
    roleEntityList.add(role9);
    roleEntityList.add(role);
    roleEntityList.add(role8);
    roleEntityList.add(role3);
    roleEntityList.add(role6);

    LoggerFactory.getLogger(SysMenuServiceImplTest.class)
        .info("removeSuc:" + roleEntityList.remove(role));
    sysMenuServiceImpl.setChilentRole(role, roleEntityList);
    showCh(role);
  }

  public void showCh(SysRoleEntity roleEntity) {
    HashSet<SysRoleEntity> roleList = roleEntity.getChildRoles();
    LoggerFactory.getLogger(SysMenuServiceImplTest.class)
        .info(
            "level:"
                + roleEntity.getLevel()
                + "  roleId:"
                + roleEntity.getId()
                + "  roleParentId:"
                + roleEntity.getParentId());

    if (roleList.size() == 0) {
      return;
    }
    for (SysRoleEntity role : roleList) {
      showCh(role);
    }
  }
}
