package com.wsfPro.util.mail;

import com.wsfPro.AbstractJunitTest;
import com.wsfPro.WsfApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WsfApplication.class)
public class MailSendUtilTest extends AbstractJunitTest<MailSendUtilTest> {

  @Autowired private MailSendUtil mailSendUtil;

  @Test
  public void sendMail() {
    File[] files = new File[2];
    files[0] = new File("C:\\Users\\WSF\\Desktop\\timg.jpg");
    files[1] = new File("C:\\Users\\WSF\\Desktop\\宽带.txt");
    File contentImage = new File("C:\\Users\\WSF\\Desktop\\timg.jpg");
    List<MailRecipient> infos = new ArrayList<>();
    infos.add(
        MailRecipient.getMailInfo(
            "1426256791@qq.com",
            null,
            null,
            "testImageAndTextEmail",
            "<a href = http://haolloyin.blog.51cto.com/>张三辞职信<input type=\"image\" src=\"cid:"
                + "a"
                + "\"></a></br><div title='666'>测试0</div>",
            contentImage,
            "a",
            files));
    infos.add(
        MailRecipient.getMailInfo(
            "1426256791@qq.com",
            null,
            null,
            "testImageAndTextEmail",
            "<a href = http://haolloyin.blog.51cto.com/>张三辞职信<input type=\"image\" src=\"cid:"
                + "a"
                + "\"></a></br><div title='666'>测试1</div>",
            contentImage,
            "a",
            files));
    infos.add(
        MailRecipient.getMailInfo(
            "1426256791@qq.com",
            null,
            null,
            "testImageAndTextEmail",
            "<a href = http://haolloyin.blog.51cto.com/>张三辞职信<input type=\"image\" src=\"cid:"
                + "a"
                + "\"></a></br><div title='666'>测试2</div>",
            contentImage,
            "a",
            files));
    //    mailSendUtil.sendMail(infos);
    //    SendMail.sendMail(infos, mailSender);

    //    ExecutorService pool = Executors.newFixedThreadPool(4);
    //    for (int i = 0; i < 1; i++) {
    //      pool.submit(new MailSendUtil(infos, mailSender));
    //    }
    //    pool.shutdown();
    //    MailSendUtil mailSendUtil = new MailSendUtil(infos, mailSender);
    //    Thread thread = new Thread(mailSendUtil);
    //    thread.run();
    //    thread.start();

    mailSendUtil.baseSendMail(infos);

    //    SendMail sendMail = null;
    //    try {
    //      sendMail = new SendMail(mailSendUtil.setMessageInfo(infos.get(0)));
    //    } catch (Exception e) {
    //      e.printStackTrace();
    //    }
    //    if (sendMail != null) {
    //      Thread thread = new Thread(sendMail);
    //      thread.start();
    //      //      thread.run();
    //    }

    //    asynchronizedExecutor(infos.get(0));

    //    try {
    //      //      mailsender.host=smtp.163.com
    //      //      mailsender.auth=true
    //      //      mailsender.formName=17876571503@163.com
    //      //      mailsender.formPassword=wap5259177
    //      //      mailsender.replayAddress=17876571503@163.com
    //      //      mailsender.debug=true
    //      final String email = "1426256791@qq.com"; // 收件人地址(用;隔开可以发多人邮件)
    //      final String title = "圣诞快乐"; // 邮件标题
    //      final String url = "https://www.google.com";
    //      final String img = "http://www.0756jy.cn/uploads/allimg/101210/6_101210102543_1.jpg"; //
    // 图片地址
    //      final String templetPath = "C:\\Users\\wsf\\Desktop\\MerryChrismas.txt"; // 邮件正文文档兼附件
    //      final String[] toEmails = email.split(";");
    //
    //      for (int i = 0; i < toEmails.length; i++) {
    //        String[] emailModel =
    //            new String[] {"文牛武人", "wenniuwuren", toEmails[i], url, url, url, img}; //
    // 邮件模板的参数设置
    //        System.out.println(
    //            "发送状态:"
    //                + JavaEmailTest.getBean()
    //                    .asynchronizedExecutor(toEmails[i], title, templetPath, emailModel));
    //      }
    //
    //    } catch (Exception e) {
    //      e.printStackTrace();
    //    }
  }
}
