package com.wsfPro.util.mail;

import com.wsfPro.entities.system.SysMenuEntity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RunableTestTest {

  public static void main(String[] args) {
    ExecutorService pool = Executors.newFixedThreadPool(4);
    for (int i = 0; i < 50; i++) {
      SysMenuEntity menuEntity = new SysMenuEntity();
      menuEntity.setName("id" + i);
      pool.submit(new RunableTest(menuEntity));
    }
    pool.shutdown();
  }
}
