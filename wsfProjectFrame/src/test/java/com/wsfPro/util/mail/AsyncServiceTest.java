package com.wsfPro.util.mail;

import com.wsfPro.AbstractJunitTest;
import com.wsfPro.WsfApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WsfApplication.class)
public class AsyncServiceTest extends AbstractJunitTest<AsyncServiceTest> {
  @Autowired MailSendUtil mailSendUtil;

  @Test
  public void doTaskOne() throws Exception {

    long start = System.currentTimeMillis();
    File[] files = new File[2];
    files[0] = new File("C:\\Users\\WSF\\Desktop\\timg.jpg");
    files[1] = new File("C:\\Users\\WSF\\Desktop\\宽带.txt");
    File contentImage = new File("C:\\Users\\WSF\\Desktop\\timg.jpg");
    TemplateMessage templateMessage = new TemplateMessage();
    templateMessage.setMessageStatus("status");
    templateMessage.setMessageCode("code");
    templateMessage.setCause("cause");
    String content = mailSendUtil.readTemplate(templateMessage, "mail/message.ftl");
    //    content =
    //        "<a href = http://haolloyin.blog.51cto.com/>张三辞职信<input type=\"image\" src=\"cid:"
    //            + "a"
    //            + "\"></a></br><div title='666'>测试2</div>";
    List<MailRecipient> infos = new ArrayList<>();
    MailRecipient mailRecipient =
        MailRecipient.getMailInfo(
            "1426256791@qq.com",
            null,
            null,
            "testImageAndTextEmail",
            content,
            contentImage,
            "a",
            files);
    //    Future<String> task1 = asyncService.doTaskOne("1426256791@qq.com", "辞职信", "辞职");

    //    Future<Boolean> task1 = asyncService.sendMail("1426256791@qq.com", "加班", "今晚不");

    //    asyncService.sendMail("1426256791@qq.com", "加班", "今晚不2");
    Future<Boolean> task2 = mailSendUtil.sendMail(mailRecipient);

    int i = 0;
    //    if (task1.get().equals("任务一完成") && task2.get() &&
    // task3.get().equals("任务三完成"))阻塞太久达到4万多毫秒，使用while循环获取结果只需7k多毫秒
    //    if (task1.get().equals("任务一完成") && task2.get() && task3.get().equals("任务三完成")) {
    //      log.info("get(){}", "是阻塞的，必须等待该异步方法执行完成返回值才能执行其外层方法");
    //    }

    while (true) {
      if (task2.isDone()) {
        // 三个任务都调用完成，退出循环等待
        log.info("mail Report {}", "success");
        break;
      }
    }
    //    asyncService.sendMail(mailRecipient);
    //    if (task2.get()) {}
  }

  @Test
  public void doTaskTwo() {}

  @Test
  public void doTaskThree() {}
}
