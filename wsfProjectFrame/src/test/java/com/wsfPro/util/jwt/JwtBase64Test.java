package com.wsfPro.util.jwt;

import com.wsfPro.AbstractJunitTest;
import org.junit.Test;
import sun.misc.BASE64Decoder;

import java.io.IOException;

public class JwtBase64Test extends AbstractJunitTest<JwtBase64Test> {
  /**
   * BASE64解密
   *
   * @param key
   * @return
   * @throws Exception
   */
  public static String decryptBASE64(String key) {
    byte[] bt;
    try {
      bt = (new BASE64Decoder()).decodeBuffer(key);
      return new String(bt); // 如果出现乱码可以改成： String(bt, "utf-8")或 gbk
    } catch (IOException e) {
      e.printStackTrace();
      return "";
    }
  }

  @Test
  public void base64GetTokenPayload() {
    String payload =
        "eyJzdWIiOiJATm90QmxhbmsobWVzc2FnZSA9IFwiY29kZeS4jeiDveS4uuepulwiKSIsInBhc3N3b3JkIjoiJDJhJDEwJHlTRzJsa3ZqRkhZNU8wLi9DUElFMU9JOFZKc3VLWUV6T1l6cUlhN0FKUjZzRWdTelVGT0FtIiwibmJmIjoxNTM5MTAwODg0LCJyb2xlcyI6W3siYXV0aG9yaXR5IjoiUk9MRV9hZG1pbiJ9XSwiaXNzIjoid3NmSXNzIiwiZXhwIjoxNTM5MTAxNDg0LCJpYXQiOjE1MzkxMDA4ODQsImp0aSI6IjE1MjY4NzExMDUyMDU3NzYzMiIsImNyZWF0ZURhdGUiOjE1MzkxMDA4ODQ0Mjl9";
    //    payload = "eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFM1MTIifQ";
    log.info(decryptBASE64(payload));
  }
}
