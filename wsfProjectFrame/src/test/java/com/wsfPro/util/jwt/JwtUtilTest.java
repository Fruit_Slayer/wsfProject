package com.wsfPro.util.jwt;

import com.wsfPro.WsfApplication;
import com.wsfPro.core.base.SysConstants;
import com.wsfPro.entities.system.SysRoleEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WsfApplication.class)
public class JwtUtilTest {
  /** 日志（slf4j->logback） */
  private final Logger log = LoggerFactory.getLogger(JwtUtilTest.class);

  private String token;
  private String base64Secret;
  @Autowired private JwtUtil jwtUtil;

  @Before
  public void setUp() throws Exception {}

  @After
  public void tearDown() throws Exception {}

  @Test
  public void generateToken() {
    Map<String, Object> map = new HashMap();
    map.put("username", "wsf");
    List<SysRoleEntity> rolesList = new ArrayList();
    SysRoleEntity r = new SysRoleEntity();
    r.setId("1");
    r.setName("ROLE_admin");
    rolesList.add(r);
    SysRoleEntity r2 = new SysRoleEntity();
    r2.setId("2");
    r2.setName("ROLE_admin");
    rolesList.add(r2);
    map.put(JwttokenconfigEnum.roles.toString(), rolesList);
    String token = jwtUtil.generateToken(map);
    log.info("Test:generateToken;" + token);
    //            token="bearer
    // eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFM1MTIifQ.eyJzdWIiOiIiLCJuYmYiOjE1MzE0MTIyODIsInJvbGVzIjpbeyJpZCI6IjEiLCJjb2RlIjpudWxsLCJuYW1lIjpudWxsLCJlbk5hbWUiOm51bGwsInJlbWFya3MiOm51bGwsImNyZWF0ZUlkIjpudWxsLCJjcmVhdGVUaW1lIjpudWxsLCJ1cGRhdGVJZCI6bnVsbCwidXBkYXRlVGltZSI6bnVsbCwidmVyc2lvbiI6bnVsbCwiZGVsZXRlIjpmYWxzZSwib3JnSWQiOm51bGwsImlzQ2hlY2siOm51bGx9LHsiaWQiOiIyIiwiY29kZSI6bnVsbCwibmFtZSI6bnVsbCwiZW5OYW1lIjpudWxsLCJyZW1hcmtzIjpudWxsLCJjcmVhdGVJZCI6bnVsbCwiY3JlYXRlVGltZSI6bnVsbCwidXBkYXRlSWQiOm51bGwsInVwZGF0ZVRpbWUiOm51bGwsInZlcnNpb24iOm51bGwsImRlbGV0ZSI6ZmFsc2UsIm9yZ0lkIjpudWxsLCJpc0NoZWNrIjpudWxsfV0sImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODAwMC9sb2dpbiIsImV4cCI6MTUzMTQxMjM0MiwiaWF0IjoxNTMxNDEyMjgyLCJ1c2VybmFtZSI6IndzZiIsImNyZWF0ZURhdGUiOjE1MzE0MTIyODI5NDV9.Kad_AhCt91ghvGEz4wDydVwpXtq9lko6l7ihsPnA_mJC2eytCjtxm88ER8_JkJqKtNHnLAuyx79_WxCGkTpGQg";
    map.clear();
    map = jwtUtil.validateToken(token);
    log.info("Test:validateToken;" + token);
    List<Map> roleList = (List<Map>) map.get(JwttokenconfigEnum.roles.toString());
    SysRoleEntity role1 = new SysRoleEntity();
    role1.setId(roleList.get(0).get(SysConstants.ID_CODE.getValue()).toString());
    role1.setName(roleList.get(0).get("name").toString());
    SysRoleEntity role2 = new SysRoleEntity();
    role1.setId(roleList.get(1).get(SysConstants.ID_CODE.getValue()).toString());
    role2.setName(roleList.get(0).get("name").toString());
    log.info("Test:roleList value1:" + role1.getId());
    log.info("Test:roleList value2:" + role2.getId());
    Date exp = new Date(Long.parseLong(map.get("exp").toString()));
    Date iat = new Date(Long.parseLong(map.get("iat").toString()));
    try {
      Thread.sleep(11000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    log.info("Test:refreshToken:" + jwtUtil.refreshToken(map));
  }

  @Test
  public void logtest() {
    log.debug("debug");
    log.info("info");
    log.error("eroor" + JwttokenconfigEnum.exp);
    System.out.println(JwttokenconfigEnum.exp);
  }

  @Test
  public void addJwtTokenToRedisHash() {
    jwtUtil.addJwtTokenToRedisHash("1", "jwtTokenTest");
    log.info("redisGetHashTest:" + jwtUtil.getJwtTokenByRedisHash("1"));
    try {
      Thread.sleep(10000L);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    log.info("rediseExpireHasValue:" + jwtUtil.getJwtTokenByRedisHash("1"));
    log.info("Test:removeJwtTokenByRedisHash:" + jwtUtil.removeJwtTokenByRedisHash("1"));
  }

  @Test
  public void testSlip() {
    String s = "asdasfavaxc";
    String[] usernameJwtToken = s.split(",");
    log.info("splitTest:{length:" + usernameJwtToken.length + " var" + usernameJwtToken[0]);
  }
}
