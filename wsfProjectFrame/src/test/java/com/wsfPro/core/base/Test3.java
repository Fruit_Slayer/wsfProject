package com.wsfPro.core.base;

public class Test3 {
  private long test33;
  private Test4 test4;

  public long getTest33() {
    return test33;
  }

  public void setTest33(long test33) {
    this.test33 = test33;
  }

  public Test4 getTest4() {
    return test4;
  }

  public void setTest4(Test4 test4) {
    this.test4 = test4;
  }
}
