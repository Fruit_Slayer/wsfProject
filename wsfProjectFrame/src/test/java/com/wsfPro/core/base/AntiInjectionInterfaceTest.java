package com.wsfPro.core.base;

import com.wsfPro.WsfApplication;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashSet;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WsfApplication.class)
public class AntiInjectionInterfaceTest {
  /** 日志（slf4j->logback） */
  private final Logger log = LoggerFactory.getLogger(AntiInjectionInterfaceTest.class);

  @Autowired private AntiInjectionInterface antiInjectionInterface;

  @Before
  public void setUp() throws Exception {}

  @After
  public void tearDown() throws Exception {}

  @Test
  public void checkSqlAntiInfusion() {

        Set<String> set = new HashSet<>();
        set.add("test1");
        set.add("fatest4");
        set.add("test222");
        set.add("test33");
        set.add("test1='zk' and fatest3='555'");
        set.add("test444444");
        antiInjectionInterface.checkSqlAntiInfusion(set, com.wsfPro.core.base.Test.class,
     Test2.class);
        for (String res : set) {
          log.info("非法注入字段：" + res);
        }
  }
}
