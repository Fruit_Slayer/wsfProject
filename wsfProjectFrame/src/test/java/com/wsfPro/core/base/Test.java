package com.wsfPro.core.base;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Test extends TestObj {
  public String test4;
  protected String test3;
  String test2;
  private String test1;
  private List<String> test5;
  private ArrayList<String> test6;
  private Map<String, Object> test7;
  private TreeMap<String, TestObj> test8;

  public String getTest1() {
    return test1;
  }

  public void setTest1(String test1) {
    this.test1 = test1;
  }

  public String getTest2() {
    return test2;
  }

  public void setTest2(String test2) {
    this.test2 = test2;
  }

  public String getTest3() {
    return test3;
  }

  public void setTest3(String test3) {
    this.test3 = test3;
  }

  public String getTest4() {
    return test4;
  }

  public void setTest4(String test4) {
    this.test4 = test4;
  }
}
