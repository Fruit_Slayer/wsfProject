package com.wsfPro.core.base;

public class TestObj {
  public String fatest4;
  protected String fatest3;
  String fatest2;
  private String fatest1;
  private String[] list;

  public String[] getList() {
    return list;
  }

  public void setList(String[] list) {
    this.list = list;
  }

  public String getFatest1() {
    return fatest1;
  }

  public void setFatest1(String fatest1) {
    this.fatest1 = fatest1;
  }

  public String getFatest2() {
    return fatest2;
  }

  public void setFatest2(String fatest2) {
    this.fatest2 = fatest2;
  }

  public String getFatest3() {
    return fatest3;
  }

  public void setFatest3(String fatest3) {
    this.fatest3 = fatest3;
  }

  public String getFatest4() {
    return fatest4;
  }

  public void setFatest4(String fatest4) {
    this.fatest4 = fatest4;
  }
}
