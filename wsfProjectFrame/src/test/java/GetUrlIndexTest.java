import com.wsfPro.AbstractJunitTest;
import org.junit.Test;

public class GetUrlIndexTest extends AbstractJunitTest<GetUrlIndexTest> {
  @Test
  public void testGetUrlIndex() {
    String url = "127.0.0.1:8081/sysMenu/153901176971477962/delSysMenuById?s=153901176971477962";
    int indexGet = url.lastIndexOf("?");
    log.info("?index:" + indexGet);
    if (indexGet != -1) {
      url = url.substring(0, indexGet);
    }
    log.info("url:" + url);
  }
}
