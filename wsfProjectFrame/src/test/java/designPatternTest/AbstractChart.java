package designPatternTest;
//开闭原则(Open-Closed Principle, OCP)：一个软件实体应当对扩展开放，对修改关闭。即软件实体应尽量在不修改原有代码的情况下进行扩展。
//
//        在开闭原则的定义中，软件实体可以指一个软件模块、一个由多个类组成的局部结构或一个独立的类。
// 在该代码中，如果需要增加一个新的图表类，如折线图LineChart，则需要修改ChartDisplay类的display()方法的源代码，增加新的判断逻辑，违反了开闭原则。
//
//         现对该系统进行重构，使之符合开闭原则。
//
//         在本实例中，由于在ChartDisplay类的display()方法中针对每一个图表类编程，因此增加新的图表类不得不修改源代码。可以通过抽象化的方式对系统进行重构，使之增加新的图表类时无须修改源代码，满足开闭原则。具体做法如下：
//
//         (1) 增加一个抽象图表类AbstractChart，将各种具体图表类作为其子类；
//
//         (2)  ChartDisplay类针对抽象图表类进行编程，由客户端来决定使用哪种具体图表。
public abstract class  AbstractChart {
     protected abstract void display();
}
