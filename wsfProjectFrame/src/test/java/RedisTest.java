import com.wsfPro.WsfApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WsfApplication.class)
public class RedisTest {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void set() {
//        redisTemplate.opsForValue().set("test:set", "testValue1");
        redisTemplate.opsForSet().add("jwtToken", "test1", "test2");
        System.out.println(redisTemplate.opsForSet().isMember("jwtToken", "test2"));
        System.out.println("remove jwtTokenSet number:" + redisTemplate.opsForSet().remove("jwtToken", "test2"));
        System.out.println(redisTemplate.opsForSet().isMember("jwtToken", "test2"));
    }

    public RedisTemplate getRedisTemplate() {
        return redisTemplate;
    }

    public void setRedisTemplate(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }
}